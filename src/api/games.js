import { getDataRequest } from "./fetch";
import axios from "axios";

const gameFields = `id, popularity, name, rating, keywords.name, summary, first_release_date, time_to_beat.normally, 
involved_companies.company.name,screenshots.image_id,cover.image_id, platforms.name,
platforms.abbreviation, genres.name, themes.name, game_modes.name, themes.name, dlcs, expansions,release_dates.date, release_dates.platform, similar_games.cover.image_id, artworks.image_id, videos.video_id, alternative_names.game, alternative_names.name`;

export const getMainGameData = async genre => {
  return new Promise(async (resolve, reject) => {
    const today = new Date();
    const twoWeeksAgo = new Date(today.getTime() - 30 * 24 * 60 * 60 * 1000);
    const games = await getDataRequest(
      "games",
      `fields ${gameFields}; where first_release_date > ${Math.trunc(twoWeeksAgo / 1000)} & first_release_date < ${Math.trunc(
        today / 1000,
      )} & themes != (42) ${genre ? ` & ${genre.type} = (${genre.id})` : ""}; sort popularity desc; limit 15;`,
    );

    const rand = Math.floor(Math.random() * 10);
    let game = games[rand];

    console.log("Main Game Data", game);
    resolve(game);
  });
};

// TODO GetMyFriendsSection

export const getPopularSection = async (offset, genre) => {
  return new Promise(async (resolve, reject) => {
    const today = new Date();
    const twoWeeksAgo = new Date(today.getTime() - (genre ? 66 : 30) * 24 * 60 * 60 * 1000);
    const games = await getDataRequest(
      "games",
      `fields ${gameFields}; where first_release_date < ${Math.trunc(today / 1000)} ${
        genre ? ` & ${genre.type} = (${genre.id})` : ""
      } & themes != (42);  sort popularity desc;  limit 15; offset ${offset ? offset : 0};`,
    );
    games.reverse();
    console.log("Popular GamesData", games);
    resolve(games);
  });
};
export const getTopRatedSection = async (offset, genre) => {
  return new Promise(async (resolve, reject) => {
    const today = new Date();
    const thisYear = new Date(today.getFullYear() - 5, 0, 1, 0, 0, 0, 0);
    const games = await getDataRequest(
      "games",
      `fields ${gameFields}; where first_release_date > ${Math.trunc(thisYear / 1000)} & first_release_date < ${Math.trunc(
        today / 1000,
      )} & total_rating > 75 ${genre ? `& ${genre.type} = (${genre.id})` : ""} & themes != (42); limit 15; sort rating desc; offset ${
        offset ? offset : 0
      };`,
    );
    games.reverse();
    console.log("Top Rated GamesData", games);
    resolve(games);
  });
};

export const getMostAnticipated = async offset => {
  return new Promise(async (resolve, reject) => {
    const today = new Date();
    const games = await getDataRequest(
      "games",
      `fields ${gameFields}; where first_release_date > ${Math.trunc(today / 1000)}; limit 15; sort popularity desc;`,
    );
    games.reverse();
    resolve(games);
  });
};

export const getOneMonthLater = async offset => {
  return new Promise(async (resolve, reject) => {
    const today = new Date();
    let oneMonthLater = new Date();
    let sevenDaysLater = new Date();

    sevenDaysLater.setDate(today.getDate() + 7);
    oneMonthLater.setDate(sevenDaysLater.getDate() + 30);

    const games = await getDataRequest(
      "games",
      `fields ${gameFields}; where first_release_date > ${Math.trunc(today / 1000)} & first_release_date < ${Math.trunc(
        oneMonthLater / 1000,
      )}; limit 15; offset ${offset ? offset : 0}; sort popularity desc;`,
    );
    games.reverse();
    resolve(games);
  });
};

export const getSevenDaysLater = async offset => {
  return new Promise(async (resolve, reject) => {
    const today = new Date();
    let sevenDaysLater = new Date();
    sevenDaysLater.setDate(today.getDate() + 7);
    const games = await getDataRequest(
      "games",
      `fields ${gameFields}; where first_release_date > ${Math.trunc(today / 1000)} & first_release_date < ${Math.trunc(
        sevenDaysLater / 1000,
      )}; limit 15; offset ${offset ? offset : 0}; sort popularity desc;`,
    );
    games.reverse();
    resolve(games);
  });
};

export const getAllUpComing = async offset => {
  return new Promise(async (resolve, reject) => {
    const today = new Date();
    let afterAll = new Date();
    afterAll.setDate(today.getDate() + 37);
    const games = await getDataRequest(
      "games",
      `fields ${gameFields}; where first_release_date > ${Math.trunc(afterAll / 1000)}; limit 18; offset ${offset ? offset : 0};`,
    );
    games.reverse();
    resolve(games);
  });
};

export const getNextTrailerSection = async () => {
  const headers = {
    Accept: "application/json",
    "X-CSRF-Token": "/CSzfYrddYY0bM1l8zT7YMM6Q8xsNrqxuflRHCQnj0i96wtj0AqrWlAo+pcy68fqBqKi3UZ6F5nDavMjaytH8Q==",
    "X-NewRelic-ID": "UwcGVFBADQMBVVRR",
  };

  return await axios({
    url: `https://www.igdb.com/next_trailer`,
    method: "GET",
    headers,
  })
    .then(response => {
      // console.log(response.data);
      return response.data;
    })
    .catch(err => {
      console.error("Request Error", err);
      return error;
    });
};

// Indie id=32
export const getGenreGamesSection = async (offset, genre, theme) => {
  return new Promise(async (resolve, reject) => {
    const today = new Date();
    const twoYearsAgo = new Date(today.getTime() - 900 * 24 * 60 * 60 * 1000);
    const games = await getDataRequest(
      "games",
      `fields ${gameFields}; where first_release_date < ${Math.trunc(today / 1000)} & first_release_date > ${Math.trunc(
        twoYearsAgo / 1000,
      )} ${genre ? `& ${genre.type} = [${genre.id}]` : ""} ${
        theme ? `& ${theme.type} = [${theme.id}]` : ""
      } & themes != (42); sort popularity desc; limit 15; offset ${offset ? offset : 0};`,
    );
    games.reverse();
    console.log("Genre GamesData", games);
    resolve(games);
  });
};

export const getCollectionsSection = async offset => {
  const offsetRandom = Math.floor(Math.random() * 50);
  return new Promise(async (resolve, reject) => {
    const games = await getDataRequest(
      "games",
      `fields ${gameFields}; where category = 3 & total_rating > 83; limit 15; offset ${offset ? offset : offsetRandom};`,
    );
    const gamesData = games.map(game => {
      return {
        data: game,
      };
    });

    console.log("getCollectionsSection", gamesData);
    resolve(gamesData);
  });
};

export const getGamesByIds = async ids => {
  return new Promise(async (resolve, reject) => {
    const games = await getDataRequest("games", `fields ${gameFields}; where id = (${ids}); limit 15;`);
    const gamesData = games.map(game => {
      return {
        data: game,
      };
    });

    console.log("getGamesByIds", gamesData);
    resolve(gamesData);
  });
};

export const getGameById = async id => {
  return new Promise(async (resolve, reject) => {
    const games = await getDataRequest("games", `fields ${gameFields}; where id = ${id}; limit 15;`);
    const gameData = games.map(game => {
      return {
        data: game,
      };
    });

    console.log("getGameById", gameData);
    resolve(gameData);
  });
};

export const getCharactersByGame = async gameId => {
  return new Promise(async (resolve, reject) => {
    const characters = await getDataRequest("character", `fields *; where id = ${gameId}; limit 15;`);

    console.log("getCharactersByGame", characters);
    resolve(characters);
  });
};

export const getCoverById = async gId => {
  return new Promise(async (resolve, reject) => {
    const covers = await getDataRequest("covers", `fields *; where game = ${gId}; limit 1;`);

    const game = await getDataRequest("games", `fields name, summary; where id = ${gId}; limit 1;`);

    const result = {
      game,
      cover: covers[0],
    };

    resolve(result);
  });
};

export const getScreenShotById = async gId => {
  return new Promise(async (resolve, reject) => {
    const covers = await getDataRequest("screenshots", `fields *; where game = ${gId}; limit 50;`);
    const rand = Math.floor(Math.random() * covers.length);

    resolve(covers[rand]);
  });
};

export const getVideosById = async gId => {
  return new Promise(async (resolve, reject) => {
    const videos = await getDataRequest("game_videos", `fields *; where id = (${gId}); limit 50;`);

    resolve(videos);
  });
};

export const getArtWorksById = async gId => {
  return new Promise(async (resolve, reject) => {
    const artworks = await getDataRequest("artworks", `fields *; where id = (${gId}); limit 50;`);

    resolve(artworks);
  });
};

// TODO
export const getMyListSection = async () => {
  return new Promise(async (resolve, reject) => {
    resolve([
      {
        image_id: "co1re6",
      },
      {
        image_id: "co1m55",
      },
      {
        image_id: "co1n24",
      },
      {
        image_id: "co1n1f",
      },
      {
        image_id: "co1n24",
      },
      {
        image_id: "co1lyv",
      },
    ]);
  });
};
