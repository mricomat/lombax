import { getDataRequest } from "./fetch";

const gameFields = `id, popularity, name, rating, total_rating, keywords.name, summary, first_release_date, time_to_beat.normally, 
involved_companies.company.name,screenshots.image_id,cover.image_id, artworks.image_id, platforms.name,
platforms.abbreviation, genres.name, game_modes.name, themes.name, dlcs, expansions,release_dates.date, release_dates.platform, similar_games, artworks, videos`;

const companieFields = `id, name, description, logo.image_id, published, developed`;

export const searchRequest = async query => {
  return new Promise(async (resolve, reject) => {
    const games = await getDataRequest("games", `fields ${gameFields};${query} & category != 3; limit 7; sort total_rating asc;`);
    games.reverse();

    const collections = await getDataRequest("games", `fields ${gameFields};${query} & category = 3; limit 7; sort total_rating asc;`);
    collections.reverse();

    const highlightGame = await getDataRequest(
      "games",
      `fields ${gameFields};${query} & category != 3 & total_rating > 50 & popularity > 50; limit 5;`,
    );

    // console.log("SearchRequest highlightGame", highlightGame);
    // console.log("SearchRequest collections", collections);
    // console.log("SearchRequest games", games);
    const rand = Math.floor(Math.random() * highlightGame.length);
    const searchResult = { collections, games, highlightGame: highlightGame[rand] };
    resolve(searchResult);
  });
};

export const searchCompaniesRequest = async (query, offset) => {
  return new Promise(async (resolve, reject) => {
    const companies = await getDataRequest("companies", `fields ${companieFields};${query}; limit 25; offset ${offset ? offset : 0};`);
    resolve(companies);
  });
};
