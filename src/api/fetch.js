import axios from 'axios';

const BASE_URL = 'https://api-v3.igdb.com/';
const USER_KEY = '21d29cc297bbf1bf047a0a840b69fdfc';

export const getDataRequest = async (endPoint, query) => {
  const headers = {
    Accept: 'application/json',
    'user-key': USER_KEY,
  };

  return await axios({
    url: `${BASE_URL}${endPoint}`,
    method: 'POST',
    headers,
    data: query,
  })
    .then(response => {
      // console.log(response.data);
      return response.data;
    })
    .catch(err => {
      console.error('Request Error', err);
      return error;
    });
};
