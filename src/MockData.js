export const imageUrisMock = [
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1re6.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1m55.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1n24.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1n1f.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1n24.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
  'https://images.igdb.com/igdb/image/upload/t_cover_big/co1lyv.jpg',
];

export const imageUrisMock2 = ['', '', '', '', '', '', '', '', '', '', '', ''];

export const sectionsMock = [
  {
    title: 'Popular right now',
    data: imageUrisMock2,
  },
  {
    title: 'My list',
    data: imageUrisMock2,
  },
  {
    title: 'Recently reviwed',
    data: imageUrisMock2,
  },
  {
    title: 'Top rating games',
    data: imageUrisMock2,
  },
  {
    title: 'Action games',
    data: imageUrisMock2,
  },
  {
    title: 'Platform games',
    data: imageUrisMock2,
  },
  {
    title: 'Shooters games',
    data: imageUrisMock2,
  },
];
