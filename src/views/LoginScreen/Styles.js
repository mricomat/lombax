import { StyleSheet, Platform, StatusBar } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

import Utils from "../../utils/Utils";

export default (styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
    backgroundColor: Utils.createRGBWithOpacity("#0E0E0E", 1),
  },

  layContainer: {
    width: "100%",
    height: "100%",
    position: "absolute",
    top: 0,
    left: 0,
  },

  safeAreaContainer: {
    flex: 1,
    alignItems: "center",
    zIndex: 2,
  },
  wrappAccess: {
    width: "100%",
    alignItems: "center",
  },
  backgroundOpacity: {
    width: "100%",
    height: "100%",
    position: "absolute",
    top: 0,
    left: 0,
    backgroundColor: "black",
    opacity: 0.3,
  },
  backdropVideo: {
    width: "120%",
    height: "140%",
    alignContent: "center",
    position: "absolute",
    top: -136,
    left: 0,
  },
  titleText: {
    textAlign: "center",
    fontSize: 32,
    fontFamily: "Roboto-Bold",
    width: "85%",
    paddingVertical: 35,
    color: "white",
  },
  descriptiontext: {
    textAlign: "center",
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    width: "85%",
    color: "white",
    marginBottom: 10,
  },
  textInput: {
    width: "85%",
    height: 40,
    borderRadius: 22.5,
    marginTop: 12,
    paddingVertical: 7,
    paddingHorizontal: 20,
    backgroundColor: "#2b2a2a",
    color: "white",
  },
  rememberUserContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "85%",
    marginVertical: 10,
  },
  buttonAccess: {
    width: "85%",
    height: 45,
    borderRadius: 22.5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#1169c6",
    marginTop: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
  },

  accessText: {
    fontSize: 14,
    color: "#fff",
  },
  recoverText: {
    fontSize: 14,
    color: "grey",
    marginTop: 20,
  },
  appIcon: {
    width: 60,
    height: 90,
    zIndex: 2,
    marginTop: 70,
  },
}));
