import React from "react";
import { Image, SafeAreaView, Text, TextInput, TouchableOpacity, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Video from "react-native-video";
import { connect } from "react-redux";
import { RNToasty } from "react-native-toasty";

import Navigation from "../../routes/Navigation";
import LoadingButton from "../../components/LoadingButton";
import { loginRequest as loginRequestAction } from "../../stores/login/LoginActions";
import styles from "./styles";

const toastStyle = {
  fontFamily: "Roboto-Regular",
  titleSize: 13,
  duration: 1,
};

class LoginScreen extends React.Component {
  state = {
    user: "",
    pass: "",
  };

  _navigateRegisterScreen = () => {
    Navigation.navigate("RegisterScreen", {});
  };

  _login = () => {
    const { user, pass } = this.state;
    if (user != "" && pass != "") {
      const { loginRequest } = this.props;
      const credentials = { username: user, password: pass };
      loginRequest(credentials);
    } else {
      this.loadingButton.onError();
      RNToasty.Error({
        title: "Username or Password can't be empty",
        ...toastStyle,
      });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    const { error } = this.props;
    if (prevProps.error !== error) {
      if (error) {
        if (error.status === "NOT_FOUND") {
          this.loadingButton.onError();
          RNToasty.Error({
            title: "That username doesn't exist, please try again or register",
            ...toastStyle,
          });
        } else if (error.status == "401") {
          this.loadingButton.onError();
          RNToasty.Error({
            title: "Your crendentials are wrong, please try again",
            ...toastStyle,
          });
        }
      }
    }

    if (prevProps.user !== this.props.user) {
      this.loadingButton.onSuccess();
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Video
          repeat={true}
          muted={true}
          resizeMode={"cover"}
          source={require("../../assets/background.mp4")}
          ref={ref => {
            this.player = ref;
          }}
          style={styles.backdropVideo}
        />
        <View style={styles.backgroundOpacity} />
        <View style={styles.layContainer}>
          <SafeAreaView style={styles.safeAreaContainer}>
            <Image style={styles.appIcon} source={require("../../assets/ice.png")} />
            <KeyboardAwareScrollView style={{ width: "100%" }}>
              <View style={styles.wrappAccess}>
                <Text style={styles.titleText}>Sign in</Text>
                <Text style={styles.descriptiontext}>{"Please sign in to start sharing \n all your game experience"}</Text>

                <TextInput
                  autoFocus={false}
                  autoCorrect={false}
                  onChangeText={user => {
                    this.setState({ user, errorMessage: "" });
                    this.loadingButton.cleanError();
                  }}
                  keyboardAppearance="dark"
                  placeholder={"User"}
                  placeholderTextColor={"grey"}
                  style={styles.textInput}
                />

                <TextInput
                  onChangeText={pass => {
                    this.setState({ pass, errorMessage: "" });
                    this.loadingButton.cleanError();
                  }}
                  autoFocus={false}
                  autoCorrect={false}
                  keyboardAppearance="dark"
                  placeholder={"Password"}
                  secureTextEntry={true}
                  placeholderTextColor={"grey"}
                  style={styles.textInput}
                />

                <LoadingButton
                  ref={ref => {
                    this.loadingButton = ref;
                  }}
                  onPress={() => this._login()}
                  title="Sign in"
                />

                <View style={{ flexDirection: "row" }}>
                  <Text style={styles.recoverText}>Have an account? </Text>
                  <TouchableOpacity onPress={() => this._navigateRegisterScreen()}>
                    <Text style={[styles.recoverText, { color: "white" }]}> Register</Text>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity>
                  <Text style={styles.recoverText}>Forgot password?</Text>
                </TouchableOpacity>
              </View>
            </KeyboardAwareScrollView>
          </SafeAreaView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  failure: state.login.failure,
  error: state.login.error,
  isFetching: state.login.isFetching,
  user: state.login.user,
});

const mapDispatchToProps = dispatch => ({
  loginRequest: params => dispatch(loginRequestAction(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen);
