import React from "react";
import { View, StyleSheet, StatusBar, Text, Dimensions, TextInput, TouchableOpacity } from "react-native";
import Icon from "react-native-ionicons";
import FastImage from "react-native-fast-image";
import { AirbnbRating } from "react-native-ratings";
import RNShineButton from "react-native-shine-button";

import styles from "./styles";
import Navigation from "../../routes/Navigation";

class AddReviewScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  _renderHeader = () => {
    return (
      <View style={styles.headerContainer}>
        <StatusBar backgroundColor={"black"} translucent={false} />
        <TouchableOpacity style={{ marginStart: 20 }} onPress={()=> this.props.navigation.pop()}>
          <Icon name="close" color={"white"} size={25} />
        </TouchableOpacity>

        <Text style={styles.headerTitle}>I Played</Text>

        <TouchableOpacity style={{ flex: 1 }}>
          <View style={{ alignSelf: "flex-end", marginEnd: 20 }}>
            <Icon name="checkmark" color={"white"} size={25} />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  _renderGameContainer = () => {
    return (
      <View style={styles.gameContainer}>
        <View style={styles.titleContainer}>
          <Text style={styles.titleGame}>Wacraft 3</Text>
          <Text style={styles.titleGameDate}>2020</Text>
        </View>
      </View>
    );
  };

  _renderRateContainer = () => {
    return (
      <View style={styles.rateContainer}>
        <View style={styles.ratingsContainer}>
          <AirbnbRating showRating={false} count={5} defaultRating={11} size={30} starStyle={{ borderRadius: 50 }} />
          <Text style={styles.ratingTitle}>Rate</Text>
        </View>
        <View style={styles.likeContainer}>
          <RNShineButton shape={<Icon name="heart" color={"white"} size={40} />} color={"#C1c1c1"} fillColor={"#D75A4A"} size={30} />
          <Text style={styles.likeTitle}>Like</Text>
        </View>
      </View>
    );
  };

  _renderDateContainer = () => {
    return (
      <View style={styles.dateContainer}>
        <Text style={styles.dateTitle}>Viernes 13 de Marzo, 2020</Text>
        <TouchableOpacity style={{ marginStart: 12 }}>
          <Icon name="ios-close-circle-outline" color={"white"} size={22} />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this._renderHeader()}
        {this._renderGameContainer()}
        <View style={styles.separatorView} />

        {this._renderDateContainer()}
        <View style={styles.separatorView} />
        <View style={styles.gameCoverContainer}>
          <FastImage
            style={styles.gameCover}
            source={{
              uri: "https://images.igdb.com/igdb/image/upload/t_cover_big/co1re6.jpg",
              priority: FastImage.priority.high,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </View>

        {this._renderRateContainer()}
        <View style={styles.separatorView} />
        <View style={styles.reviewContainer}>
          <TextInput style={styles.reviewInput} placeholder={"Add review..."} placeholderTextColor={"grey"} />
        </View>
        <View style={styles.separatorView} />

        <View style={styles.replayContainer}>
          <TouchableOpacity style={{ alignItems: "center" }}>
            <Icon name="logo-game-controller-b" color={"white"} size={60} />
            <Text style={styles.replayTitle}>{"I've played this \n game before"}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ alignItems: "center" }}>
            <Icon name="logo-game-controller-b" color={"white"} size={60} />
            <Text style={styles.replayTitle}>{"I've played this \n game before"}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default AddReviewScreen;
