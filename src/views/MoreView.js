import React from 'react';
import {View} from 'react-native';

class MoreView extends React.Component {
  render() {
    return <View style={{flex: 1, backgroundColor: "purple"}}></View>;
  }
}

export default MoreView;
