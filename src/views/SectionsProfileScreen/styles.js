import { StyleSheet, Platform, StatusBar } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

const STATUS_BAR_HEIGHT = Platform.OS === "ios" ? (IS_IPHONE_X ? 44 : 20) : StatusBar.currentHeight;

export default styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#0E0E0E" },
  flatList: {
    flex: 1,
  },
  imageDiaryItem: {
    resizeMode: "stretch",
    width: 43,
    height: 55,
    borderRadius: 4,
    borderWidth: 0.4,
  },
  headerContainer: {
    height: 90 - STATUS_BAR_HEIGHT,
    backgroundColor: "black",
    width: "100%",
    alignItems: "center",
    flexDirection: "row",
  },
  icon: {
    marginStart: 15,
    marginTop: STATUS_BAR_HEIGHT,
  },
  headerTitle: {
    color: "white",
    fontSize: 18,
    marginStart: 30,
    fontFamily: "Roboto-Bold",
    marginTop: STATUS_BAR_HEIGHT,
  },
  dayBox: {
    width: 55,
    height: 55,
    borderWidth: 0.7,
    borderRadius: 5,
    borderColor: "#c1c1c1",
    marginStart: 15,
    marginEnd: 10,
    justifyContent: "center",
  },
  dayTitle: {
    color: "white",
    alignSelf: "center",
    fontFamily: "Roboto-Light",
    fontSize: 22,
    alignSelf: "center",
  },
  diaryContainerInfo: {
    marginStart: 10,
    alignSelf: "center",
    marginBottom: 3,
    height: 55,
    width: "100%",
    justifyContent: "center",
  },
  diaryContainer: {
    flexDirection: "row",
    marginTop: 13,
  },
  diaryTitle: {
    color: "white",
    alignSelf: "center",
    fontFamily: "Roboto-Bold",
    fontSize: 16,
  },
  diaryDate: {
    color: "#c1c1c1",
    alignSelf: "center",
    fontFamily: "Roboto-Medium",
    fontSize: 13,
    marginStart: 8,
    marginTop: 1,
  },
  diarySeparator: {
    position: "absolute",
    width: "100%",
    height: 1,
    opacity: 0.5,
    backgroundColor: "grey",
    bottom: -8,
  },
  summaryReview: {
    alignItems: "center",
    fontFamily: "Roboto-Regular",
    color: "#dadada",
    fontSize: 14,
    marginStart: 10,
    lineHeight: 19,
    width: "72%",
  },
  imageReview: {
    resizeMode: "stretch",
    width: "25%",
    height: 130,
    borderRadius: 3,
    borderWidth: 0.4,
    marginBottom: 10,
  },
  reviewContainer: {
    marginTop: 13,
    marginStart: 15,
    marginEnd: 15,
  },
});
