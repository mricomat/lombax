import React from "react";
import { FlatList, Text, View, TouchableOpacity } from "react-native";
import FastImage from "react-native-fast-image";
import Icon from "react-native-ionicons";

import ActivityBar from "../../components/ActivityBar";
import { getImageUrl } from "../../utils/imageUris";
import styles from "./styles";

class SectionsProfileScreen extends React.Component {
  constructor(props) {
    super(props);

    const section = props.navigation.getParam("section");
    this.state = { section };
  }

  // TODO Section MONTH YEAR
  _renderDiaryItem = ({ item, index }) => {
    return (
      <View style={styles.diaryContainer}>
        <View style={styles.dayBox}>
          <Text style={styles.dayTitle}>21</Text>
        </View>
        <FastImage
          style={styles.imageDiaryItem}
          source={{
            uri: getImageUrl("co1re6", "t_cover_big"),
            priority: FastImage.priority.normal,
          }}
          resizeMode={FastImage.resizeMode.cover}
        />
        <View style={styles.diaryContainerInfo}>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.diaryTitle}>Borderlands 3</Text>
            <Text style={styles.diaryDate}>2019</Text>
          </View>
          <ActivityBar rating={5} isReview={true} isRePl ayed={true} isLiked={true} />
          <View style={styles.diarySeparator} />
        </View>
      </View>
    );
  };

  _renderDirary = () => {
    const mockData = [{ date: "", data: { title: "", date: "", activity: "", coverId: "" } }];
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          ref={ref => (this.list = ref)}
          keyExtractor={item => Math.random(10000)}
          data={[1, 2, 3, 4, 5]}
          renderItem={this._renderDiaryItem}
          style={styles.flatList}
        />
      </View>
    );
  };

  _renderReviewItem = ({ item, index }) => {
    return (
      <View style={styles.reviewContainer}>
        <View style={{ marginBottom: 3, height: 55, justifyContent: "center" }}>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.diaryTitle}>Borderlands 3</Text>
            <Text style={styles.diaryDate}>2019</Text>
          </View>
          <ActivityBar rating={5} isReview={true} isRePl ayed={true} isLiked={true} />
        </View>
        <View style={{ flexDirection: "row" }}>
          <FastImage
            style={styles.imageReview}
            source={{
              uri: getImageUrl("co1re6", "t_cover_big"),
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
          <Text numberOfLines={7} style={styles.summaryReview}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.usmod tempor
            incididunt ut labore eusmod tempor incididunt ut labore eusmod tempor incididunt ut labore e
          </Text>
        </View>

        <View style={styles.diarySeparator} />
      </View>
    );
  };

  renderReviews = () => {
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          ref={ref => (this.list = ref)}
          keyExtractor={item => Math.random(10000)}
          data={[1, 2, 3, 4, 5]}
          renderItem={this._renderReviewItem}
          style={styles.flatList}
        />
      </View>
    );
  };

  _renderFollowItem = ({ item, index }) => {
    return (
      <View style={{ flexDirection: "row", marginTop: 16, marginStart: 15 }}>
        <FastImage
          style={{ resizeMode: "stretch", width: 40, height: 40, borderRadius: 50, borderWidth: 0.6, borderColor: "#c1c1c1" }}
          source={{
            uri: getImageUrl("co1re6", "t_cover_big"),
            priority: FastImage.priority.normal,
          }}
          resizeMode={FastImage.resizeMode.cover}
        />
        <Text style={{ color: "white", fontFamily: "Roboto-Regular", fontSize: 16, alignSelf: "center", marginStart: 15 }}>
          Martin Rico
        </Text>
      </View>
    );
  };

  renderFollows = () => {
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          ref={ref => (this.list = ref)}
          keyExtractor={item => Math.random(10000)}
          data={[1, 2, 3, 4, 5]}
          renderItem={this._renderFollowItem}
          style={styles.flatList}
        />
      </View>
    );
  };

  _renderSection = section => {
    switch (section.title) {
      case "Diary":
        return this._renderDirary();
      case "Reviews":
        return this.renderReviews();
      case "Likes":
        break;
      case "Following":
        return this.renderFollows();
      case "Followers":
        return this.renderFollows();
    }
  };

  render() {
    const { section } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Icon style={styles.icon} name={"arrow-back"} size={22} color={"#FFFFFF"} />
          </TouchableOpacity>

          <Text style={styles.headerTitle}>
            {"Martin's "}
            {section.title}
          </Text>
        </View>
        {this._renderSection(section)}
      </View>
    );
  }
}

export default SectionsProfileScreen;
