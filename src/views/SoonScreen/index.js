import ViewPager from "@react-native-community/viewpager";
import moment from "moment";
import React from "react";
import { Dimensions, FlatList, Image, ScrollView, Text, TouchableOpacity, TouchableWithoutFeedback, View } from "react-native";
import FastImage from "react-native-fast-image";
import Icon from "react-native-ionicons";
import LinearGradient from "react-native-linear-gradient";
import Animated from "react-native-reanimated";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

import { getAllUpComing, getMostAnticipated, getOneMonthLater, getSevenDaysLater } from "../../api/games";
import Navigation from "../../routes/Navigation";
import { getImageUrl } from "../../utils/imageUris";
import styles from "./Styles";
import LoadingView from "../../components/LoadingView";

const COLORS = ["rgba(0,0,0,0.00)", "rgba(26, 26, 26, 0.85)", "rgba(26, 26, 26, 0.95)", "rgba(26, 26, 26, 1)"];

const LOCATIONS = [0.5, 0.8, 0.9, 1];

const createRGBWithOpacity = (hex, a) => {
  if (!hex) return;

  let emptyHex = hex.substring(hex.indexOf("#") + 1);
  var r = parseInt(emptyHex.substring(0, 2), 16);
  var g = parseInt(emptyHex.substring(2, 4), 16);
  var b = parseInt(emptyHex.substring(4, 6), 16);

  r = isNaN(r) ? 0 : r;
  g = isNaN(g) ? 0 : g;
  b = isNaN(b) ? 0 : b;

  let rgba = `rgba(${r}, ${g}, ${b}, ${a})`;

  return rgba;
};

const getColorUp = () => {
  let arr = [0.9, 0.9, 0.7, 0.5, 0.5, 0.3, 0];
  var arrRGBA = [];
  let color = "#1A1A1A";

  for (var i = 0; i < arr.length; i++) {
    arrRGBA.push(createRGBWithOpacity(color, arr[i]));
  }
  return arrRGBA;
};

class SoonScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      mostAnticipated: [],
      sevenDays: [],
      oneMonth: [],
      allUpcoming: [],
    };
  }

  async componentDidMount() {
    const anticipated = await getMostAnticipated();
    const sevenDays = await getSevenDaysLater();
    const oneMonth = await getOneMonthLater();
    const allUpcoming = await getAllUpComing();
    this.setState({
      mostAnticipated: anticipated,
      sevenDays: sevenDays,
      oneMonth: oneMonth,
      allUpcoming: allUpcoming,
      loading: false,
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.mostAnticipated != this.state.mostAnticipated) {
      return true;
    }
    if (nextState.oneMonth != this.state.oneMonth) {
      return true;
    }
    return false;
  }

  navigateToMosaicScreen = item => {
    Navigation.navigate("MosaicScreen", { offset: item.games.length - 1, wService: wService, section: item });
  };

  navigateToDetail = game => {
    Navigation.navigate("DetailGameView", {
      game,
    });
  };

  tagWidth = tag => {
    const width = tag.length * wp("2.40%");
    if (tag.length <= 4) {
      return width + 20;
    }

    if (tag.length >= 9) {
      return width - 9;
    }
    return width;
  };

  _renderPage = game => {
    const wordsList = [...(game.genres ? game.genres : []), ...(game.themes ? game.themes : [])];
    const screenShot = game.screenshots ? game.screenshots[0] : null;
    const cover = game.cover;
    let lengthWords = 0;

    return (
      <View style={styles.slide}>
        <Image
          source={{
            uri: getImageUrl(screenShot ? screenShot.image_id : cover.image_id, "t_screenshot_big"),
            priority: FastImage.priority.high,
          }}
          style={styles.backgroundImage}
          blurRadius={0.1}
        />

        <LinearGradient colors={getColorUp()} style={styles.linearGradientTop}></LinearGradient>
        <LinearGradient locations={LOCATIONS} colors={COLORS} style={styles.linearGradientAll}></LinearGradient>

        <View style={styles.slideInfoContainer}>
          <View style={styles.titleGameContainer}>
            <Text style={styles.titleGame}>{game.name}</Text>
            <Text style={styles.dateGame}>{moment.unix(game.first_release_date).format("MMMM Do, YYYY")}</Text>
          </View>

          <View style={{ flexDirection: "row", flex: 1 }}>
            <Image
              source={{
                uri: getImageUrl(cover ? cover.image_id : "", "t_cover_big"),
                priority: FastImage.priority.normal,
              }}
              style={styles.coverGame}
            />
            <View style={{ flexDirection: "column", alignItems: "center", flex: 1, marginStart: 10 }}>
              <Text style={styles.summaryText} numberOfLines={4}>
                {game.summary}
              </Text>
              <View style={{ flexDirection: "row" }}>
                {wordsList
                  ? wordsList.map((word, index) => {
                      let wordName = word.name;
                      if (wordName.length > 15) {
                        wordName = wordName.substring(0, 10) + "...";
                      }

                      if (index < 4 && lengthWords < 25) {
                        lengthWords = lengthWords + wordName.length;
                        return (
                          <View style={styles.containerKeyWord} key={index}>
                            <Text style={styles.labelKeyWord}>{wordName}</Text>
                            {index != wordsList.length - 1 && lengthWords < 25 ? <View style={styles.dot} /> : null}
                          </View>
                        );
                      }
                    })
                  : null}
              </View>

              <View style={{ flexDirection: "row" }}>
                {game.platforms
                  ? game.platforms.map((platform, index) => {
                      const tag = platform.abbreviation ? platform.abbreviation : platform.name;
                      if (index == 4) return;
                      return (
                        <TouchableOpacity
                          style={[
                            {
                              overflow: "hidden",
                              justifyContent: "center",
                              backgroundColor: "black",
                              width: 65,
                              height: 27,
                              borderRadius: 6,
                              marginTop: 10,
                              marginEnd: 10,
                            },
                            { width: this.tagWidth(index == 3 ? "+" : tag) },
                          ]}>
                          <Image
                            resizeMode={"contain"}
                            style={{
                              width: 280,
                              height: 280,
                              transform: [{ rotate: "-50deg" }],
                              alignSelf: "center",
                              position: "absolute",
                              opacity: 0.85,
                            }}
                            source={require("../../assets/ice.png")}
                          />
                          <Text style={{ fontFamily: "Roboto-Bold", fontSize: 12, color: "white", alignSelf: "center" }}>
                            {index == 3 ? "+" : tag}
                          </Text>
                        </TouchableOpacity>
                      );
                    })
                  : null}
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };

  _renderSlide = ({ item, index }) => {
    const wordsList = [...(item.genres ? item.genres : []), ...(item.themes ? item.themes : [])];
    const screenShot = item.screenshots ? item.screenshots[0] : null;
    const cover = item.cover;
    let lengthWords = 0;

    return (
      <View style={styles.slide}>
        <Image
          style={styles.backgroundSlide}
          source={{
            uri: getImageUrl(screenShot ? screenShot.image_id : cover.image_id, "t_screenshot_med"),
            priority: FastImage.priority.normal,
          }}
          resizeMode={FastImage.resizeMode.cover}
          blurRadius={0.1}
        />
        <Text style={styles.titleSlide}>{item.name}</Text>
        <View style={{ flexDirection: "row", flex: 1, marginTop: 5 }}>
          <TouchableWithoutFeedback onPress={() => this.navigateToDetail(item)}>
            <FastImage
              style={styles.coverSlide}
              source={{
                uri: getImageUrl(cover ? cover.image_id : "", "t_cover_big"),
                priority: FastImage.priority.normal,
              }}
              resizeMode={FastImage.resizeMode.cover}
            />
          </TouchableWithoutFeedback>

          <View style={{ flexDirection: "column", marginStart: 10, alignItems: "center", flex: 1 }}>
            <Text style={styles.summaryText} numberOfLines={4}>
              {item.summary}
            </Text>
            <View style={{ flexDirection: "row" }}>
              {wordsList
                ? wordsList.map((word, index) => {
                    let wordName = word.name;
                    if (wordName.length > 15) {
                      wordName = wordName.substring(0, 10) + "...";
                    }

                    if (index < 4 && lengthWords < 25) {
                      lengthWords = lengthWords + wordName.length;
                      return (
                        <View style={styles.containerKeyWord} key={index}>
                          <Text style={styles.labelKeyWord}>{wordName}</Text>
                          {index != wordsList.length - 1 && lengthWords < 25 ? <View style={styles.dot} /> : null}
                        </View>
                      );
                    }
                  })
                : null}
            </View>

            <View style={styles.bottomSlideContainer}>
              <Text style={styles.dateText}>{moment.unix(item.first_release_date).format("MMMM Do, YYYY")}</Text>
              <TouchableOpacity style={styles.buttonContainer}>
                <Icon name={"play"} size={19} color={"#000000"} />
                <Text style={styles.textButton}>Trailer</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };

  _renderItem = ({ item, index }) => {
    if (item.cover) {
      return (
        <TouchableWithoutFeedback onPress={() => this.navigateToDetail(item)}>
          <FastImage
            style={styles.imageItem}
            source={{
              uri: getImageUrl(item.cover.image_id, "t_cover_big"),
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </TouchableWithoutFeedback>
      );
    }
  };

  _renderSection = (title, data) => {
    return (
      <View>
        <View style={{ flexDirection: "row", alignContent: "center", marginTop: 15 }}>
          <Text style={styles.sectionTitle}>{title}</Text>

          <TouchableOpacity>
            <Icon
              style={{ alignSelf: "center", marginStart: 8, marginBottom: 2 }}
              name={"add-circle-outline"}
              size={22}
              color={"#FFFFFF"}
            />
          </TouchableOpacity>
        </View>

        <FlatList
          extraData={this.state}
          horizontal={true}
          ref={ref => (this.list = ref)}
          keyExtractor={item => item.id}
          data={data}
          renderItem={this._renderItem}
          style={styles.flatList}
        />
      </View>
    );
  };

  render() {
    const { mostAnticipated, sevenDays, oneMonth, allUpcoming, loading } = this.state;

    if (loading) {
      return <LoadingView />;
    }

    return (
      <View style={styles.container}>
        <ScrollView>
          {mostAnticipated ? (
            <ViewPager style={styles.slide} initialPage={0} loop>
              {mostAnticipated.map((game, index) => {
                return <View key="1">{this._renderPage(game)}</View>;
              })}
            </ViewPager>
          ) : null}

          <View style={{ marginTop: -50 }}>
            {this._renderSection("Upcoming in 7 days", sevenDays)}
            {this._renderSection("Upcoming in 1 month", oneMonth)}
            {this._renderSection("All Upcoming", allUpcoming)}
          </View>
          <View style={styles.logoContainer}>
            <Image resizeMode={"contain"} style={{ width: 50, height: 50 }} source={require("../../assets/ice.png")} />
            <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
              <Text style={styles.gamesLabel}>Most anticipated</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default SoonScreen;
