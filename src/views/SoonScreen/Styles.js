import { StyleSheet, StatusBar, Dimensions } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
const STATUS_BAR_HEIGHT = StatusBar.currentHeight;

const { height } = Dimensions.get("window");

const HEIGTH_BOTTOM_BAR = 30;
const IMAGE_HEIGTH = height / 2 + HEIGTH_BOTTOM_BAR;

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#191919",
    width: "100%",
  },
  titleText: {
    alignSelf: "center",
    fontFamily: "Roboto-Bold",
    fontSize: 50,
    marginTop: 60,
    marginBottom: 20,
    color: "white",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  sectionTitle: {
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    marginStart: 8,
    fontFamily: "Roboto-Bold",
    fontSize: 15.5,
    color: "white",
  },
  backgroundImage: {
    position: "absolute",
    width: "100%",
    height: IMAGE_HEIGTH,
    backgroundColor: "#191919",
    resizeMode: "cover",
  },
  slide: {
    width: "100%",
    height: IMAGE_HEIGTH,
  },
  backgroundSlide: {
    width: "100%",
    height: "100%",
    position: "absolute",
    opacity: 0.6,
  },
  titleGameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
  },
  logoContainer: {
    position: "absolute",
    alignItems: "center",
    flexDirection: "row",
    marginStart: wp("6%"),
    marginTop: 32,
  },
  gamesLabel: {
    fontFamily: "Roboto-Regular",
    fontSize: wp("4%"),
    marginStart: wp("3%"),
    color: "white",
  },

  linearGradientAll: {
    position: "absolute",
    width: "100%",
    height: IMAGE_HEIGTH,
  },
  linearGradientTop: {
    position: "absolute",
    width: "100%",
    height: 100,
  },
  slideInfoContainer: {
    position: "absolute",
    width: "100%",
    bottom: 0,
    padding: 10,
    marginBottom: 35,
    justifyContent: "flex-end",
  },
  titleGame: {
    fontFamily: "Roboto-Bold",
    fontSize: 21,
    alignSelf: "flex-start",
    color: "white",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  dateGame: {
    fontFamily: "Roboto-Bold",
    fontSize: 14,
    color: "white",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },

  summaryText: {
    alignSelf: "flex-start",
    fontFamily: "Roboto-Regular",
    fontSize: 14,
    width: wp(67),
    color: "white",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  coverGame: {
    width: wp(24),
    height: hp(20),
    borderRadius: 4,
    borderWidth: 0.2,
    borderColor: "grey",
  },

  flatList: {
    marginTop: 5,
  },
  containerKeyWord: {
    flexDirection: "row",
    marginTop: 3,
    alignSelf: "center",
  },
  labelKeyWord: {
    textTransform: "capitalize",
    fontFamily: "Montserrat-Medium",
    alignSelf: "center",
    fontSize: 12,
    color: "white",
  },
  dot: {
    alignSelf: "center",
    width: 4.5,
    height: 4.5,
    borderRadius: 100 / 2,
    backgroundColor: "#144D93",
    marginStart: 5,
    marginEnd: 5,
  },

  imageItem: {
    resizeMode: "stretch",
    width: wp("28"),
    height: hp("24%"),
    marginStart: 5,
    borderRadius: 4,
  },

  headerContainer: {
    alignItems: "center",
    position: "absolute",
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 32,
  },
});
