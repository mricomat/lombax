import { StyleSheet, StatusBar, Platform } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
const STATUS_BAR_HEIGHT = StatusBar.currentHeight;

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#191919",
    width: "100%",
  },
  headerContainer: {
    width: "100%",
    height: 60,
    backgroundColor: "#0F0F0F",
    justifyContent: "center",
  },
  headerTitle: {
    fontFamily: "Roboto-Bold",
    color: "white",
    marginTop: STATUS_BAR_HEIGHT - 5,
    fontSize: 16.5,
    marginStart: 15,
  },
  flatList: { marginTop: 10 },
  reviewContainer: {
    alignSelf: "center",
    width: wp(95),
    height: hp(27.6),
    marginBottom: 10,
    backgroundColor: "#191919",
    borderRadius: 5,

    shadowOffset: { width: 10, height: 10 },
    shadowColor: "black",
    shadowOpacity: 1,
    elevation: 8,
  },
  headerReviewContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginStart: 10,
    marginTop: 5,
  },
  titleReview: {
    fontFamily: "Roboto-Bold",
    color: "white",
    fontSize: 16,
  },
  timeGameReview: {
    fontFamily: "Roboto-Medium",
    color: "white",
    marginTop: 2,
    marginStart: 6,
    fontSize: 13,
  },
  userReviewContainer: {
    alignItems: "flex-end",
  },
  userTitle: {
    fontFamily: "Roboto-Medium",
    color: "white",
    fontSize: 13,
  },
  timeReview: {
    fontFamily: "Roboto-Medium",
    color: "white",
    fontSize: 13,
    lineHeight: 15,
  },
  profileImage: {
    marginStart: 6,
    borderRadius: 20,
    width: 35,
    height: 35,
  },
  contentReviewContainer: {
    flexDirection: "row",
    marginTop: 7,
    marginStart: 10,
  },
  coverImage: {
    borderRadius: 5,
    borderColor: "white",
    borderWidth: 0.2,
    width: wp(23),
    height: hp(19),
  },
  summaryReview: {
    fontFamily: "Roboto-Regular",
    color: "white",
    fontSize: 13,
    marginStart: 10,
    width: wp(65),
    lineHeight: 15,
  },
});
