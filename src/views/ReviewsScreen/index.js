import React from "react";
import { View, ScrollView, FlatList, Text, StatusBar, TouchableOpacity } from "react-native";
import FastImage from "react-native-fast-image";
import Icon from "react-native-ionicons";

import Navigation from "../../routes/Navigation";
import styles from "./styles";

const review =
  "Lorem ipsum dolor sit amet consectetur adipiscing elit auctor inceptos, varius sociosqu taciti commodolibero quam massa fames vitae, id est ante lectuspellentesque dis potenti felis...Lorem ipsum dolorsit amet consectetur adipiscing elit auctor inceptos,varius sociosqu taciti commodo libero quam massafames vitae, id est ante lectus pellentesque dis ";
class ReviewsScreen extends React.Component {
  navigateToDetail = () => {
    Navigation.navigate("ReviewDetailScreen", {});
  };

  _navigateToProfile = () => {
    Navigation.navigate("ProfileScreen", { isOtherUser: true });
  };

  _renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={styles.reviewContainer}
        cardElevation={8}
        cardMaxElevation={8}
        cornerRadius={5}
        onPress={this.navigateToDetail}>
        <View style={styles.headerReviewContainer}>
          <View>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Text style={styles.titleReview}>Hollow Knight</Text>
              <Text style={styles.timeGameReview}>2019</Text>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Icon name={"star"} size={16} color={"#A78864"} />
              <Icon name={"star"} size={16} color={"#A78864"} />
              <Icon name={"star"} size={16} color={"#A78864"} />
              <Icon name={"star"} size={16} color={"#A78864"} />
              <Icon name={"star"} size={16} color={"#A78864"} />
              <Icon style={{ marginTop: 2, marginStart: 5 }} name={"heart"} size={14} color={"#D75A4A"} />
            </View>
          </View>
          <View style={{ flexDirection: "row", marginEnd: 6, marginTop: 3 }}>
            <View style={styles.userReviewContainer}>
              <Text style={styles.userTitle}>Martin Rico</Text>
              <Text style={styles.timeReview}>November 10, 2019</Text>
            </View>
            <TouchableOpacity onPress={this._navigateToProfile}>
              <FastImage
                style={styles.profileImage}
                source={{
                  uri: "https://images.igdb.com/igdb/image/upload/t_cover_big/co1h9f.jpg",
                }}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.contentReviewContainer}>
          <FastImage
            style={styles.coverImage}
            source={{
              uri: "https://images.igdb.com/igdb/image/upload/t_cover_big/co1h9f.jpg",
            }}
          />
          <Text style={styles.summaryReview} numberOfLines={8}>
            {review}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {/* <StatusBar translucent={true} /> */}
        <View style={styles.headerContainer}>
          <Text style={styles.headerTitle}>Reviews</Text>
        </View>
        <ScrollView>
          <FlatList
            ref={ref => (this.list = ref)}
            keyExtractor={item => item}
            data={[1, 2, 3, 4, 5]}
            renderItem={this._renderItem}
            style={styles.flatList}
          />
        </ScrollView>
      </View>
    );
  }
}

export default ReviewsScreen;
