import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Image,
  ScrollView
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import moment from 'moment';

import TagsGroup from '../../../components/TagsGroup';
import {getGamesByIds} from '../../../api/games';
import {getImageUrl} from '../../../utils/imageUris';
import Navigation from '../../../routes/Navigation';
import Platforms from '../../../constants/Platforms';

class DetailTab extends React.Component {
  constructor(props) {
    super(props);

    const gameData = this.props.gameData;

    this.state = {
      gameData,
      dlcs: [],
      expansions: [],
      isLoading: true,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    const {expansions, dlcs, isLoading} = this.state;

    if (nextState.expansions != expansions) {
      return true;
    }

    if (nextState.dlcs != dlcs) {
      return true;
    }

    if (nextState.isLoading != isLoading) {
      return true;
    }

    return false;
  }

  async componentDidMount() {
    const {gameData, dlcs, expansions} = this.state;
    let expansionsResult = [];
    let dlcsResult = [];

    if (dlcs.length <= 0 && gameData.dlcs) {
      dlcsResult = await getGamesByIds(gameData.dlcs);
    }

    if (expansions.length <= 0 && gameData.expansions) {
      expansionsResult = await getGamesByIds(gameData.expansions);
    }

    this.setState({
      isLoading: false,
      expansions: expansionsResult,
      dlcs: dlcsResult,
    });
  }

  navigateToDetail = game => {
    Navigation.navigate('DetailGameView', {
      game,
    });
  };

  renderItem = ({item}) => {
    const cover = item.data.cover;

    if (cover) {
      return (
        <TouchableWithoutFeedback
          onPress={() => this.navigateToDetail(item.data)}>
          <View>
            <FastImage
              style={styles.imageItem}
              source={{
                uri: getImageUrl(cover.image_id, 't_cover_big'),
                priority: FastImage.priority.high,
              }}
              resizeMode={FastImage.resizeMode.cover}
            />
          </View>
        </TouchableWithoutFeedback>
      );
    }
  };

  renderCovers = (data, title) => {
    if (data.length > 0) {
      return (
        <View style={{flexDirection: 'column', flex: 1}}>
          <Text
            style={{
              marginTop: 10,
              fontFamily: 'Roboto-Medium',
              fontSize: hp('2.4%'),
              color: '#dadada',
            }}>
            {title}
          </Text>
          <FlatList
            horizontal={true}
            ref={ref => (this.list = ref)}
            keyExtractor={item => item.id}
            data={data}
            maxToRenderPerBatch={15}
            renderItem={this.renderItem}
            style={styles.flatList}
          />
        </View>
      );
    }
  };

  render() {
    const {gameData, isLoading} = this.state;

    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.tagsContainer}>
            {gameData.platforms ? (
              <TagsGroup
                title={'Platforms: '}
                tags={gameData.platforms}
                alignItems={'flex-start'}
              />
            ) : null}
            {gameData.genres ? (
              <TagsGroup
                title={'Genres: '}
                tags={gameData.genres}
                alignItems={'flex-end'}
              />
            ) : null}
          </View>

          <View style={styles.tagsContainer}>
            {gameData.game_modes ? (
              <TagsGroup
                title={'Games modes: '}
                tags={gameData.game_modes}
                alignItems={'flex-start'}
                columns={1}
              />
            ) : null}

            {gameData.themes ? (
              <TagsGroup
                title={'Themes: '}
                tags={gameData.themes}
                alignItems={'flex-end'}
              />
            ) : null}
          </View>

          <View
            style={{
              marginStart: -20,
              marginTop: 10,
              height: 2,
              width: 500,
              backgroundColor: '#131212',
            }}
          />

          {this.renderCovers(this.state.dlcs, 'DLCS')}
          {this.renderCovers(this.state.expansions, 'Expansion')}

          <Text
            style={{
              marginTop: 10,
              fontFamily: 'Roboto-Medium',
              fontSize: hp('2.4%'),
              color: '#dadada',
            }}>
            Release Dates:
          </Text>

          {gameData.release_dates && gameData.release_dates.map(item => {
            return (
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={{
                    marginTop: 3,
                    fontFamily: 'Roboto-Regular',
                    fontSize: hp('2%'),
                    color: '#dadada',
                  }}>
                  {moment.unix(item.date).format('MMMM Do, YYYY')} -{' '}
                </Text>
                <Text
                  style={{
                    marginTop: 3,
                    fontFamily: 'Roboto-Medium',
                    fontSize: hp('2%'),
                    color: '#dadada',
                  }}>
                  {
                    Platforms.find(platform => platform.id == item.platform)
                      .name
                  }
                </Text>
              </View>
            );
          })}

          <Text
            style={{
              marginTop: 10,
              fontFamily: 'Roboto-Medium',
              fontSize: hp('2.4%'),
              color: '#dadada',
            }}>
            Developers:
          </Text>

          <Text
            style={{
              marginTop: 3,
              fontFamily: 'Roboto-Regular',
              fontSize: hp('2.2%'),
              color: '#dadada',
            }}>
            Pubg Corp
          </Text>

          <Text
            style={{
              marginTop: 10,
              fontFamily: 'Roboto-Medium',
              fontSize: hp('2.4%'),
              color: '#dadada',
            }}>
            Publishers:
          </Text>

          <Text
            style={{
              marginTop: 3,
              fontFamily: 'Roboto-Regular',
              fontSize: hp('2.2%'),
              color: '#dadada',
            }}>
            Microsoft Studios
          </Text>
          <Text
            style={{
              marginTop: 3,
              fontFamily: 'Roboto-Regular',
              fontSize: hp('2.2%'),
              color: '#dadada',
            }}>
            BlueHole Studio
          </Text>

          <Text
            style={{
              marginTop: 10,
              fontFamily: 'Roboto-Medium',
              fontSize: hp('2.4%'),
              color: '#dadada',
            }}>
            Game engine:
          </Text>

          <Text
            style={{
              marginTop: 3,
              fontFamily: 'Roboto-Regular',
              fontSize: hp('2.2%'),
              color: '#dadada',
            }}>
            Unreal engine 4
          </Text>
        </View>
      </ScrollView>
    );
  }
}

export default DetailTab;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tagsContainer: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 0,
    marginRight: 1,
    justifyContent: 'space-between',
  },
  flatList: {
    flex: 1,
    marginStart: 3,
    marginTop: 10,
  },
  imageItem: {
    resizeMode: 'stretch',
    width: wp('28%'),
    height: hp('24%'),
    marginEnd: 5,
  },
});
