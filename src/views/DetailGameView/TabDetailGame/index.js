import React from 'react';
import {View, StyleSheet, Dimensions, FlatList} from 'react-native';
import {TabView, TabBar, SceneMap} from 'react-native-tab-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import DetailTab from './DetailTab';
import SimilarsTab from './SimilarsTab';
import MultimediaTab from './MultimediaTab';

const ThirthRoute = () => (
  <View style={[styles.scene, {backgroundColor: '#191919'}]} />
);

const FourthRoute = () => (
  <View style={[styles.scene, {backgroundColor: '#191919'}]} />
);

class TabDetailGame extends React.Component {
  state = {
    index: 0,
    routes: [
      {key: 'detail', title: 'Details'},
      {key: 'similars', title: 'Similars'},
      {key: 'reviews', title: 'Reviews'},
      {key: 'multimedia', title: 'Multimedia'},
    ],
  };

  shouldComponentUpdate(nextProps, nextState) {
    const {index} = this.state;

    if (nextState.index != index) {
      return true;
    }

    return false;
  }

  renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled={true}
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      labelStyle={styles.label}
      tabStyle={styles.tabStyle}
      activeColor={'#dadada'}
      inactiveColor={'#828282'}
    />
  );

  renderScene = ({route}) => {
    const gameData = this.props.game;
    switch (route.key) {
      case 'detail':
        return <DetailTab gameData={gameData} />;
      case 'similars':
        return <SimilarsTab similarsData={gameData.similar_games} />;
      case 'multimedia':
        return <MultimediaTab gameData={gameData} />;
      default:
        return null;
    }
  };

  render() {
    return (
      <TabView
        style={styles.tabView}
        navigationState={this.state}
        renderScene={this.renderScene}
        renderTabBar={this.renderTabBar}
        onIndexChange={index => this.setState({index})}
        initialLayout={{width: Dimensions.get('window').width}}
      />
    );
  }
}

const styles = StyleSheet.create({
  tabView: {
    marginStart:7,
    marginEnd: 7,
  },
  tabbar: {
    
   
    width: '100%',
    backgroundColor: '#191919',
  },
  tabStyle: {
    
    width: 'auto',
  },
  indicator: {
    backgroundColor: '#4E75AC',
    borderRadius: 5,
    height: 4,
  },
  label: {
    fontFamily: 'Roboto-Medium',
    fontSize: hp('2.1%'),
  },
  scene: {
    flex: 1,
  },
});
export default TabDetailGame;
