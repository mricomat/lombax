import React from 'react';
import {
  FlatList,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Text,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Navigation from '../../../routes/Navigation';

import {getGamesByIds} from '../../../api/games';
import {getImageUrl} from '../../../utils/imageUris';

export default class SimilarsTab extends React.Component {
  constructor(props) {
    super(props);
    this.similarIds = this.props.similarsData;
    this.state = {
      games: null,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.games !== this.state.games) {
      return true;
    }
    return false;
  }

  async componentDidMount() {
    if (this.similarIds && !this.state.games) {
      const similarGames = await getGamesByIds(this.similarIds);
      this.setState({games: similarGames});
    }
  }

  navigateToDetail = game => {
    Navigation.navigate('DetailGameView', {
      game,
    });
  };

  renderItem = ({item}) => {
    const cover = item.data.cover;
    if (cover && cover.image_id) {
      return (
        <Animatable.View
          animation="zoomInUp"
          useNativeDriver={true}
          duration={100}>
          <TouchableWithoutFeedback onPress={() => this.navigateToDetail(item)}>
            <FastImage
              style={styles.imageItem}
              source={{
                uri: getImageUrl(cover.image_id, 't_cover_big'),
                priority: FastImage.priority.high,
              }}
              resizeMode={FastImage.resizeMode.cover}
            />
          </TouchableWithoutFeedback>
        </Animatable.View>
      );
    }
  };

  render() {
    const games = this.state.games;
    return (
      <View style={{flex: 1}}>
        <FlatList
          ref={ref => (this.list = ref)}
          keyExtractor={item => item.id}
          data={games}
          numColumns={3}
          maxToRenderPerBatch={15}
          renderItem={this.renderItem}
          style={styles.flatList}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flatList: {
    flex: 1,
    marginTop: 10,
  },
  imageItem: {
    borderRadius: 4,
    margin: 3,
    width: wp('30%'),
    height: hp('24%'),
  },
});
