import React from "react";
import { FlatList, View, StyleSheet, TouchableWithoutFeedback, Text } from "react-native";
import * as Animatable from "react-native-animatable";
import FastImage from "react-native-fast-image";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

import { getGamesByIds, getVideosById, getArtWorksById } from "../../../api/games";
import { getImageUrl } from "../../../utils/imageUris";
import YoutubeVideoView from "../../../components/YoutubeVideoView";

export default class MultimediaTab extends React.Component {
  constructor(props) {
    super(props);
    this.gameData = this.props.gameData;
    this.state = {
      artworks: null,
      videos: null,
      screenshots: this.props.gameData.screenshots,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.artworks !== this.state.artworks) {
      return true;
    }

    if (nextState.screenshots !== this.state.screenshots) {
      return true;
    }

    if (nextState.videos !== this.state.videos) {
      return true;
    }

    return false;
  }

  async componentDidMount() {
    if (this.gameData.artworks && !this.state.artworks) {
      const artworks = await getArtWorksById(this.gameData.artworks);
      this.setState({ artworks: artworks });
    }

    if (this.gameData.videos && !this.state.videos) {
      const videoIds = [];
      this.gameData.videos.map(item => {
        videoIds.push(item.id);
      });
      const videos = await getVideosById(videoIds);
      this.setState({ videos: videoIds });
    }
  }

  renderScreenshots = ({ item }) => {
    if (item.image_id) {
      return (
        <Animatable.View animation="zoomInUp" useNativeDriver={true} duration={100}>
          <TouchableWithoutFeedback>
            <FastImage
              style={styles.screenshootItem}
              source={{
                uri: getImageUrl(item.image_id, "t_screenshot_big"),
                priority: FastImage.priority.high,
              }}
              resizeMode={FastImage.resizeMode.cover}
            />
          </TouchableWithoutFeedback>
        </Animatable.View>
      );
    }
  };

  renderArtworks = ({ item }) => {
    if (item.image_id) {
      return (
        <Animatable.View animation="zoomInUp" useNativeDriver={true} duration={100}>
          <TouchableWithoutFeedback>
            <FastImage
              style={styles.artworkItem}
              source={{
                uri: getImageUrl(item.image_id, "t_screenshot_big"),
                priority: FastImage.priority.high,
              }}
              resizeMode={FastImage.resizeMode.cover}
            />
          </TouchableWithoutFeedback>
        </Animatable.View>
      );
    }
  };

  renderVideos = ({ item }) => {
    console.log(item);
    if (item.video_id) {
      return (
        <View style={{ width: wp("90%") }}>
          <YoutubeVideoView idVideo={item.video_id} />
        </View>
      );
    }
  };

  render() {
    const { artworks, screenshots, videos } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Text
          style={{
            marginTop: 10,
            fontFamily: "Roboto-Medium",
            fontSize: hp("2.5%"),
            color: "#dadada",
          }}>
          Artworks
        </Text>
        <View style={{ height: hp("26%") }}>
          <FlatList
            horizontal={true}
            ref={ref => (this.list = ref)}
            keyExtractor={item => item.id}
            data={artworks}
            maxToRenderPerBatch={15}
            renderItem={this.renderArtworks}
            style={styles.flatList}
          />
        </View>

        <Text
          style={{
            marginTop: 10,
            fontFamily: "Roboto-Medium",
            fontSize: hp("2.5%"),
            color: "#dadada",
          }}>
          Screenshoots
        </Text>
        <View style={{ height: hp("26%") }}>
          <FlatList
            horizontal={true}
            ref={ref => (this.list = ref)}
            keyExtractor={item => item.id}
            data={screenshots}
            maxToRenderPerBatch={15}
            renderItem={this.renderScreenshots}
            style={styles.flatList}
          />
        </View>

        <Text
          style={{
            marginTop: 10,
            fontFamily: "Roboto-Medium",
            fontSize: hp("2.5%"),
            color: "#dadada",
          }}>
          Videos
        </Text>
        <View style={{ height: hp("26%") }}>
          <FlatList
            horizontal={true}
            ref={ref => (this.list = ref)}
            keyExtractor={item => item.id}
            z
            data={videos}
            maxToRenderPerBatch={15}
            renderItem={this.renderVideos}
            style={styles.flatList}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flatList: {
    marginTop: 10,
  },
  screenshootItem: {
    borderRadius: 4,
    margin: 3,
    width: wp("80%"),
    height: hp("26%"),
  },
  artworkItem: {
    borderRadius: 4,
    margin: 3,
    width: wp("50%"),
    height: hp("26%"),
  },
});
