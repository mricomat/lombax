import React from "react";
import { View, StyleSheet, StatusBar, Image, Text, Dimensions, TouchableOpacity } from "react-native";
import moment from "moment";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

import SkeletonLoader from "../../components/SkeletonLoader";
import { getImageUrl } from "../../utils/imageUris";
import HeaderDetailGame from "./HeaderDetailGame";

import GraphRating from "../../components/GraphRating";
import TabDetailGame from "./TabDetailGame";

class DetailGameView extends React.Component {
  constructor(props) {
    super(props);
    this.game = props.navigation.getParam("game");

    const gameCover = getImageUrl(this.game.cover ? this.game.cover.image_id : "", "t_cover_big");
    const gameScreenshoot = getImageUrl(
      this.game.screenshots ? this.game.screenshots[0].image_id : this.game.cover ? this.game.cover.image_id : "",
      "t_screenshot_med",
    );
    this.state = {
      headerGameScreenshot: gameScreenshoot,
      gameCover,
      data: this.game,
      loading: true,
    };

    console.log("DetailGameView", this.game);
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { gameCover, data, loading } = this.state;

    if (nextState.data != data) {
      return true;
    }

    if (nextState.gameCover != gameCover) {
      return true;
    }

    if (nextState.loading != loading) {
      return true;
    }

    return false;
  }

  async componentDidMount() {
    this.setState({
      loading: false,
    });
  }

  splitCompaniesNames = companies => {
    let text = "";
    if (companies) {
      companies.map((item, index) => {
        text = index != item.company.length - 1 ? text + item.company.name + ", " : text + item.company.name;
      });
    }
    return text;
  };

  renderContent = () => {
    const gameData = this.state.data;
    const companies = gameData.involved_companies;
    const releaseDate = gameData.first_release_date;

    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          marginTop: 5,
        }}>
        <View style={{ marginEnd: 8, marginStart: 16 }}>
          <Text style={styles.textContentTitle}>Developed by:</Text>
          <View
            style={{
              flexDirection: "column",
            }}>
            <View
              style={{
                flexDirection: "row",
              }}>
              <Text style={styles.textContentDetail}>{this.splitCompaniesNames(companies)}</Text>
            </View>
          </View>
          <Text style={styles.textContentTitle}>Release date:</Text>
          <Text style={styles.textReleaseDate}>{moment.unix(releaseDate).format("MMMM Do, YYYY")}</Text>

          {gameData.time_to_beat ? (
            <View>
              <Text style={styles.textContentTitle}>Time to beat:</Text>
              <Text style={styles.textContentDetail}>{moment.unix(gameData.time_to_beat.normally).format("HHmmss")}</Text>
            </View>
          ) : null}

          <Text numberOfLines={4} style={[styles.textSummary, { marginTop: !gameData.time_to_beat ? hp("2.8%") : hp("2%") }]}>
            {gameData.summary}
          </Text>
          <View style={{ flexDirection: "row", alignSelf: "center", marginTop: 10 }}>
            <View
              style={{
                marginRight: 3,
                borderRadius: 10,
                height: 4.5,
                width: 4.5,
                backgroundColor: "#dadada",
              }}
            />

            <View
              style={{
                marginRight: 3,
                borderRadius: 10,
                height: 4.5,
                width: 4.5,
                backgroundColor: "#dadada",
              }}
            />

            <View
              style={{
                marginRight: 3,
                borderRadius: 10,
                height: 4.5,
                width: 4.5,
                backgroundColor: "#dadada",
              }}
            />
          </View>
          <View
            style={{
              marginStart: -20,
              marginTop: 10,
              height: 2,
              width: 500,
              backgroundColor: "#131212",
            }}
          />

          <Text style={styles.textContentTitle}>Ratings</Text>
          <GraphRating rating={gameData.rating} />
        </View>
        <View
          style={{
            marginStart: -20,
            marginTop: 10,
            height: 2,
            width: 500,
            backgroundColor: "#131212",
          }}
        />

        <TabDetailGame game={gameData} />
      </View>
    );
  };

  render() {
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, backgroundColor: "#191919" }}>
          <SkeletonLoader />
        </View>
      );
    }
    const { headerGameScreenshot, gameCover, data } = this.state;
    return (
      <View style={styles.container}>
        <HeaderDetailGame
          title={data.name}
          goBack={this.props.navigation.goBack}
          renderContent={this.renderContent}
          gameCover={gameCover}
          headerScreenshot={headerGameScreenshot}
        />
      </View>
    );
  }
}

export default DetailGameView;

const styles = StyleSheet.create({
  image: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  container: {
    flex: 1,
    backgroundColor: "#191919",
  },
  iconLeft: {
    marginTop: 15,
    marginStart: 5,
  },
  textContentTitle: {
    marginTop: 8,
    fontFamily: "Roboto-Regular",
    color: "#dadada",
    fontSize: hp("2.1%"),
  },
  textContentDetail: {
    marginTop: 1,
    fontFamily: "Roboto-Bold",
    color: "#dadada",
    fontSize: hp("2.2%"),
    marginEnd: 150,
  },
  textSummary: {
    alignItems: "center",
    fontFamily: "Roboto-Regular",
    color: "#dadada",
    fontSize: hp("2.1%"),
    marginEnd: 8,
  },
  textReleaseDate: {
    marginTop: 2,
    fontFamily: "Roboto-BoldItalic",
    color: "#dadada",
    fontSize: hp("2.1%"),
  },
});
