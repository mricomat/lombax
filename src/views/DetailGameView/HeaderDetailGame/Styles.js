import {StyleSheet, Platform, StatusBar} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const HEADER_MAX_HEIGHT = 190;
const HEADER_MIN_HEIGHT = 70;

const STATUS_BAR_HEIGHT =
  Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : StatusBar.currentHeight;

export default styles = StyleSheet.create({
  headerContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#000000',
    alignItems: 'center',
  },
  imageHeader: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  linearGradientUp: {position: 'absolute', width: '100%', height: 100},
  linearGradientDown: {position: 'absolute', width: '100%', height: '100%'},
  headerAnimNavContainer: {
    backgroundColor: 'black',
    position: 'absolute',
    width: '100%',
    height: HEADER_MIN_HEIGHT,
  },
  statusBarContainer: {
    width: '100%',
    height: STATUS_BAR_HEIGHT,
  },
  headerNavContainer: {
    width: '100%',
    flexDirection: 'row',
    height: HEADER_MIN_HEIGHT - STATUS_BAR_HEIGHT,
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
  },
  titleNavHeader: {
    fontWeight: 'bold',
    fontSize: hp('2.6%'),
    marginLeft: 30,
    color: 'white',
  },
  coverContainer: {
    flex: 1,
    width: 300,
    height: 300,
    position: 'absolute',
    alignSelf: 'flex-end',
  },
  coverAnimContainer: {overflow: 'hidden', alignSelf: 'flex-end'},
  coverImage: {
    borderRadius:3,
    borderWidth:0.4, 
    borderColor:"#dadada",
    flex: 1,
    width: null,
    height: null,
  },
  titleContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleAnimContainer: {
    overflow: 'hidden',
  },
  title: {
    fontWeight: 'bold',
    fontSize: hp('3%'),
    paddingLeft: 15,
    color: '#dadada',
  },
});
