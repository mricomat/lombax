import React, { Component } from "react";
import { View, Text, Animated, TouchableOpacity, Easing } from "react-native";
import FastImage from "react-native-fast-image";
import LinearGradient from "react-native-linear-gradient";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import Icon from "react-native-ionicons";

import styles from "./Styles";

const HEADER_MAX_HEIGHT = 190;
const HEADER_MIN_HEIGHT = 70;
const PROFILE_IMAGE_MAX_HEIGHT = hp("23%");
const PROFILE_IMAGE_MAX_WIDTH = wp("26%");

const PROFILE_IMAGE_MIN_HEIGHT = 40;
const PROFILE_IMAGE_MIN_WIDTH = 40;

const COLORS = ["rgba(0,0,0,0.00)", "rgba(26, 26, 26, 0.85)", "rgba(26, 26, 26, 0.95)", "rgba(26, 26, 26, 1)"];

const COLORS2 = ["rgba(26, 26, 26, 0.8)", "rgba(26,26,26,0.3)", "rgba(0,0,0,0.00)"];

const LOCATIONS = [0.5, 0.8, 0.9, 1];
const LOCATIONS2 = [0, 0.4, 1];

class HeaderDetailGame extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(0),
      isScrolled: false,
      isTitleShown: false,
    };
    this.opacity = new Animated.Value(0);
    this.headerTitleOpacity = new Animated.Value(0);
  }
  setOpacity() {
    Animated.timing(this.opacity, {
      toValue: this.state.isScrolled ? 1 : 0,
      duration: 150,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => this.setOpacity());
  }

  setHaderTitleOpacity() {
    Animated.timing(this.headerTitleOpacity, {
      toValue: this.state.isTitleShown ? 1 : 0,
      duration: 300,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => this.setHaderTitleOpacity());
  }

  render() {
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      //useNativeDriver: true,
      extrapolate: "clamp",
    });
    const headerBlur = this.state.scrollY.interpolate({
      inputRange: [0, 300],
      outputRange: [0, 2],
      easing: Easing.linear,
      //useNativeDriver: true,
      extrapolate: "extend",
    });

    const coverImageHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
      outputRange: [PROFILE_IMAGE_MAX_HEIGHT, PROFILE_IMAGE_MIN_HEIGHT],
      useNativeDriver: true,
      extrapolate: "clamp",
    });

    const coverImageWidht = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
      outputRange: [PROFILE_IMAGE_MAX_WIDTH, PROFILE_IMAGE_MIN_WIDTH],
      //useNativeDriver: true,
      extrapolate: "clamp",
    });

    const coverImageMarginTop = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_MAX_HEIGHT + hp("6%") - HEADER_MIN_HEIGHT],
      outputRange: [hp("24.4%"), hp("32%")],
      //useNativeDriver: true,
      extrapolate: "clamp",
    });

    const coverImageBorderRadius = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_MAX_HEIGHT + hp("6%") - HEADER_MIN_HEIGHT],
      outputRange: [0, 40],
      extrapolate: "clamp",
      //useNativeDriver: true,
    });

    const coverImageMarginEnd = this.state.scrollY.interpolate({
      inputRange: [0, wp("30%")],
      outputRange: [17, wp("10%")],
      extrapolate: "clamp",
      // useNativeDriver: true,
    });

    const profileTitleMarginTop = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_MAX_HEIGHT + hp("30%") - HEADER_MIN_HEIGHT],
      outputRange: [HEADER_MAX_HEIGHT + hp("9%") - PROFILE_IMAGE_MAX_HEIGHT / 2, HEADER_MAX_HEIGHT + hp("8%") + 5],
      extrapolate: "clamp",
      //useNativeDriver: true,
    });

    const contentMarginTop = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_MAX_HEIGHT + hp("30%") - HEADER_MIN_HEIGHT],
      outputRange: [-80, 80],
      //useNativeDriver: true,
      extrapolate: "clamp",
    });

    const headerZindex = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT, 120],
      outputRange: [0, 0, 1000],
      //useNativeDriver: true,
      extrapolate: "clamp",
    });

    const { renderContent, headerScreenshot, gameCover, goBack } = this.props;

    return (
      <View style={{ flex: 1, backgroundColor: "#191919" }}>
        <Animated.View
          style={[
            styles.headerContainer,
            {
              height: headerHeight,
              zIndex: headerZindex,
              elevation: headerZindex, //required for android
            },
          ]}>
          <Animated.Image
            // TODO
            style={styles.imageHeader}
            //blurRadius={headerBlur}
            source={{
              uri: headerScreenshot,
            }}
          />
          <LinearGradient locations={LOCATIONS2} colors={COLORS2} style={styles.linearGradientUp}></LinearGradient>
          <LinearGradient locations={LOCATIONS} colors={COLORS} style={styles.linearGradientDown}></LinearGradient>
          <View style={{ width: "100%" }}>
            <Animated.View
              style={[
                styles.headerAnimNavContainer,
                {
                  opacity: this.opacity.interpolate({
                    inputRange: [0, 0.2],
                    outputRange: [0, 1],
                    //useNativeDriver: true,
                  }),
                  transform: [
                    {
                      scale: this.opacity.interpolate({
                        inputRange: [0, 1],
                        outputRange: [1.1, 1],
                        // useNativeDriver: true,
                      }),
                    },
                  ],
                },
              ]}
            />
            <View style={styles.statusBarContainer} />
            <View style={styles.headerNavContainer}>
              <TouchableOpacity style={{ marginStart: wp("5%") }} onPress={() => goBack()}>
                <Icon ios="arrow-back" android="arrow-back" color={"white"} size={24} />
              </TouchableOpacity>

              <Animated.Text
                style={[
                  styles.titleNavHeader,
                  {
                    opacity: this.headerTitleOpacity.interpolate({
                      inputRange: [0, 0.2],
                      outputRange: [0, 1],
                      // useNativeDriver: true,
                    }),
                    transform: [
                      {
                        scale: this.headerTitleOpacity.interpolate({
                          inputRange: [0, 1],
                          outputRange: [1.1, 1],
                          //   useNativeDriver: true,
                        }),
                      },
                    ],
                  },
                ]}>
                {this.props.title}
              </Animated.Text>
            </View>
          </View>
        </Animated.View>

        <Animated.ScrollView
          style={{ flex: 1 }}
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],

            {
              listener: event => {
                const offsetY = event.nativeEvent.contentOffset.y;
                if (!this.state.isScrolled && offsetY > 140) {
                  this.setOpacity();
                  this.setState({ isScrolled: true });
                }
                if (offsetY > 160) {
                  this.setHaderTitleOpacity();
                  this.setState({ isTitleShown: true });
                }

                if (this.state.isScrolled && offsetY < 160) {
                  this.setOpacity();
                  this.setHaderTitleOpacity();
                  this.setState({ isScrolled: false, isTitleShown: false });
                }
              },
            },
          )}>
          <View style={styles.coverContainer}>
            <Animated.View
              style={[
                styles.coverAnimContainer,
                {
                  height: coverImageHeight,
                  width: coverImageWidht,
                  borderRadius: coverImageBorderRadius,
                  marginTop: coverImageMarginTop,
                  marginEnd: coverImageMarginEnd,
                },
              ]}>
              <FastImage
                style={styles.coverImage}
                source={{
                  uri: gameCover,
                  priority: FastImage.priority.high,
                }}
                resizeMode={FastImage.resizeMode.cover}
              />
            </Animated.View>
          </View>

          <View style={styles.titleAnimContainer}>
            <Animated.View
              style={[
                styles.titleAnimContainer,
                {
                  marginTop: profileTitleMarginTop,
                },
              ]}>
              <View>
                <Text style={styles.title}>{this.props.title}</Text>
              </View>
              {renderContent()}
            </Animated.View>
          </View>
        </Animated.ScrollView>
      </View>
    );
  }
}
export default HeaderDetailGame;
