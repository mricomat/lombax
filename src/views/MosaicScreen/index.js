import React from "react";
import { View, FlatList, Text, TouchableWithoutFeedback, TouchableOpacity } from "react-native";

import FastImage from "react-native-fast-image";
import Icon from "react-native-ionicons";
import * as Animatable from "react-native-animatable";

import Navigation from "../../routes/Navigation";
import { getImageUrl } from "../../utils/imageUris";
import LoadingView from "../../components/LoadingView";
import { getPopularSection, getTopRatedSection, getGenreGamesSection, getCollectionsSection } from "../../api/games";
import Styles from "./Styles";
import Genres from "../../constants/Genres";
import ActivityBar from "../../components/ActivityBar";

class MosaicScreen extends React.Component {
  constructor(props) {
    super(props);

    const offset = props.navigation.getParam("offset");
    const sectionData = props.navigation.getParam("section");
    const wService = props.navigation.getParam("wService");
    const genre = props.navigation.getParam("genre");

    this.state = {
      offset,
      title: sectionData.title,
      games: sectionData.games,
      genre,
      wService,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { offset, games, wService } = this.state;

    if (nextState.offset !== offset) {
      return true;
    }

    if (nextState.games !== games) {
      return true;
    }

    if (nextState.wService !== wService) {
      return true;
    }

    return false;
  }

  navigateToDetail = game => {
    Navigation.navigate("DetailGameView", {
      game,
    });
  };

  renderItem = ({ item, index }) => {
    if (item == "") {
      return <LoadingView style={Styles.imageItem} />;
    }
    const cover = item.cover;
    if (cover) {
      return (
        <Animatable.View animation="zoomInUp" useNativeDriver={true} duration={100}>
          <TouchableOpacity style={{marginBottom:5}} onPress={() => this.navigateToDetail(item)}>
            <FastImage
              style={Styles.imageItem}
              source={{
                uri: getImageUrl(cover.image_id, "t_cover_big"),
                priority: FastImage.priority.normal,
              }}
              resizeMode={FastImage.resizeMode.cover}
            />
            <ActivityBar rating={3.2} isReview={true} isRePlayed={true} isLiked={true} />
          </TouchableOpacity>
        </Animatable.View>
      );
    }
  };

  onEndReached = async () => {
    const { offset, games, wService, genre } = this.state;
    //this.setState({loading: true});

    const result = await wService(offset, genre ? Genres.find(item => item.name == genre).id : null);
    const newGames = [...games, ...result];
    this.setState({ games: newGames });
  };

  render() {
    const { title, games } = this.state;

    return (
      <View style={Styles.container}>
        <View style={Styles.headerContainer}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Icon style={Styles.icon} name={"arrow-back"} size={22} color={"#FFFFFF"} />
          </TouchableOpacity>

          <Text style={Styles.headerTitle}>{title}</Text>
        </View>
        <View style={{ flex: 1 }}>
          <FlatList
            numColumns={3}
            ref={ref => (this.list = ref)}
            keyExtractor={item => item.id}
            data={games}
            extraData={this.state}
            renderItem={this.renderItem}
            style={Styles.flatList}
            onEndReached={this.onEndReached}
          />
        </View>
      </View>
    );
  }
}

export default MosaicScreen;
