import { StyleSheet, Platform, StatusBar } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

const STATUS_BAR_HEIGHT = Platform.OS === "ios" ? (IS_IPHONE_X ? 44 : 20) : StatusBar.currentHeight;

export default styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#191919" },
  flatList: {
    flex: 1,
    marginStart: 3,
    marginTop: 10,
  },
  headerContainer: {
    height: 90 - STATUS_BAR_HEIGHT,
    backgroundColor: "black",
    width: "100%",
    alignItems: "center",
    flexDirection: "row",
  },
  icon: {
    marginStart: 15,
    marginTop: STATUS_BAR_HEIGHT,
  },
  headerSectionLabel: {
    marginStart: 8,
    fontFamily: "Montserrat-Bold",
    fontSize: 14,
    color: "white",
  },
  headerTitle: {
    color: "white",
    fontSize: 18,
    marginStart: 30,
    fontFamily: "Roboto-Bold",
    marginTop: STATUS_BAR_HEIGHT,
  },
  imageItem: {
    resizeMode: "stretch",
    width: wp("31.4 %"),
    height: hp("24%"),
    marginStart: 5,
    borderRadius: 4,
  },
});
