import React from 'react';
import {
  Text,
  Image,
  View,
  TouchableOpacity,
  StatusBar,
  Switch,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-ionicons';

import styles from './styles';

class SettingsScreen extends React.Component {
  renderSetting = (title, onPress) => {
    return (
      <TouchableOpacity style={styles.settingContainer} onPress={onPress}>
        <Text style={styles.settingTitle}>{title}</Text>
        <Icon
          style={{marginTop: 2}}
          name={'ios-arrow-forward'}
          size={20}
          color={'white'}
        />
      </TouchableOpacity>
    );
  };

  renderTitleSettings = (title, icon) => {
    return (
      <View style={styles.titleSettingsContainer}>
        <Icon style={{marginTop: 2}} name={icon} size={20} color={'white'} />
        <Text style={styles.accountSettingsTitle}>{title}</Text>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar translucent={true} backgroundColor={'#191919'} />
        <View style={styles.headerContainer}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.coverProfileContainer}>
              <Image
                source={{uri: 'https://picsum.photos/200/300'}}
                style={styles.coverProfile}
              />
            </View>
            <View style={styles.profileNameContainer}>
              <Text style={styles.profileNameText}>Cliew</Text>
              <Text style={styles.profileEmailText}>juadas212@gmail.com</Text>
            </View>
          </View>
        </View>

        <ScrollView>
          <View style={styles.accountSettingsContainer}>
            {this.renderTitleSettings('Account settings', 'ios-cog')}
            {this.renderSetting('Change your Username')}
            {this.renderSetting('Change your Password')}
            {this.renderSetting('Change your Avatar')}
            {this.renderSetting('Change your Background')}
            {this.renderSetting('Change your Language')}

            <TouchableOpacity style={styles.settingContainer}>
              <Text
                style={[
                  styles.settingTitle,
                  {color: '#bf1432', fontFamily: 'Roboto-Bold'},
                ]}>
                Delete Account
              </Text>
              <Icon
                style={{marginTop: 2}}
                name={'ios-arrow-forward'}
                size={20}
                color={'#bf1432'}
              />
            </TouchableOpacity>
          </View>
          <View style={[styles.accountSettingsContainer, {height: 125}]}>
            {this.renderTitleSettings('Application settings', 'ios-hammer')}
            <View style={[styles.settingContainer, {marginEnd: 2}]}>
              <Text style={styles.settingTitle}>Allow notifications</Text>
              <Switch value={true} />
            </View>
            {this.renderSetting('About us')}
          </View>
          <TouchableOpacity style={styles.touchableLogout}>
            <Text style={styles.logoutText}>Logout</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

export default SettingsScreen;
