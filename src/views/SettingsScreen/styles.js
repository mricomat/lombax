import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0E0E0E',
  },
  headerContainer: {
    height: 200,
    backgroundColor: '#191919',
    justifyContent: 'flex-end',
  },
  coverProfileContainer: {
    height: 88,
    width: 88,
    backgroundColor: 'white',
    borderRadius: 50,
    marginStart: 14,
    marginBottom: 10,
    justifyContent: 'center',
  },
  coverProfile: {
    height: 84,
    width: 84,
    borderRadius: 50,
    alignSelf: 'center',
  },
  profileNameContainer: {
    flexDirection: 'column',
    alignSelf: 'center',
    marginStart: 15,
    marginTop: -12,
  },
  profileNameText: {
    color: 'white',
    fontSize: 20,
    fontFamily: 'Roboto-Medium',
  },
  profileEmailText: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
  },
  accountSettingsContainer: {
    marginTop: 20,
    height: 257,
    width: '90%',
    borderRadius: 10,
    borderWidth: 0.4,
    borderColor: 'grey',
    alignSelf: 'center',
    backgroundColor: '#191919',
  },
  accountSettingsTitle: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    marginStart: 6,
  },
  settingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginStart: 8,
    marginTop: 12,
    marginEnd: 8,
  },
  settingTitle: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
  },
  titleSettingsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginStart: 8,
    marginBottom: 12,
  },
  logoutText: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: 18,
    alignSelf: 'center',
  },
  touchableLogout:{
    width: '80%',
    height: 35,
    borderRadius: 15,
    alignSelf: 'center',
    backgroundColor: '#117ea9',
    marginTop: 20,
    justifyContent: 'center',
  }
});
