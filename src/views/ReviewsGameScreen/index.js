import React from "react";
import {
  Dimensions,
  FlatList,
  LayoutAnimation,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StatusBar,
  View,
} from "react-native";
import { TabView, TabBar, SceneMap } from "react-native-tab-view";
import Icon from "react-native-ionicons";
import FastImage from "react-native-fast-image";
import LinearGradient from "react-native-linear-gradient";

import ReviewsList from "../../components/ReviewsList";
import styles from "./styles";

const COLORS = ["#0E0E0E", "rgba(0,0,0,0.00)", "#0E0E0E", "#0E0E0E"];

class DetailGameScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      routes: [
        { key: "all", title: "Everyone" },
        { key: "friends", title: "Friends" },
        { key: "you", title: "You" },
        { key: "liked", title: "Liked" },
      ],
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { index } = this.state;

    if (nextState.index != index) {
      return true;
    }

    return false;
  }

  renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled={true}
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      labelStyle={styles.label}
      tabStyle={styles.tabStyle}
      activeColor={"white"}
      inactiveColor={"#c0c0c0"}
    />
  );

  renderScene = ({ route }) => {
    switch (route.key) {
      case "all":
        return (
          <View style={{ flex: 1, paddingTop: 15, paddingStart: 10, backgroundColor: "#0E0E0E" }}>
            <ReviewsList />
          </View>
        );
      case "friends":
        return (
          <View style={{ flex: 1, paddingTop: 15, paddingStart: 10, backgroundColor: "#0E0E0E" }}>
            <ReviewsList />
          </View>
        );
      case "you":
        return (
          <View style={{ flex: 1, paddingTop: 15, paddingStart: 10, backgroundColor: "#0E0E0E" }}>
            <ReviewsList />
          </View>
        );
      case "liked":
        return (
          <View style={{ flex: 1, paddingTop: 15, paddingStart: 10, backgroundColor: "#0E0E0E" }}>
            <ReviewsList />
          </View>
        );
      default:
        return null;
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <FastImage
          style={{ width: "100%", height: 250, position: "absolute", top: 0 }}
          source={{
            uri: "https://images.igdb.com/igdb/image/upload/t_cover_big/co1re6.jpg",
            priority: FastImage.priority.high,
          }}
          resizeMode={FastImage.resizeMode.cover}
        />
        <LinearGradient
          colors={COLORS}
          style={{
            width: "100%",
            height: 250,
            position: "absolute",
            top: 0,
          }}></LinearGradient>
        <View
          style={{
            height: 100,
            flexDirection: "row",
            paddingTop: StatusBar.currentHeight,
            marginStart: 15,
            marginEnd: 15,
            alignItems: "center",
          }}>
          <Icon ios="arrow-back" android="arrow-back" color={"white"} size={24} />
          <Text style={{ fontFamily: "Roboto-Bold", color: "white", fontSize: 20, marginStart: 30, alignSelf: "center" }}>
            Reviews of Warcraf III
          </Text>
        </View>
        <TabView
          style={styles.tabView}
          navigationState={this.state}
          renderScene={this.renderScene}
          renderTabBar={this.renderTabBar}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{ width: Dimensions.get("window").width }}
        />
      </View>
    );
  }
}

export default DetailGameScreen;
