import { StyleSheet, Platform, StatusBar } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0E0E0E",
  },
  tabView: {},
  tabbar: {
    width: "100%",
    backgroundColor: "transparent",
  },
  tabStyle: {
    width: wp(100) / 4,
  },
  indicator: {
    backgroundColor: "#289FD1",
    borderRadius: 5,
    height: 2.5,
  },
  label: {
    fontFamily: "Roboto-Medium",
    fontSize: hp("2.05%"),
  },
  scene: {
    flex: 1,
  },
});
