import { StyleSheet, Platform, StatusBar } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

export default styles = StyleSheet.create({
  containerFooter: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "center",
  },
  containerButtonLeft: {
    justifyContent: "center",
    alignItems: "center",
    marginEnd: 40,
  },
  containerButtonRight: {
    justifyContent: "center",
    alignItems: "center",
    marginStart: 40,
  },
  image: {
    width: 20,
    height: 20,
  },
  label: {
    marginTop: 10,
    fontFamily: "Montserrat-Medium",
    fontSize: wp("2.8%"),
    color: "white",
  },
  flatList: {
    flex: 1,
    marginStart: 7,
    marginTop: 10,
  },
  headerRowList: {
    marginTop: 15,
    marginStart: 12,
    fontFamily: "Montserrat-Bold",
    fontSize: 14,
    color: "white",
  },
  imageItem: {
    width: wp("29%"),
    height: hp("24%"),
    marginStart: 5,
  },
  headerContainer: {
    alignItems: "center",
    position: "absolute",
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 32,
  },
  logoContainer: {
    alignItems: "center",
    flexDirection: "row",
    marginStart: wp("6%"),
  },
  gamesLabel: {
    fontFamily: "Roboto-Regular",
    fontSize: wp("4%"),
    marginStart: wp("3%"),
    color: "white",
  },
  arrowDropDown: {
    marginTop: 5,
    alignSelf: "center",
    marginStart: 10,
  },
  modalTitle: {
    alignSelf: "center",
    fontFamily: "Roboto-Bold",
    fontSize: 20.5,
    color: "white",
    marginTop: 70,
  },
  modalExitTouchable: {
    width: 70,
    height: 70,
    borderRadius: 40,
    backgroundColor: "#f2f0f0",
    justifyContent: "flex-end",
    position: "absolute",
    justifyContent: "center",
    bottom: 0,
  },
  modalItemTitle: {
    alignSelf: "center",
    fontFamily: "Roboto-Medium",
    fontSize: 16.5,
    color: "#C1C1C1",
    marginTop: 26,
    justifyContent: "center",
    textAlign: "center",
  },
});
