import React from "react";
import {
  Image,
  StatusBar,
  View,
  Text,
  ScrollView,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList,
} from "react-native";
import Icon from "react-native-ionicons";
import Modal from "react-native-modal";

import KeyWordsList from "../../components/KeyWordsList";
import MainImageView from "../../components/MainImageView";
import ButtonPlay from "../../components/ButtonPlay";
import SectionGameList from "../../components/SectionGamesList";
import { getMainGameData } from "../../api/games";
import { getImageUrl } from "../../utils/imageUris";
import SkeletonLoader from "../../components/SkeletonLoader";
import Navigation from "../../routes/Navigation";
import styles from "./styles";
import { connect } from "react-redux";
import { genresThemesSorted } from "../../constants/Genres";
import { sectionsRequest } from "../../stores/sections/SectionsActions";
import store from "../../stores/rootStore";

const { height } = Dimensions.get("window");
const HEIGTH_BOTTOM_BAR = 56;
const IMAGE_HEIGTH = height - HEIGTH_BOTTOM_BAR;

genresThemesSorted.unshift({ name: "All genres", type: "all genres" });

const mapDispatchToProps = dispatch => ({
  sectionsRequest: sort => dispatch(sectionsRequest(sort)),
});

const mapStateToProps = state => {
  return {
    highLight: state.sections.highLight,
    games: state.sections.games,
    isFetching: state.sections.isFetching,
    failure: state.sections.failure,
    error: state.sections.error,
  };
};

class HomeView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      keyWords: [],
      isModalVisible: false,
      genre: genresThemesSorted[0],
    };
  }

  renderItem = ({ item, index }) => {
    return <Image source={{ uri: item }} style={styles.imageItem}></Image>;
  };

  navigateYoutubeVideoView = () => {
    const game = this.props.highLight.game;
    if (game.videos) {
      Navigation.navigate("YoutubeVideoScreen", { idVideo: game.videos[0].video_id, fullScreen: true });
    }
  };

  navigateDetailGameView = () => {
    Navigation.navigate("DetailGameScreen", { game: this.props.highLight.game });
  };

  renderFooterMainGame = () => {
    const { highLight } = this.props;

    let keyWords = [];
    if (highLight) {
      keyWords = highLight.keywords;
    }

    return (
      <View>
        <KeyWordsList wordsList={keyWords} />
        <View style={styles.containerFooter}>
          <View style={styles.containerButtonLeft}>
            <Icon name={"add"} size={24} color={"#FFFFFF"} />
            <Text style={styles.label}>My List</Text>
          </View>
          <ButtonPlay onPress={this.navigateYoutubeVideoView} />

          <View style={styles.containerButtonRight}>
            <TouchableWithoutFeedback onPress={() => this.navigateDetailGameView()}>
              <View>
                <TouchableOpacity onPress={() => this.navigateDetailGameView()}>
                  <Icon name={"information-circle-outline"} size={24} color={"#FFFFFF"} />
                </TouchableOpacity>
                <Text style={styles.label}>Info</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    );
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  _genreSelected = genre => {
    store.dispatch(sectionsRequest(genre));
    this.setState({ isModalVisible: !this.state.isModalVisible, genre: genre });
  };

  _renderGenreItem = ({ item }) => {
    return (
      <TouchableOpacity style={{ justifyContent: "center", alignSelf: "center" }} onPress={() => this._genreSelected(item)}>
        <Text style={styles.modalItemTitle}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  _renderModal = () => {
    const { isModalVisible } = this.state;

    return (
      <Modal isVisible={isModalVisible}>
        <View style={{ flex: 1, flexDirection: "column", alignItems: "center" }}>
          <FlatList
            contentContainerStyle={{ paddingBottom: 100 }}
            data={genresThemesSorted}
            renderItem={this._renderGenreItem}
            style={{ flex: 1 }}
            keyExtractor={item => item.id}
            ListHeaderComponent={() => {
              return <Text style={styles.modalTitle}>All genres</Text>;
            }}
          />
          <TouchableOpacity style={styles.modalExitTouchable} onPress={this.toggleModal}>
            <Icon style={{ alignSelf: "center" }} name={"ios-close"} size={42} color={"black"} />
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  _renderHeader = () => {
    return (
      <View style={styles.headerContainer}>
        <View style={styles.logoContainer}>
          <Image resizeMode={"contain"} style={{ width: 50, height: 50 }} source={require("../../assets/ice.png")} />
          <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }} onPress={this.toggleModal}>
            <Text style={styles.gamesLabel}>{this.state.genre.name}</Text>
            <Icon style={styles.arrowDropDown} name={"arrow-dropdown"} size={22} color={"#FFFFFF"} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    const { highLight, isFetching } = this.props;
    let mainImageUrl = null;
    if (highLight.game && highLight.game.cover) {
      mainImageUrl = getImageUrl(highLight.game.cover.image_id, "t_cover_big_2x");
    }

    if (isFetching) {
      return (
        <View style={{ flex: 1, backgroundColor: "#191919" }}>
          <SkeletonLoader />
        </View>
      );
    }
    return (
      <View style={{ flex: 1, backgroundColor: "#191919" }}>
        <StatusBar backgroundColor={"#0000"} translucent={true} />
        <ScrollView>
          <MainImageView imageUri={{ uri: mainImageUrl }} />
          {this._renderHeader()}
          <View
            style={{
              marginTop: IMAGE_HEIGTH - IMAGE_HEIGTH * 0.36,
              height: "100%",
              flex: 1,
            }}>
            {this.renderFooterMainGame()}
            <SectionGameList />
          </View>
        </ScrollView>
        {this._renderModal()}
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeView);
