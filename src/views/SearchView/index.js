import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TextInput,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Keyboard,
  ActivityIndicator,
  StatusBar,
} from "react-native";

import Animated from "react-native-reanimated";
import LinearGradient from "react-native-linear-gradient";
import { ParallaxSwiper, ParallaxSwiperPage } from "react-native-parallax-swiper";
import Icon from "react-native-ionicons";
import { onScroll } from "react-native-redash";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

import { genresThemesInfo } from "../../constants/Genres";
import { platformsWithImage } from "../../constants/Platforms";
import MainSearchSlider from "./MainSearchSlide";
import { searchRequest, searchCompaniesRequest } from "../../api/search";
import ResultSearchView from "./ResultSearchView";
import TimeRatingSlide from "./TimeRatingSlide";
import CompaniesSlide from "./CompaniesSlide";
import InterestList from "../../components/InterestList";

// import styles from "./Styles";

const { height } = Dimensions.get("window");
const φ = (1 + Math.sqrt(5)) / 2;

export const MIN_HEADER_HEIGHT = 35;
export const MAX_HEADER_HEIGHT = height * 0.38; // 38% height

export const BUTTON_HEIGHT = 48;
export const BUTTON_WIDTH = 200;
const EXP_HEIGTH = MAX_HEADER_HEIGHT - BUTTON_HEIGHT;

const { interpolate, Extrapolate, Value } = Animated;

const y = new Value(0);

class SearchView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: "",
      companiesSearchValue: "",
      filters: [],
      searchResult: [],
      searchCompaniesResult: [],
      loading: false,
      activePageIndex: 2,
      isScrollEnabled: true,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this.keyboardDidShow);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
  }

  keyboardDidShow = () => {
    this.scrollToTop();
  };

  shouldComponentUpdate(nextProps, nextState) {
    const { filters, searchResult, searchCompaniesResult, loading } = this.state;
    if (nextState.filters != filters) {
      return true;
    }
    if (nextState.searchResult != searchResult) {
      return true;
    }
    if (nextState.searchCompaniesResult != searchCompaniesResult) {
      return true;
    }
    if (nextState.loading != loading) {
      return true;
    }
    return false;
  }

  search = async () => {
    const { filters, searchValue } = this.state;
    const query = this.queryConstructor();

    this.setState({ loading: true });

    let result = await searchRequest(query);
    if (result.length == 0) {
      result = ["EMPTY"];
    }

    this.scrollToTop();

    if (filters.length == 0 && searchValue == "") {
      this.setState({ loading: false, searchResult: [] });
    } else {
      this.setState({ searchResult: result, loading: false });
    }
  };

  searchCompanies = async () => {
    const { companiesSearchValue } = this.state;
    if (companiesSearchValue != "") {
      this.setState({ loading: true });
      let result = await searchCompaniesRequest(`where name = "${companiesSearchValue}"*`);
      this.setState({ loading: false, searchCompaniesResult: result });
    }
  };

  queryConstructor = () => {
    const { filters, searchValue } = this.state;
    const platforms = [],
      genres = [],
      themes = [],
      companies = [];
    let rating = null;

    console.log("filters", filters);
    filters.forEach(filter => {
      switch (filter.type) {
        case "platform":
          platforms.push(filter.id);
          break;
        case "genres":
          genres.push(filter.id);
          break;
        case "themes":
          themes.push(filter.id);
          break;
        case "rating":
          // TODO
          break;
        case "company":
          companies.push(filter.id);
          break;
      }
    });

    const query = `where name = "${searchValue}"*${platforms.length > 0 ? `& platforms = (${platforms})` : ``}${
      genres.length > 0 ? `& genres = [${genres}]` : ``
    }${themes.length > 0 ? `& themes = [${themes}]` : ``}${companies.length > 0 ? `& involved_companies = (${companies})` : ``}${
      rating != null ? `& total_rating ${rating.data.range} ${rating.data.number}` : ``
    }`;

    console.log(query);
    return query;
  };

  scrollToTop = () => {
    this.scroll.getNode().scrollTo({ x: 0, y: 0, animated: true });
    this.setState({ isScrollEnabled: false });
  };

  addFilter = filterData => {
    const { filters } = this.state;
    if (filters.length == 0) {
      this.scrollToTop();
    }

    if (!filters.includes(filterData)) {
      this.setState(prevState => ({
        filters: [...prevState.filters, filterData],
      }));
    }
  };

  removeFilter = filter => {
    const { filters, searchResult } = this.state;
    const newFilters = filters.filter(item => {
      return item !== filter;
    });
    this.setState(
      {
        filters: newFilters,
        searchResult: newFilters.length == 0 ? [] : searchResult,
      },
      () => {
        if (newFilters.length != 0 && searchResult.games && searchResult.games.length != 0) {
          this.search();
        }
      },
    );
  };

  onChangeText = text => {
    const { activePageIndex } = this.state;
    activePageIndex != 3 ? this.setState({ searchValue: text }) : this.setState({ companiesSearchValue: text });
  };

  widthItemSelected = name => {
    const width = name.length * wp("3.2%");
    if (name.length <= 5) {
      return width + 12;
    }
    if (name.length >= 9) {
      return width - 12;
    }
    return width;
  };

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => this.removeFilter(item)}
        style={[styles.touchableItem, { width: this.widthItemSelected(item.name) }]}>
        <LinearGradient
          start={{ x: 0.0, y: 0.25 }}
          end={{ x: 1, y: 1 }}
          style={[styles.linearGradient, { flexDirection: "row", width: this.widthItemSelected(item.name) }]}
          colors={[item.colorLeft ? item.colorLeft : "#F59B23", item.colorRight ? item.colorRight : "#7B4E12"]}>
          <Text style={styles.textTitleItem}>{item.name}</Text>
          {/* <Icon style={{ marginStart: 5 }} name={"close"} size={21} color={"white"} /> */}
        </LinearGradient>
      </TouchableOpacity>
    );
  };

  renderSwipeItem = ({ item }) => {
    return <MainSearchSlider title={item.title} addFilter={this.addFilter} data={item.info} />;
  };

  renderMainView = () => {
    const { searchResult, loading, searchCompaniesResult, activePageIndex, companiesSearchValue } = this.state;
    if (loading) {
      return (
        <View style={{ flex: 1, height: hp("80%"), justifyContent: "center" }}>
          <ActivityIndicator style={{ justifyContent: "center", alignSelf: "center" }} size={65} color="white" />
        </View>
      );
    }

    return (
      <View style={styles.swipeContainer}>
        {searchResult.games && searchResult.games.length > 0 ? (
          <ResultSearchView searchData={searchResult} />
        ) : (
          <ParallaxSwiper
            speed={0.5}
            style={{ width: "100%", flex: 1, backgroundColor: "red" }}
            dividerWidth={8}
            dividerColor="#191919"
            scrollToIndex={activePageIndex}
            backgroundColor="#191919"
            onMomentumScrollEnd={activePageIndex => this.setState({ activePageIndex })}
            progressBarBackgroundColor="#191919"
            progressBarValueBackgroundColor="white">
            <ParallaxSwiperPage
              BackgroundComponent={<View style={{ width: wp("100%"), height: 3000 }} />}
              ForegroundComponent={<TimeRatingSlide addFilter={this.addFilter} />}
            />
            <ParallaxSwiperPage
              BackgroundComponent={<View style={{ width: wp("100%"), height: 3000 }} />}
              ForegroundComponent={<InterestList title={"Platforms"} onPress={this.addFilter} data={platformsWithImage} />}
            />
            <ParallaxSwiperPage
              BackgroundComponent={<View style={{ width: wp("100%"), height: 3000 }} />}
              ForegroundComponent={<InterestList title={"Genres & Themes"} onPress={this.addFilter} data={genresThemesInfo} />}
            />
            <ParallaxSwiperPage
              BackgroundComponent={<View style={{ width: wp("100%"), height: 3000 }} />}
              ForegroundComponent={
                <CompaniesSlide
                  searchResult={searchCompaniesResult}
                  companiesSearchValue={companiesSearchValue}
                  addFilter={this.addFilter}
                />
              }
            />
          </ParallaxSwiper>
        )}
      </View>
    );
  };

  render() {
    const { filters, isScrollEnabled } = this.state;

    // const height = interpolate(y, {
    //   inputRange: [-MAX_HEADER_HEIGHT, -BUTTON_HEIGHT / 2],
    //   outputRange: [0, MAX_HEADER_HEIGHT + BUTTON_HEIGHT],
    //   extrapolate: Extrapolate.CLAMP,
    // });

    const opacity = interpolate(y, {
      inputRange: [-MAX_HEADER_HEIGHT / 2, 0, MAX_HEADER_HEIGHT / 2],
      outputRange: [0, 1, 0],
      extrapolate: Extrapolate.CLAMP,
    });

    return (
      <View style={{ flex: 1 }}>
        {/* <StatusBar translucent  /> */}
        <Animated.ScrollView
          ref={c => {
            this.scroll = c;
          }}
          onScroll={onScroll({ y })}
          style={styles.container}
          showsVerticalScrollIndicator={false}
          scrollEventThrottle={1}
          stickyHeaderIndices={[1]}>
          <View style={{ height: isScrollEnabled ? EXP_HEIGTH : hp("7.5%") }}>
            <View style={styles.titleContainer}>
              <Animated.Text style={[styles.title, { opacity }]}>Search</Animated.Text>
            </View>
          </View>
          <View style={styles.textInputContainer}>
            <View style={styles.sectionTextInput}>
              <Icon style={styles.iconSearch} name={"search"} size={24} color={"#444444"} />
              <TextInput
                autoCapitalize
                clearButtonMode={"while-editing"}
                onChangeText={text => this.onChangeText(text)}
                style={styles.placeHolder}
                placeholder={"Find all games & collections"}
                underlineColorAndroid="transparent"
              />
            </View>
            {filters.length > 0 ? (
              <ScrollView style={{ marginTop: 8, height: 50 }}>
                <FlatList
                  contentContainerStyle={styles.contentContainer}
                  style={styles.flatList}
                  data={filters}
                  showsHorizontalScrollIndicator={true}
                  horizontal
                  maxToRenderPerBatch={12}
                  keyExtractor={item => Math.random() * 1000}
                  renderItem={this.renderItem}
                />
              </ScrollView>
            ) : null}
          </View>
          {this.renderMainView()}
        </Animated.ScrollView>
        <TouchableOpacity
          onPress={() => (this.state.activePageIndex != 3 ? this.search() : this.searchCompanies())}
          style={{ height: 55, width: 55, position: "absolute", right: 40, top: hp("75%"), borderRadius: 50 }}>
          <LinearGradient
            start={{ x: 0.0, y: 0.25 }}
            end={{ x: 1, y: 1 }}
            style={{ height: 64, width: 64, borderRadius: 50, alignItems: "center", justifyContent: "center" }}
            colors={["#191919", "black"]}>
            <Icon style={styles.iconSearchButton} name={"search"} size={27} color={"white"} />
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  }
}
export default SearchView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#191919",
    paddingTop: MIN_HEADER_HEIGHT - BUTTON_HEIGHT / 2,
    width: "100%",
  },
  swipeContainer: {
    flex: 1,
    width: "100%",
    backgroundColor: "#191919",
  },
  marginProperty: {
    height: MAX_HEADER_HEIGHT - BUTTON_HEIGHT,
  },
  gradient: {
    position: "absolute",
    left: 0,
    bottom: 0,
    right: 0,
    alignItems: "center",
  },
  titleContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    textAlign: "center",
    color: "white",
    fontSize: 48,
    fontWeight: "bold",
    textShadowColor: "rgba(0, 0, 0, 0.9)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  itemContainer: {
    borderRadius: 8,
    marginRight: 8,
    marginLeft: 8,
  },

  placeHolder: {
    alignSelf: "center",
    color: "#444444",
    borderRadius: 4,
    textAlign: "left",
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    width: wp("59%"),
  },
  textInputContainer: {
    backgroundColor: "#191919",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    marginTop: -BUTTON_HEIGHT,
  },
  sectionTextInput: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    width: wp("93%"),
    borderRadius: 5,
    margin: 10,
  },
  iconSearch: {
    position: "absolute",
    left: wp("8%"),
    resizeMode: "stretch",
    alignItems: "center",
  },
  iconInfo: {
    marginTop: 1,
    marginStart: 5,
    alignSelf: "center",
  },
  flatList: {
    width: "100%",
  },
  columnWrapper: {
    margin: 8,
  },
  contentContainer: {
    alignItems: "center",
  },
  titleGenre: {
    fontFamily: "Roboto-Bold",
    alignSelf: "flex-start",
    marginStart: 17,
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    marginBottom: 10,
    fontSize: 18.5,
    color: "white",
  },
  touchableItem: {
    elevation: 10,
    height: hp("6%"),
    backgroundColor: "#dadada",
    borderRadius: 4,
    marginStart: 10,
  },
  linearGradient: {
    elevation: 10,
    alignItems: "center",
    height: hp("6%"),
    justifyContent: "center",
    borderRadius: 4,
  },
  textTitleItem: {
    fontFamily: "Roboto-Bold",
    fontSize: 14,
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    color: "white",
  },
  textDetailItem: {
    fontFamily: "Roboto-Regular",
    marginStart: 10,
    fontSize: 11,
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    color: "white",
  },

  iconSearchButton: {
    resizeMode: "stretch",
    alignItems: "center",
  },
});
