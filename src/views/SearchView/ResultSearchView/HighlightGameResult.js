import React from "react";
import { Text, ImageBackground, Image, View, StyleSheet, FlatList, TouchableOpacity, TouchableWithoutFeedback } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import FastImage from "react-native-fast-image";

import { getImageUrl } from "../../../utils/imageUris";
import Navigation from "../../../routes/Navigation";

class ResultSearchView extends React.Component {
  _navigateToDetail = game => {
    Navigation.navigate("DetailGameView", {
      game,
    });
  };

  render() {
    const gameData = this.props.gameData;
    const image_id = gameData.cover ? gameData.cover.image_id : "";
    return (
      <TouchableOpacity style={styles.container} onPress={() => this._navigateToDetail(gameData)}>
        <FastImage
          style={styles.coverImage}
          source={{
            uri: getImageUrl(image_id, "t_cover_big"),
            priority: FastImage.priority.high,
          }}
        />
        <View style={{ width: wp("55%") }}>
          <Text style={styles.titleName}>{gameData.name}</Text>
          <View style={styles.wordsContainer}>
            {gameData.keywords
              ? gameData.keywords.map((word, index) => {
                  if (index < 4) {
                    return (
                      <View style={{ flexDirection: "row" }} key={index}>
                        <Text style={styles.wordTitle}>{word.name}</Text>
                        {index != gameData.keywords.length - 1 && index < 2 ? <View style={styles.dot} /> : null}
                      </View>
                    );
                  }
                })
              : null}
          </View>

          <Text numberOfLines={4} style={styles.summary}>
            {gameData.summary}
          </Text>
        </View>

        <View style={{ flexDirection: "column", justifyContent: "center" }}>
          <AnimatedCircularProgress
            ref={ref => (this.circularProgresUsers = ref)}
            lineCap="round"
            size={wp(14)}
            width={6}
            rotation={360}
            fill={gameData.total_rating}
            tintColorSecondary="#1BAE4B"
            tintColor="white"
            backgroundColor="#3d5875">
            {fill => (
              <View style={{ alignItems: "center" }}>
                <Text style={styles.ratingText}>{gameData.total_rating ? ((gameData.total_rating / 100) * 5).toFixed(1) : "N/A"}</Text>
                <Text style={[styles.ratingText, { fontSize: 11 }]}>Great</Text>
              </View>
            )}
          </AnimatedCircularProgress>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginStart: 13,
    marginBottom: 10,
    marginEnd: 16,
    justifyContent: "space-between",
  },
  coverImage: {
    borderRadius: 4,
    width: wp("20%"),
    height: hp("17%"),
  },
  titleName: {
    fontFamily: "Roboto-Medium",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    fontSize: 15,
    color: "white",
  },
  wordsContainer: {
    marginTop: 5,
    flexDirection: "row",
  },
  wordTitle: {
    textTransform: "capitalize",
    fontFamily: "Roboto-Regular",
    alignSelf: "center",
    fontSize: hp("1.9%"),
    color: "#dadada",
  },
  dot: {
    alignSelf: "center",
    width: 4.5,
    height: 4.5,
    borderRadius: 100 / 2,
    backgroundColor: "#C42727",
    marginStart: 5,
    marginEnd: 5,
  },
  summary: {
    marginTop: 5,
    fontFamily: "Roboto-Regular",
    color: "#dadada",
    fontSize: hp("1.9%"),
    marginEnd: 8,
  },

  ratingText: {
    fontSize: 15,
    color: "#dadada",
    fontFamily: "Roboto-Regular",
  },
});

export default ResultSearchView;
