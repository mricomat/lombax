import React from "react";
import { Text, ImageBackground, Image, View, StyleSheet, FlatList, TouchableOpacity, TouchableWithoutFeedback } from "react-native";
import Icon from "react-native-ionicons";
import FastImage from "react-native-fast-image";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

import { getImageUrl } from "../../../utils/imageUris";
import HighlightGameResult from "./HighlightGameResult";
import Navigation from "../../../routes/Navigation";

class ResultSearchView extends React.Component {
  constructor(props) {
    super(props);
  }
  renderItem = ({ item }) => {
    const image_id = item.cover ? item.cover.image_id : "nocover_qhhlj6";
    return (
      <TouchableOpacity style={styles.gameListItem} onPress={() => this._navigateToDetail(item)}>
        <View style={{ flexDirection: "row", marginBottom: 10 }}>
          <FastImage
            style={{
              borderRadius: 4,
              width: wp("12%"),
              height: hp("7.4%"),
            }}
            source={{
              uri: getImageUrl(image_id, "t_cover_big"),
              priority: FastImage.priority.high,
            }}
          />
          <View style={{ flexDirection: "row", justifyContent: "space-between", width: wp("80%") }}>
            <View style={{ flexDirection: "column", alignSelf: "center", justifyContent: "flex-start", marginStart: 10 }}>
              <Text style={styles.gameTitle}>{item.name}</Text>
              <View style={styles.wordsContainer}>
                {item.keywords
                  ? item.keywords.map((word, index) => {
                      if (index < 4) {
                        return (
                          <View style={{ flexDirection: "row" }} key={index}>
                            <Text style={styles.wordTitle}>{word.name}</Text>
                            {index != item.keywords.length - 1 && index < 3 ? <View style={styles.dot} /> : null}
                          </View>
                        );
                      }
                    })
                  : null}
              </View>
            </View>
            <Icon style={{ alignSelf: "center" }} name={"arrow-forward"} size={18} color={"white"} />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _navigateToDetail = game => {
    Navigation.navigate("DetailGameView", {
      game,
    });
  };

  renderCollection = ({ item }) => {
    if (item.cover) {
      const image_id = item.cover.image_id;
      return (
        <TouchableOpacity onPress={() => this._navigateToDetail(item)}>
          <FastImage
            style={styles.collectionItem}
            source={{
              uri: getImageUrl(image_id, "t_cover_big"),
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </TouchableOpacity>
      );
    }
  };

  renderEmptyContainer = () => {
    return (
      <View style={{ height: hp("25%"), justifyContent: "center" }}>
        <Icon style={{ alignSelf: "center" }} name={"flag"} size={40} color={"white"} />
        <View style={{ marginTop: 10 }}>
          <Text style={styles.emptyLabel}>We didn't find any result</Text>
          <Text style={styles.emptyLabel}>for your search</Text>
        </View>
        <View style={{ marginTop: 10 }}>
          <Text style={styles.emptyAdviceLabel}>Check that you have written it well,</Text>
          <Text style={styles.emptyAdviceLabel}>or try with anothers words or parameters</Text>
        </View>
      </View>
    );
  };
  render() {
    const { searchData } = this.props;
    return (
      <View style={styles.container}>
        {searchData == "EMPTY" ? (
          this.renderEmptyContainer()
        ) : (
          <View style={{ flex: 1 }}>
            {searchData.highlightGame && <HighlightGameResult gameData={searchData.highlightGame} />}
            {searchData.collections && searchData.collections.length > 0 ? (
              <View style={{ height: hp("26%"), marginLeft: 15 }}>
                <Text style={styles.sectionTitle}>Collections</Text>
                <FlatList
                  horizontal
                  ref={ref => (this.list = ref)}
                  keyExtractor={item => item.id}
                  data={searchData.collections}
                  renderItem={this.renderCollection}
                  style={styles.flatList}
                />
              </View>
            ) : null}

            <View style={{ marginLeft: 15, flex: 1 }}>
              <Text style={styles.sectionTitle}>Games</Text>
              <FlatList
                style={styles.flatList}
                data={searchData.games}
                showsHorizontalScrollIndicator={true}
                maxToRenderPerBatch={12}
                keyExtractor={item => Math.random() * 1000}
                renderItem={this.renderItem}
              />
            </View>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatList: {
    flex: 1,
    marginTop: 5,
    width: "100%",
  },
  gameListItem: {
    elevation: 10,
  },
  gameTitle: {
    fontFamily: "Roboto-Medium",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    fontSize: 14,
    color: "white",
  },

  wordsContainer: {
    flexDirection: "row",
  },
  wordTitle: {
    textTransform: "capitalize",
    fontFamily: "Roboto-Regular",
    alignSelf: "center",
    fontSize: hp("1.8%"),
    color: "#dadada",
  },
  dot: {
    alignSelf: "center",
    width: 4.5,
    height: 4.5,
    borderRadius: 100 / 2,
    backgroundColor: "#C42727",
    marginStart: 5,
    marginEnd: 5,
  },
  emptyLabel: {
    fontFamily: "Roboto-Medium",
    alignSelf: "center",
    fontSize: hp("2.5%"),
    color: "white",
  },
  sectionTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: hp("2.5%"),
    color: "white",
  },
  emptyAdviceLabel: { fontFamily: "Roboto-Medium", alignSelf: "center", fontSize: hp("2.2%"), color: "white" },
  collectionItem: {
    resizeMode: "stretch",
    width: wp("26%"),
    height: hp("20%"),
    marginEnd: 5,
    borderRadius: 4,
  },
});

export default ResultSearchView;
