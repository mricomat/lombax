import React from "react";
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import LinearGradient from "react-native-linear-gradient";

class TimeRatingSlide extends React.Component {
  state = {
    sliderRatingValue: [0, 100],
  };

  sliderRatingChangeFinish = values => {
    const oldValues = this.state.sliderRatingValue;
    if (oldValues != values) {
      const valueDiferent = values.filter(value => !oldValues.includes(value));
      const index = values.indexOf(valueDiferent[0]);
      const filterData = {
        name: `Rating ${index != 0 ? `<` : `>`} ${valueDiferent}`,
        type: "rating",
        data: {
          range: index != 0 ? "<" : ">",
          number: valueDiferent[index],
        },
        colorLeft: "#c94b4b",
        colorRight: "#4b134f",
      };
      this.props.addFilter(filterData);
      this.setState({
        sliderRatingValue: values,
      });
    }
  };

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.props.addFilter(item)} style={styles.touchableItem}>
        <LinearGradient
          start={{ x: 0.0, y: 0.25 }}
          end={{ x: 1, y: 1 }}
          style={styles.linearGradient}
          colors={[item.colorLeft ? item.colorLeft : "#CB356B", item.colorRight ? item.colorRight : "#BD3F32"]}>
          <Text style={styles.textTitleItem}>{item}</Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  };

  render() {
    const data = [2021, 2021, 2021, 2021, 2021, 2021, 2021, 2021, 2021, 2021, 2021, 2021, 2021, 2021, 2021, 2021];
    return (
      <View style={styles.container}>
        <View style={{}}>
          <Text style={styles.titleText}>Rating</Text>

          <MultiSlider
            values={[this.state.sliderRatingValue[0], this.state.sliderRatingValue[1]]}
            sliderLength={340}
            containerStyle={{ justfyContent: "center", alignSelf: "center" }}
            markerStyle={{ width: 30, height: 30, borderRadius:20 }}
            trackStyle={{height:4}}
            pressedMarkerStyle={{ width: 20, height: 20 }}
            onValuesChangeStart={this.sliderRatingChangeStart}
            onValuesChange={this.sliderRatingChange}
            onValuesChangeFinish={this.sliderRatingChangeFinish}
            min={0}
            max={100}
            step={1}
            allowOverlap
            snapped
          />

          <View style={{ width: "100%", height: 400 }}>
            <Text style={styles.titleText}>Time</Text>
            <FlatList
              columnWrapperStyle={styles.columnWrapper}
              contentContainerStyle={styles.contentContainer}
              style={styles.flatList}
              data={data}
              numColumns={4}
              maxToRenderPerBatch={16}
              keyExtractor={item => Math.random() * 1000}
              renderItem={this.renderItem}
            />
          </View>
        </View>
      </View>
    );
  }
}
export default TimeRatingSlide;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  titleText: {
    fontFamily: "Roboto-Bold",
    alignSelf: "flex-start",
    marginStart: 10,
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    marginBottom: 10,
    fontSize: 16,
    color: "white",
  },
  textTitleItem: {
    fontFamily: "Roboto-Bold",
    alignSelf: "center",
    marginStart: 10,
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    marginBottom: 10,
    fontSize: 12,
    color: "white",
  },
  columnWrapper: {
    margin: 8,
  },
  flatList: {
    flex: 1,
    width: "100%",
  },
  contentContainer: {
    alignItems: "center",
  },
  touchableItem: {
    elevation: 10,
    height: hp("10%"),
    width: wp("20%"),
    backgroundColor: "#dadada",
    borderRadius: 4,
    marginRight: 8,
    marginLeft: 8,
    overflow: "hidden",
  },
  linearGradient: {
    elevation: 10,
    height: hp("10%"),
    width: wp("20%"),
    backgroundColor: "#dadada",
    overflow: "hidden",
    borderRadius: 4,
  },
});
