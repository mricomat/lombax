import { StyleSheet, Dimensions } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

const { height } = Dimensions.get("window");
const φ = (1 + Math.sqrt(5)) / 2;

export const MIN_HEADER_HEIGHT = 35;
export const MAX_HEADER_HEIGHT = height * (1 - 1 / φ) + 10; // 38% height
export const BUTTON_HEIGHT = 48;
export const BUTTON_WIDTH = 200;

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#191919",
    paddingTop: MIN_HEADER_HEIGHT - BUTTON_HEIGHT / 2,
    width: "100%",
  },
  swipeContainer: {
    flex: 1,
    width: "100%",
    backgroundColor: "#191919",
  },
  gradient: {
    position: "absolute",
    left: 0,
    bottom: 0,
    right: 0,
    alignItems: "center",
  },
  titleContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    textAlign: "center",
    color: "white",
    fontSize: 48,
    fontWeight: "bold",
    textShadowColor: "rgba(0, 0, 0, 0.9)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  placeHolder: {
    color: "#444444",
    borderRadius: 4,
    textAlign: "center",
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    width: wp("59%"),
  },
  textInputContainer: {
    backgroundColor: "#191919",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    marginTop: -BUTTON_HEIGHT,
  },
  sectionTextInput: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    width: wp("93%"),
    borderRadius: 5,
    margin: 10,
  },
  iconSearch: {
    padding: 10,
    margin: 5,
    resizeMode: "stretch",
    alignItems: "center",
  },
  flatList: {
    width: "100%",
  },
  contentContainer: {
    alignItems: "center",
  },
  touchableItem: {
    elevation: 10,
    height: hp("6%"),
    backgroundColor: "#dadada",
    borderRadius: 4,
    marginStart: 10,
  },
  linearGradient: {
    elevation: 10,
    alignItems: "center",
    height: hp("6%"),
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 4,
  },
  textTitleItem: {
    fontFamily: "Roboto-Bold",
    fontSize: 14,
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    color: "white",
  },
});
