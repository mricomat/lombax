import React from "react";
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from "react-native";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import FastImage from "react-native-fast-image";
import Icon from "react-native-ionicons";
import { searchCompaniesRequest } from "../../api/search";

class CompaniesSlide extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchResult: this.props.searchResult,
      companiesSearchValue: this.props.companiesSearchValue,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { searchResult, companiesSearchValue } = this.state;
    if (nextState.searchResult != searchResult) {
      return true;
    }
    if (nextState.companiesSearchValue != companiesSearchValue) {
      return true;
    }
    return false;
  }

  onEndReached = async distanceFromEnd => {
    // TODO Solve why is called all the time to the end
    const { searchResult, companiesSearchValue } = this.state;
    const length = searchResult.length - 1;
    const result = await searchCompaniesRequest(`where name = "${companiesSearchValue}"*`, length);
    const newGames = [...searchResult, ...result];
    this.setState({ searchResult: newGames });
  };

  renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity style={styles.itemContainer} onPress={() => console.log("OnPressCompannie")}>
        <Text style={styles.itemTitle}>{item.name}</Text>
        <TouchableOpacity style={styles.addIconContainer} onPress={() => this.props.addFilter({ ...item, type: "company" })}>
          <Icon style={styles.addIcon} name={"add"} size={25} color={"#FFFFFF"} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  render() {
    const { searchResult } = this.state;

    if (searchResult.length == 0) {
      return (
        <View style={styles.emptyContainer}>
          <Icon style={styles.emptyIcon} name={"ios-search"} size={100} color={"#FFFFFF"} />
          <Text style={styles.emptyText}>Find the companies that you are looking {"\n"}for your games</Text>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <FlatList
          extraData={this.state}
          ref={ref => (this.list = ref)}
          keyExtractor={item => item.id}
          data={searchResult}
          renderItem={this.renderItem}
          style={styles.flatList}
          maxToRenderPerBatch={25}
          onEndReachedThreshold={0.5}
          onEndReached={this.onEndReached}
        />
      </View>
    );
  }
}

export default CompaniesSlide;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginRight: 12,
    marginLeft: 12,
  },
  flatList: { flex: 1 },
  emptyContainer: {
    height: 350,
    justifyContent: "center",
    alignContent: "center",
  },
  emptyText: {
    fontFamily: "Roboto-Regular",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    alignSelf: "center",
    justifyContent: "center",
    textAlign: "center",
    fontSize: 14,
    marginTop: 15,
    color: "white",
  },
  emptyIcon: {
    alignSelf: "center",
    justifyContent: "center",
  },
  addIcon: {
    justifyContent: "center",
  },
  addIconContainer: {
    height: 40,
    justifyContent: "center",
    marginEnd: 15,
  },
  itemContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 40,
    backgroundColor: "#242424",
    marginTop: 5,
    borderRadius: 3,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
    elevation: 5,
  },
  itemTitle: {
    fontFamily: "Roboto-Medium",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    opacity: 0.9,
    justifyContent: "flex-start",
    fontSize: 16,
    marginStart: 15,
    color: "white",
  },
});
