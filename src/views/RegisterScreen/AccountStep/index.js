import React, { useState, useEffect } from "react";
import { Keyboard } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { isValidEmail, isValidUsername } from "../../../api2/auth";
import LTextInput from "../../../components/LTextInput";
import styles from "../styles";

const EMPTY = "";

export default (AccountStep = props => {
  const [username, setUsername] = useState("");
  const [validUsername, setValidUsername] = useState("");

  const [email, setEmail] = useState("");
  const [validEmail, setValidEmail] = useState("");

  const [password, setPassword] = useState("");
  const [validPassword, setValidPassword] = useState("");

  const [validRepeatPassword, setValidPasswordRepeat] = useState("");

  const validateEmail = async text => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      setValidEmail("The email must has correct format");
    } else {
      setValidEmail(EMPTY);
      const result = await isValidEmail(text);
      result ? setValidEmail("valid") : setValidEmail("The email already exists");
    }
  };

  const validateUsername = async text => {
    if (text === EMPTY) {
      setValidUsername("Please introduce your username");
    } else {
      setValidUsername(EMPTY);
      const result = await isValidUsername(text);
      result ? setValidUsername("valid") : setValidUsername("The email already exists");
    }
  };

  const validatePassword = async text => {
    const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))(?=.*[0-9]))(?=.{6,})");
    if (text === EMPTY) {
      setValidPassword("Please introduce your Password");
    } else if (mediumRegex.test(text) === false) {
      setValidPassword(
        "The password must contain at least 1 lowercase and uppercase character, at least 1 numeric character and must be 6 characters or longer ",
      );
    } else {
      setValidPassword("valid");
    }
  };

  const validateRepeatPassword = async text => {
    if (text !== password) {
      setValidPasswordRepeat("Please repeat your Password");
    } else {
      setValidPasswordRepeat("valid");
    }
  };

  const onChangeTextEmail = text => {
    setValidEmail(EMPTY);
    setEmail(text);
    validateEmail(text);
  };
  const onChangeUsername = text => {
    setValidUsername(EMPTY);
    setUsername(text);
    validateUsername(text);
  };
  const onChangePassword = text => {
    setValidPassword(EMPTY);
    setPassword(text);
    validatePassword(text);
  };

  const onChangeRepeatedPassword = async text => {
    setValidPasswordRepeat(EMPTY);
    await validateRepeatPassword(text);
  };

  useEffect(() => {
    validateAll();
  }, [validEmail, validUsername, validPassword, validRepeatPassword]);

  const validateAll = () => {
    if (validUsername == "valid" && validEmail == "valid" && validPassword == "valid" && validRepeatPassword == "valid") {
      //Keyboard.dismiss();
      props.ready({ username, email, password });
    } else {
      props.notReady();
    }
  };

  return (
    <KeyboardAwareScrollView style={{}}>
      <LTextInput
        onChangeText={text => onChangeUsername(text)}
        title={"Username"}
        autoCapitalize="none"
        placeholder={"Enter a Username"}
        valid={validUsername}
      />
      <LTextInput
        onChangeText={text => onChangeTextEmail(text)}
        title={"Email"}
        autoCapitalize="none"
        placeholder={"Enter your email"}
        valid={validEmail}
      />

      <LTextInput
        onChangeText={text => onChangePassword(text)}
        title={"Enter your password"}
        autoCapitalize="none"
        placeholder={"Enter your password"}
        secureTextEntry={true}
        valid={validPassword}
      />

      <LTextInput
        onChangeText={text => onChangeRepeatedPassword(text)}
        title={"Repeat your password"}
        autoCapitalize="none"
        placeholder={"Repeat your password"}
        secureTextEntry={true}
        valid={validRepeatPassword}
        style={[styles.textInput, { marginBottom: 15 }]}
      />
    </KeyboardAwareScrollView>
  );
});
