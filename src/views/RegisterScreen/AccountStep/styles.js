import { StyleSheet, StatusBar } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

export default (styles = StyleSheet.create({
  textInputTitle: {
    color: "white",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
  },
  textInput: {
    width: "100%",
    height: 40,
    borderRadius: 5,
    marginTop: 12,
    //marginBottom: 32,
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: "#191919",
    color: "white",
  },
}));
