import React, { useState, useEffect } from "react";
import { ScrollView, FlatList, Text, TouchableOpacity, View, Image } from "react-native";
import Icon from "react-native-ionicons";
import ImagePicker from "react-native-image-picker";
import deepDiffer from "react-native/Libraries/Utilities/differ/deepDiffer";

import { getImageUrl_2 } from "../../../utils/imageUris";
import styles from "./styles";

const avatarsMock = [
  { uri: "" },
  { uri: getImageUrl_2("5e9b3449bea5ee794a4d386e") },
  { uri: getImageUrl_2("5e9b36b6bea5ee794a4d387d") },
  { uri: getImageUrl_2("5e9b3643bea5ee794a4d3877") },
  { uri: getImageUrl_2("5e9b3662bea5ee794a4d387a") },
];
const backgroundMock = [
  { uri: "" },
  { uri: getImageUrl_2("5e9b3449bea5ee794a4d386e") },
  { uri: getImageUrl_2("5e9b36b6bea5ee794a4d387d") },
  { uri: getImageUrl_2("5e9b3643bea5ee794a4d3877") },
  { uri: getImageUrl_2("5e9b3662bea5ee794a4d387a") },
];

const options = {
  title: "Select Avatar",
  storageOptions: {
    skipBackup: true,
    path: "images",
  },
};

export default (AvatarStep = props => {
  const [avatar, setAvatar] = useState({ uri: getImageUrl_2("5e9b36b6bea5ee794a4d387d") });
  const [backgroundAvatar, setBackgroundAvatar] = useState({ uri: getImageUrl_2("5e9b3449bea5ee794a4d386e") });
  const [avatarSource, setAvatarSource] = useState("");
  const [backgroundSource, setBackgroundSource] = useState("");

  useEffect(() => {
    props.avatarSelected(avatar);
  }, [avatar]);

  useEffect(() => {
    props.backAvatarSelected(backgroundAvatar);
  }, [backgroundAvatar]);

  const showImagePicker = isAvatar => {
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else {
        ///const source = { uri: response.uri };
        console.log(response);
        // You can also display the image using data:
        const source = { uri: response };

        isAvatar ? setAvatarSource(source) : setBackgroundSource(source);
        isAvatar ? setAvatar(source) : setBackgroundAvatar(source);
      }
    });
  };

  const renderAvatarItems = ({ item, index, isAvatar }) => {
    const selected = isAvatar ? avatar : backgroundAvatar;

    return index !== 0 ? (
      <TouchableOpacity onPress={() => (isAvatar ? setAvatar(item) : setBackgroundAvatar(item))}>
        <View style={[styles.coverAvatarContainer, { backgroundColor: !deepDiffer(item, selected) ? "#117ea9" : "white" }]}>
          <Image source={item} style={styles.coverProfile} />
        </View>
      </TouchableOpacity>
    ) : (isAvatar ? (
        avatarSource
      ) : (
        backgroundSource
      )) ? (
      <TouchableOpacity onPress={() => (isAvatar ? setAvatar(avatarSource) : setBackgroundAvatar(backgroundSource))}>
        <View style={[styles.coverAvatarContainer, { backgroundColor: !deepDiffer(item, selected) ? "#117ea9" : "white" }]}>
          {/* <Image source={isAvatar ? avatarSource : backgroundSource} style={styles.coverProfile} /> */}
        </View>
      </TouchableOpacity>
    ) : (
      <View style={styles.coverAvatarContainer}>
        <TouchableOpacity style={[styles.coverProfile, { justifyContent: "center" }]} onPress={() => showImagePicker(isAvatar)}>
          <Icon style={{ alignSelf: "center" }} name={"ios-add"} size={35} color={"white"} />
        </TouchableOpacity>
      </View>
    );
  };

  const renderList = (data, isAvatar) => {
    return (
      <FlatList
        contentContainerStyle={styles.contentContainerInterest}
        style={styles.flatList}
        data={data}
        showsHorizontalScrollIndicator={true}
        horizontal
        maxToRenderPerBatch={12}
        keyExtractor={item => Math.random() * 1000}
        renderItem={({ item, index }) =>
          renderAvatarItems({
            item,
            index,
            isAvatar,
          })
        }
      />
    );
  };

  return (
    <ScrollView>
      <Text style={styles.avatarTitle}>Customize Avatar</Text>
      {renderList(avatarsMock, true)}
      <Text style={styles.avatarTitle}>Customize Background</Text>
      {renderList(backgroundMock, false)}
    </ScrollView>
  );
});
