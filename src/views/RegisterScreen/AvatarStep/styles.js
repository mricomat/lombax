import { StyleSheet, StatusBar } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

export default (styles = StyleSheet.create({
  avatarTitle: {
    fontFamily: "Roboto-Medium",
    fontSize: 17,
    color: "white",
    marginTop: 15,
    marginStart: 15,
    marginBottom: 10,
  },
  coverProfile: {
    width: 72,
    height: 72,
    borderRadius: 50,
    alignSelf: "center",
    backgroundColor: "#191919",
  },
  backgroundAvatar: {
    position: "absolute",
    width: "100%",
    height: 230,
  },
  contentContainerInterest: {
    alignItems: "center",
  },
  flatList: {
    width: "100%",
  },
  coverAvatarContainer: {
    width: 75,
    height: 75,
    backgroundColor: "white",
    borderRadius: 50,
    marginStart: 14,
    marginBottom: 10,
    justifyContent: "center",
  },
}));
