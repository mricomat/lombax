import React, { useState, useEffect } from "react";
import { Animated } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

export default (StepIndicator = props => {
  const [step, setStep] = useState(props.step);
  const [marginAnimated, setMargiAnimated] = useState(new Animated.Value(0));
  const [margin, setMargin] = useState(0);

  useEffect(() => {
    let marginLeft = 0;
    if ((step === 0 && props.step === 1) || (step === 2 && props.step === 1)) {
      marginLeft = marginAnimated.interpolate({
        inputRange: [0, 30],
        outputRange: [0, wp(530)],
      });
    } else if (step === 1 && props.step === 2) {
      marginLeft = marginAnimated.interpolate({
        inputRange: [0, 30],
        outputRange: [0, wp(1000)],
      });
    }
    setStep(props.step);

    Animated.timing(marginAnimated, {
      toValue: 2,
      duration: 500,
    }).start();

    setMargin(marginLeft);
  }, [props.step]);

  return <Animated.View style={[props.style, { marginLeft: margin }]} />;
});
