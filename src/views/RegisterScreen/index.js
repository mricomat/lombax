import ViewPager from "@react-native-community/viewpager";
import React from "react";
import { Animated, Dimensions, FlatList, Image, ScrollView, StatusBar, Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-ionicons";
import LinearGradient from "react-native-linear-gradient";
import Modal from "react-native-modal";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

import { register } from "../../api2/auth";
import InterestList from "../../components/InterestList";
import { genresThemesInfo } from "../../constants/Genres";
import AccountStep from "./AccountStep";
import AvatarStep from "./AvatarStep";
import StepIndicator from "./StepIndicator";
import styles from "./styles";

const width = Dimensions.get("window").width;

const colorsBackground = ["#0E0E0E", "rgba(0,0,0,0.00)", "rgba(0,0,0,0.00)", "#0E0E0E"];
const colorsFooter = ["rgba(0,0,0,0.00)", "#0E0E0E", "#0E0E0E", "#0E0E0E"];

class RegisterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 2,
      accountData: {},
      interests: [],
      avatar: "",
      backgroundAvatar: "",
      showNextButton: false,
      showModalInfo: false,
    };
    this.bottonWidth = new Animated.Value(width);
  }

  shouldComponentUpdate(nextState, nextProps) {
    const { currentStep, accountData, showNextButton, avatarSource } = this.state;
    if (nextState.currentStep != currentStep) {
      return true;
    }
    if (nextState.accountData != accountData) {
      return true;
    }

    if (nextState.showNextButton != showNextButton) {
      return true;
    }

    if (nextState.avatarSource != avatarSource) {
      return true;
    }
  }

  _widthItemSelected = name => {
    const width = name.length * wp("3.2%");
    if (name.length <= 5) {
      return width + 12;
    }
    if (name.length >= 9) {
      return width - 12;
    }
    return width;
  };

  _addInterest = interestData => {
    const { interests } = this.state;
    if (!interests.includes(interestData) && interests.length < 3) {
      this.setState(prevState => ({
        interests: [...prevState.interests, interestData],
      }));
    }
  };

  _removeFilter = filter => {
    const { interests } = this.state;
    const newInterests = interests.filter(item => {
      return item !== filter;
    });
    this.setState({
      interests: newInterests,
    });
  };

  _renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => this._removeFilter(item)}
        style={[styles.interestTouchable, { width: this._widthItemSelected(item.name) }]}>
        <LinearGradient
          start={{ x: 0.0, y: 0.25 }}
          end={{ x: 1, y: 1 }}
          style={[styles.gradientInterest, { flexDirection: "row", width: this._widthItemSelected(item.name) }]}
          colors={[item.colorLeft ? item.colorLeft : "#F59B23", item.colorRight ? item.colorRight : "#7B4E12"]}>
          <Text style={styles.textTitleItem}>{item.name}</Text>
          {/* <Icon style={{ marginStart: 5 }} name={"close"} size={21} color={"white"} /> */}
        </LinearGradient>
      </TouchableOpacity>
    );
  };

  _renderInterestsStep = () => {
    const { interests } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <View>
          {interests.length > 0 ? (
            <ScrollView style={{ height: 40, marginTop: 12 }}>
              <FlatList
                contentContainerStyle={styles.contentContainerInterest}
                style={styles.flatList}
                data={interests}
                showsHorizontalScrollIndicator={true}
                horizontal
                maxToRenderPerBatch={12}
                keyExtractor={item => Math.random() * 1000}
                renderItem={this._renderItem}
              />
            </ScrollView>
          ) : null}
        </View>

        <InterestList style={{ marginTop: 8 }} data={genresThemesInfo} onPress={this._addInterest} />
      </View>
    );
  };

  _renderModalInfo = () => {
    const { showModalInfo } = this.state;
    return (
      <View>
        <Modal
          isVisible={showModalInfo}
          onBackButtonPress={() => this.setState({ showModalInfo: false })}
          onBackdropPress={() => this.setState({ showModalInfo: false })}>
          <View style={styles.modal}>
            <Text style={styles.textModal}>Please select 3 genres</Text>
          </View>
        </Modal>
      </View>
    );
  };

  _registerUser = async () => {
    const { interests, accountData, avatar, backgroundAvatar } = this.state;
    const result = await register({
      username: accountData.username,
      email: accountData.email,
      password: accountData.password,
      //interests: [interests[0].id, interests[1].id, interests[2].id],
      avatar: avatar,
      background: backgroundAvatar,
    });
    console.log("_registerUser", result);
  };

  _nextStep = () => {
    const { currentStep, interests, accountData, avatar, backgroundAvatar } = this.state;
    if (currentStep != 2) {
      if (currentStep == 1 && interests.length != 3) {
        this.setState({ showModalInfo: true });
      } else {
        const newStep = currentStep + 1;
        this.viewPager.setPage(newStep);
        this.setState({ currentStep: newStep });
      }
    } else {
      this._registerUser();
    }
  };

  _returnStep = () => {
    const { currentStep } = this.state;
    const newStep = currentStep - 1;
    this.viewPager.setPage(newStep);
    this.setState({ currentStep: newStep });
  };

  _renderHeader = () => {
    const { currentStep, avatar, accountData, interests } = this.state;
    return (
      <View style={[styles.headerContainer, currentStep == 2 ? styles.headerContainerAvatar : styles.headerContainer]}>
        <View style={styles.headerTopContainer}>
          <TouchableOpacity
            style={{ position: "absolute", left: 0, marginStart: 15 }}
            onPress={() => this.props.navigation.goBack(null)}>
            <Icon name={"arrow-back"} size={23} color={"white"} />
          </TouchableOpacity>

          <View style={{ flexDirection: "row", alignSelf: "center" }}>
            <Image resizeMode={"contain"} style={{ width: 50, height: 50 }} source={require("../../assets/ice.png")} />
            <Text style={styles.titleRegister}>Register</Text>
          </View>
        </View>

        <View style={{ flexDirection: "row" }}>
          {currentStep == 2 ? (
            <View style={{ flexDirection: "row" }}>
              <View style={styles.coverAvatarContainer}>
                {/* <Image source={avatar} style={styles.coverProfile} /> */}
              </View>
              <View style={styles.profileNameContainer}>
                <Text style={styles.profileNameText}>{accountData.username}</Text>
                <Text style={styles.interestedIn}>Interested in:</Text>
                <Text style={styles.interestsText}>
                  {/* {interests[0].name} {interests[1].name} {interests[2].name} */}
                </Text>
              </View>
            </View>
          ) : null}
        </View>
        <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
          <Text style={[styles.titleStep, { color: currentStep != 0 ? "grey" : "white" }]}>Account</Text>
          <Text style={[styles.titleStep, { color: currentStep != 1 ? "grey" : "white" }]}>Interests</Text>
          <Text style={[styles.titleStep, { color: currentStep != 2 ? "grey" : "white" }]}>Avatar</Text>
        </View>
      </View>
    );
  };

  _accountStepReady = accountData => {
    this.setState({ accountData: accountData, showNextButton: true });
  };

  _hideNextButton = () => {
    if (this.state.showNextButton) {
      this.setState({ showNextButton: false });
    }
  };

  render() {
    const { currentStep, backgroundAvatar, showNextButton } = this.state;

    return (
      <View style={styles.container}>
        <StatusBar translucent={true} backgroundColor={currentStep == 2 ? "transparent" : "#191919"} />
        {currentStep == 2 ? (
          <View>
            <Image style={[styles.backgroundAvatar, { opacity: 0.5 }]} resizeMode={"cover"} source={backgroundAvatar} />
            <LinearGradient colors={colorsBackground} style={styles.backgroundAvatar} />
          </View>
        ) : null}

        {this._renderHeader()}
        <StepIndicator step={currentStep} style={styles.stepIndicator} />
        <ViewPager
          style={{ flex: 1 }}
          scrollEnabled={false}
          initialPage={currentStep}
          ref={viewPager => {
            this.viewPager = viewPager;
          }}>
          <AccountStep ready={data => this._accountStepReady(data)} notReady={() => this._hideNextButton()} />
          {this._renderInterestsStep()}

          <AvatarStep
            avatarSelected={avatar => this.setState({ avatar: avatar })}
            backAvatarSelected={backgroundAvatar => this.setState({ backgroundAvatar: backgroundAvatar })}
          />
        </ViewPager>
        {true && (
          <LinearGradient style={styles.gradientFooter} colors={colorsFooter}>
            {currentStep != 0 ? (
              <TouchableOpacity style={styles.backTouchable} onPress={this._returnStep}>
                <Icon style={{ marginEnd: 10, marginTop: 2 }} name={"ios-arrow-back"} size={23} color={"white"} />
                <Text style={styles.nextText}>Back</Text>
              </TouchableOpacity>
            ) : null}
            <Animated.View
              style={{
                width: currentStep != 0 ? width * 0.75 : width,
                alignSelf: "flex-end",
              }}>
              <View style={styles.linearGradient}>
                <TouchableOpacity style={[styles.nextTouchable, { width: "92%" }]} onPress={this._nextStep}>
                  <Text style={styles.nextText}>{currentStep != 2 ? "Next" : "Register"}</Text>
                </TouchableOpacity>
              </View>
            </Animated.View>
          </LinearGradient>
        )}
        {this._renderModalInfo()}
      </View>
    );
  }
}

export default RegisterScreen;
