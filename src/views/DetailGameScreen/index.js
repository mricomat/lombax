import moment from "moment";
import React from "react";
import {
  Dimensions,
  FlatList,
  LayoutAnimation,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  UIManager,
  View,
} from "react-native";
import FastImage from "react-native-fast-image";
import Icon from "react-native-ionicons";
import LinearGradient from "react-native-linear-gradient";
import { AirbnbRating } from "react-native-ratings";
import RBSheet from "react-native-raw-bottom-sheet";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import RatingStars from "../../components/RatingStars";
import ReviewsList from "../../components/ReviewsList";
import Navigation from "../../routes/Navigation";
import { getImageUrl } from "../../utils/imageUris";
import { ChartRating } from "./../../components/ChartRating";
import styles from "./styles";

const COLORS = ["#0E0E0E", "rgba(0,0,0,0.00)", "#0E0E0E"];
const MODALCOLORS = ["#2A64B5", "#144D93", "#0e3669", "#0E0E0E"];

const mock = [1, 1, 1, 26, 1, 45, 1, 1, 34, 1, 20, 1];
const mock2 = [1, 1, 2, 10, 15, 24, 1, 40, 26, 1, 20, 1];

const CustomLayoutAnimation = {
  duration: 200,
  create: {
    property: LayoutAnimation.Properties.opacity,
    type: LayoutAnimation.Types.linear,
  },
  update: {
    property: LayoutAnimation.Properties.opacity,
    type: LayoutAnimation.Types.linear,
  },
  delete: {
    duration: 200,
    property: LayoutAnimation.Properties.opacity,
    type: LayoutAnimation.Types.linear,
  },
};
class DetailGameScreen extends React.Component {
  constructor(props) {
    super(props);
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    console.log("DetailGameScreen", props.navigation.getParam("game"));
    this.state = {
      game: props.navigation.getParam("game"),
      isSummaryOpen: false,
      isModalVisible: false,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { game, isSummaryOpen, isModalVisible, similarGames } = this.state;

    if (game != nextState.game) {
      return true;
    }

    if (isSummaryOpen != nextState.isSummaryOpen) {
      return true;
    }

    if (isModalVisible != nextState.isModalVisible) {
      return true;
    }

    return false;
  }

  splitCompaniesNames = companies => {
    let text = "";
    if (companies) {
      companies.map((item, index) => {
        text = index != companies.length - 1 ? text + item.company.name + ", " : text + item.company.name;
      });
    }
    return text;
  };

  navigateYoutubeVideoView = () => {
    const { game } = this.state;
    if (game.videos) {
      Navigation.navigate("YoutubeVideoScreen", { idVideo: game.videos[0].video_id, fullScreen: true });
    }
  };

  _navigateToAddReview = () => {
    this.RBSheet.close();
    Navigation.navigate("AddReviewScreen", {});
  };

  // TODO Refactor
  _renderModal = () => {
    const { isModalVisible, game } = this.state;

    return (
      <RBSheet
        ref={ref => {
          this.RBSheet = ref;
        }}
        height={Dimensions.get("window").height / 2 + 20}
        duration={250}
        customStyles={{
          draggableIcon: {
            backgroundColor: "red",
            height: 0,
          },
          container: {
            backgroundColor: "#2A64B5",
            justifyContent: "center",
            alignItems: "center",
            width: "106%",
          },
        }}
        closeOnDragDown={true}
        animationType={"fade"}>
        <LinearGradient
          colors={MODALCOLORS}
          style={{
            flex: 1,
            backgroundColor: "#0E0E0E",
          }}>
          <View style={{ marginStart: 15, marginEnd: 15 }}>
            <Text style={{ fontFamily: "Roboto-Bold", color: "#dadada", fontSize: 15, marginTop: -8 }}>{game.name}</Text>
            <Text style={{ fontFamily: "Roboto-Medium", color: "#C1c1c9", fontSize: 15, marginTop: 5 }}>
              {moment.unix(game.first_release_date).format("YYYY")}
            </Text>
            <View style={{ width: "200%", height: 1, backgroundColor: "#c1c1c1", alignSelf: "center", marginTop: 10, opacity: 0.4 }} />
            <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginTop: 10 }}>
              <TouchableOpacity>
                <Icon name="logo-game-controller-b" color={"white"} size={50} />
                <Text style={{ fontFamily: "Roboto-Medium", color: "#C1c1c9", fontSize: 14, marginTop: -5, alignSelf: "center" }}>
                  Play
                </Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Icon name="heart-empty" color={"white"} size={48} />
                <Text style={{ fontFamily: "Roboto-Medium", color: "#C1c1c9", fontSize: 14, marginTop: -3, alignSelf: "center" }}>
                  Like
                </Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Icon name="logo-game-controller-b" color={"white"} size={50} />
                <Text style={{ fontFamily: "Roboto-Medium", color: "#C1c1c9", fontSize: 14, marginTop: -5, alignSelf: "center" }}>
                  Watchlist
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ width: "200%", height: 1, backgroundColor: "#c1c1c1", alignSelf: "center", marginTop: 10, opacity: 0.4 }} />
            <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "center", marginTop: 10 }}>
              <AirbnbRating showRating={false} count={5} defaultRating={11} size={33} />
            </View>
            <View style={{ width: "200%", height: 1, backgroundColor: "#c1c1c1", alignSelf: "center", marginTop: 10, opacity: 0.4 }} />
            <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => this._navigateToAddReview()}>
              <Icon name="add" color={"white"} size={20} />
              <Text style={{ fontFamily: "Roboto-Medium", color: "#C1c1c9", fontSize: 14, marginStart: 5, alignSelf: "center" }}>
                Review or log
              </Text>
            </TouchableOpacity>
            <View style={{ width: "200%", height: 1, backgroundColor: "#c1c1c1", alignSelf: "center", marginTop: 10, opacity: 0.4 }} />
            <TouchableOpacity style={{ flexDirection: "row" }}>
              <Icon name="list" color={"white"} size={20} />
              <Text style={{ fontFamily: "Roboto-Medium", color: "#C1c1c9", fontSize: 14, marginStart: 5, alignSelf: "center" }}>
                Add to a list
              </Text>
            </TouchableOpacity>
            <View style={{ width: "200%", height: 1, backgroundColor: "#c1c1c1", alignSelf: "center", marginTop: 10, opacity: 0.4 }} />
            <TouchableOpacity style={{ flexDirection: "row" }}>
              <Icon name="share" color={"white"} size={20} />
              <Text style={{ fontFamily: "Roboto-Medium", color: "#C1c1c9", fontSize: 14, marginStart: 5, alignSelf: "center" }}>
                Share
              </Text>
            </TouchableOpacity>
          </View>
        </LinearGradient>
      </RBSheet>
    );
  };

  _renderCoverImage = () => {
    const { game } = this.state;
    const cover = getImageUrl(game.cover ? game.cover.image_id : "", "t_cover_big");
    return (
      <View style={{ height: hp("23%"), width: wp("26%"), position: "absolute", alignSelf: "flex-end" }}>
        <FastImage
          style={styles.coverImage}
          source={{
            uri: cover,
            priority: FastImage.priority.high,
          }}
          resizeMode={FastImage.resizeMode.cover}
        />
        <View style={styles.playButtonContainer}>
          <TouchableOpacity style={styles.playButtonTouchable} onPress={() => this.navigateYoutubeVideoView()}>
            <Icon style={{ alignSelf: "center", marginStart: 1 }} name={"ios-play"} size={24} color={"#0E0E0E"} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  _renderDateTimeToBeat = () => {
    const { game } = this.state;
    return (
      <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
        <Text style={{ fontFamily: "Roboto-Bold", color: "#dadada", fontSize: 15 }}>
          {moment.unix(game.first_release_date).format("MMMM Do, YYYY")}
        </Text>

        {game.time_to_beat && (
          <View style={{ flexDirection: "row" }}>
            <View
              style={{
                marginStart: 5,
                marginEnd: 5,
                height: 5,
                width: 5,
                borderRadius: 20,
                alignSelf: "center",
                backgroundColor: "#dadada",
              }}
            />
            <Text style={{ fontFamily: "Roboto-Bold", color: "#dadada", fontSize: 14 }}>
              {moment()
                .utc(game.time_to_beat)
                .format("HHmmss")}
            </Text>
          </View>
        )}
      </View>
    );
  };

  tagWidth = tag => {
    const width = tag.length * wp("2.40%");
    if (tag.length <= 3) {
      return width + 13;
    }

    if (tag.length >= 9) {
      return width - 9;
    }
    return width;
  };

  _renderPlatformsInfo = () => {
    const { game } = this.state;
    return (
      <View style={{ width: "100%", height: 30, marginTop: 15 }}>
        <FlatList
          data={game.platforms}
          renderItem={({ item, index }) => {
            return (
              <TouchableOpacity style={[styles.touchableTag, { width: this.tagWidth(item.abbreviation) }]}>
                <Text style={styles.textTag}>{item.abbreviation}</Text>
              </TouchableOpacity>
            );
          }}
          style={{ flex: 1 }}
          horizontal
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };

  _renderMainDataSection = () => {
    const { game, isSummaryOpen } = this.state;

    return (
      <>
        <View
          style={{
            marginTop: -hp(3),
            marginEnd: 15,
            paddingLeft: 15,
          }}>
          {this._renderCoverImage()}
          <Text style={styles.gameTitle}>{game.name}</Text>
          <Text style={{ fontFamily: "Roboto-Regular", color: "#dadada", marginTop: 10 }}>Developed by</Text>
          <Text style={{ fontFamily: "Roboto-Bold", color: "#dadada", fontSize: 15, width: "70%" }}>
            {this.splitCompaniesNames(game.involved_companies)}
          </Text>
          {this._renderDateTimeToBeat()}
          {this._renderPlatformsInfo()}
          {game.summary ? (
            <>
              <Text
                style={[
                  styles.textSummary,
                  { height: isSummaryOpen ? "auto" : 60 },
                  { marginTop: game.involved_companies ? (game.involved_companies.length > 1 ? 15 : 20) : 15 },
                ]}>
                {game.summary}
              </Text>
              <TouchableWithoutFeedback
                onPress={() => {
                  LayoutAnimation.configureNext(CustomLayoutAnimation);
                  this.setState({ isSummaryOpen: !isSummaryOpen });
                }}>
                <View style={styles.summaryDotContainer}>
                  <View style={styles.summaryDot} />
                  <View style={styles.summaryDot} />
                  <View style={styles.summaryDot} />
                </View>
              </TouchableWithoutFeedback>
            </>
          ) : null}
        </View>
        <View style={styles.separator} />
      </>
    );
  };

  renderItemGamePlayedBy = ({ item, index }) => {
    return (
      <View style={{ marginEnd: 18 }}>
        <FastImage style={styles.itemPlayedBy} source={{ uri: "https://images.igdb.com/igdb/image/upload/t_cover_big/co1re6.jpg" }} />
        <View style={{ flexDirection: "row", justifyContent: "center", marginTop: 5 }}>
          <RatingStars size={11} />
        </View>
      </View>
    );
  };

  _renderGamePlayedSection = () => {
    return (
      <>
        <Text style={styles.ratingsTitle}>Game played by</Text>
        <View style={{ marginStart: 15, marginTop: 10, height: 65, marginBottom: 10 }}>
          <FlatList
            data={[1, 2, 3]}
            renderItem={this.renderItemGamePlayedBy}
            style={{}}
            horizontal
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <View style={styles.separator} />
      </>
    );
  };

  _renderTagsSection = (type, title) => {
    const { game } = this.state;
    let data = [];
    if (type == "genres") {
      const genres = game.genres ? game.genres : [];
      const themes = game.themes ? game.themes : [];
      const keywords = game.keywords ? game.keywords : [];

      data = [...genres, ...themes, ...keywords];
    } else {
      const gameModes = game.game_modes ? game.game_modes : [];
      data = [...gameModes];
    }

    return (
      <>
        {data.length > 0 ? (
          <>
            <Text style={styles.ratingsTitle}>{title}</Text>
            <View style={{ height: 30, marginTop: 15, marginStart: 15, marginEnd: 5, marginBottom: 10 }}>
              <FlatList
                data={data}
                renderItem={({ item, index }) => {
                  return (
                    <TouchableOpacity style={[styles.touchableTag, { width: this.tagWidth(item.name) }]}>
                      <Text style={styles.textTag}>{item.name}</Text>
                    </TouchableOpacity>
                  );
                }}
                style={{}}
                horizontal
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <View style={styles.separator} />
          </>
        ) : null}
      </>
    );
  };

  _renderReviewsSection = () => {
    return (
      <>
        <Text style={styles.ratingsTitle}>Reviews</Text>
        <View style={{ flex: 1, marginStart: 15, marginTop: 19, marginEnd: 15 }}>
          <ReviewsList />
          <TouchableOpacity onPress={() => Navigation.navigate("ReviewsGameScreen", {})}>
            <Text style={{ fontFamily: "Roboto-Medium", color: "#dadada", fontSize: 15, alignSelf: "center", marginBottom: 10 }}>
              All Reviews
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.separator} />
      </>
    );
  };

  _renderSimilarGamesSection = () => {
    const { game } = this.state;
    const similarGames = game.similar_games;

    return (
      <>
        {similarGames ? (
          <>
            <Text style={styles.ratingsTitle}>Similar games</Text>
            <View style={{ marginTop: 10, marginStart: 10, marginEnd: 5, marginBottom: 10 }}>
              <FlatList
                data={similarGames}
                renderItem={({ item, index }) => {
                  const cover = item.cover;
                  return (
                    <TouchableWithoutFeedback>
                      <FastImage
                        style={styles.similarImage}
                        source={{
                          uri: getImageUrl(cover ? cover.image_id : "", "t_cover_big"),
                          priority: FastImage.priority.normal,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                      />
                    </TouchableWithoutFeedback>
                  );
                }}
                style={{ marginEnd: 5 }}
                horizontal
                keyExtractor={(item, index) => item.toString()}
              />
            </View>
            <View style={styles.separator} />
          </>
        ) : null}
      </>
    );
  };

  _renderScreenshotsSection = () => {
    const { game } = this.state;
    return (
      <View style={{ marginTop: 10, marginStart: 10, marginEnd: 5, marginBottom: 10 }}>
        <FlatList
          data={game.screenshots}
          renderItem={({ item, index }) => {
            if (item.image_id) {
              return (
                <TouchableWithoutFeedback>
                  <FastImage
                    style={styles.screenshotImage}
                    source={{
                      uri: getImageUrl(item.image_id, "t_screenshot_big"),
                      priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                  />
                </TouchableWithoutFeedback>
              );
            }
          }}
          style={{ marginEnd: 5 }}
          horizontal
          keyExtractor={(item, index) => item.toString()}
        />
      </View>
    );
  };

  _renderChartRating = () => {
    return (
      <>
        <Text style={styles.ratingsTitle}>Ratings</Text>
        <View style={{ marginBottom: 16 }}>
          <ChartRating past={mock} future={mock2} minTemperature={0} maxTemperature={25} />
        </View>
        <View style={styles.separator} />
      </>
    );
  };

  render() {
    const { game } = this.state;

    const headerBackgroundCover = getImageUrl(
      game.screenshots ? game.screenshots[0].image_id : game.cover ? game.cover.image_id : "",
      "t_screenshot_med",
    );

    return (
      <View style={styles.container}>
        <ScrollView style={{ flex: 1 }}>
          <View style={{}}>
            <View style={styles.headerContainer}>
              <FastImage
                style={styles.imageHeader}
                source={{
                  uri: headerBackgroundCover,
                  priority: FastImage.priority.high,
                }}
                resizeMode={FastImage.resizeMode.cover}
              />
              <LinearGradient colors={COLORS} style={styles.linearGradientHeader}></LinearGradient>
              <TouchableOpacity style={styles.arrowBack} onPress={() => this.props.navigation.goBack()}>
                <Icon ios="arrow-back" android="arrow-back" color={"white"} size={24} />
              </TouchableOpacity>
            </View>

            {this._renderMainDataSection()}
            {this._renderChartRating()}
            {this._renderGamePlayedSection()}
            {this._renderTagsSection("genres", "Genres & Themes")}
            {this._renderReviewsSection()}

            {this._renderSimilarGamesSection()}
            {this._renderTagsSection("gameModes", "Game modes")}

            <Text style={styles.ratingsTitle}>Screenshots</Text>
            {this._renderScreenshotsSection()}
            <View style={styles.separator} />
          </View>
        </ScrollView>
        <TouchableOpacity onPress={() => this.RBSheet.open()} style={styles.modalButton}>
          <Icon style={{ alignSelf: "center", marginStart: 1 }} name={"ios-add"} size={30} color={"white"} />
        </TouchableOpacity>
        {this._renderModal()}
      </View>
    );
  }
}

export default DetailGameScreen;
