import React from "react";
import { FlatList, Image, StatusBar, Text, TouchableOpacity, View, ScrollView } from "react-native";
import FastImage from "react-native-fast-image";
import Icon from "react-native-ionicons";
import LinearGradient from "react-native-linear-gradient";

import { getMostAnticipated, getSevenDaysLater } from "../../api/games";
import Navigation from "../../routes/Navigation";
import styles from "./styles";
import { getImageUrl } from "../../utils/imageUris";
import ActivityBar from "../../components/ActivityBar";
import { ChartRating } from "./../../components/ChartRating";

const colorsBackground = ["#0E0E0E", "rgba(0,0,0,0.00)", "rgba(0,0,0,0.00)", "#0E0E0E"];

class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);

    const isOtherUser = props.navigation.getParam("isOtherUser");

    this.state = {
      loading: true,
      favorites: [],
      recentActivity: [],
      isOtherUser: isOtherUser,
      isFollow: false,
    };
  }

  async componentDidMount() {
    const anticipated = await getMostAnticipated();
    const sevenDays = await getSevenDaysLater();
    this.setState({
      favorites: anticipated,
      recentActivity: sevenDays,
      loading: false,
    });
  }

  _navigateToSettings = () => {
    Navigation.navigate("SettingsScreen", {});
  };

  _navigateToDetail = game => {
    Navigation.navigate("DetailGameView", {
      game,
    });
  };

  _navigateToMosaicScreen = item => {
    // TODO ADAPT MOSAIC SCREEN
  };

  _onBack = () => {
    this.props.navigation.pop();
  };

  _renderFavoritesItem = ({ item, index }) => {
    if (item.cover) {
      return (
        <TouchableOpacity style={{ marginStart: 5 }} onPress={() => this._navigateToDetail(item)}>
          <FastImage
            style={styles.imageItem}
            source={{
              uri: getImageUrl(item.cover.image_id, "t_cover_big"),
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </TouchableOpacity>
      );
    }
  };

  _renderRecentActivityItem = ({ item, index }) => {
    if (item.cover) {
      return (
        <TouchableOpacity style={{ marginStart: 5 }} onPress={() => this._navigateToDetail(item)}>
          <FastImage
            style={styles.imageItem}
            source={{
              uri: getImageUrl(item.cover.image_id, "t_cover_big"),
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
          <ActivityBar rating={3.2} isReview={true} isRePlayed={true} isLiked={true} />
        </TouchableOpacity>
      );
    }
  };

  _renderGamesSection = (title, data) => {
    return (
      <View>
        <View style={{ flexDirection: "row", alignContent: "center", marginTop: 15, marginStart: 10 }}>
          <Text style={styles.sectionTitle}>{title}</Text>

          <TouchableOpacity onPress={this._navigateToMosaicScreen({ title, data })}>
            <Icon
              style={{ alignSelf: "center", marginStart: 8, marginBottom: 2 }}
              name={"add-circle-outline"}
              size={22}
              color={"#FFFFFF"}
            />
          </TouchableOpacity>
        </View>

        <FlatList
          extraData={this.state}
          horizontal={true}
          ref={ref => (this.list = ref)}
          keyExtractor={item => item.id}
          data={data}
          renderItem={title != "Favorites" ? this._renderRecentActivityItem : this._renderFavoritesItem}
          style={styles.flatList}
        />
      </View>
    );
  };

  _renderMoreDataItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={{
          flexDirection: "row",
          height: 40,
          justifyContent: "space-between",
          marginTop: 5,
          alignItems: "center",
          marginStart: 5,
          marginEnd: 5,
        }}
        onPress={() => Navigation.navigate(item.onPress, { section: item.section })}>
        <Text style={{ color: "white", fontSize: 15, fontFamily: "Roboto-Medium" }}>{item.title}</Text>
        <Text style={{ color: "#C1c1c1", fontSize: 14, fontFamily: "Roboto-Regular" }}>2250</Text>
      </TouchableOpacity>
    );
  };

  _renderMoreDataSection = () => {
    const mockGames = this.state.favorites;
    const data = [
      { title: "Games", onPress: "MosaicScreen", section: { title: "Martin's Games", games: mockGames } },
      { title: "Diary", onPress: "SectionsProfileScreen", section: { title: "Diary" } },
      { title: "Reviews", onPress: "SectionsProfileScreen", section: { title: "Reviews" } },
      { title: "Watchlist", onPress: "MosaicScreen", section: { title: "Martin's Watchlist", games: mockGames } },
      { title: "Likes", onPress: "", section: {} },
      { title: "Following", onPress: "SectionsProfileScreen", section: { title: "Following" }},
      { title: "Followers", onPress: "SectionsProfileScreen", section: { title: "Followers" }},
    ];
    return (
      <View>
        <FlatList
          ref={ref => (this.list = ref)}
          keyExtractor={item => item.id}
          data={data}
          renderItem={this._renderMoreDataItem}
          style={styles.flatList}
        />
      </View>
    );
  };

  render() {
    const { recentActivity, favorites, isFollow, isOtherUser } = this.state;

    const past = [1, 1, 1, 26, 1, 45, 1, 1, 34, 1, 20, 1];
    return (
      <View style={styles.container}>
        <StatusBar translucent={true} backgroundColor={"transparent"} />
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.headerContainer}>
            <Image
              style={[styles.backgroundAvatar, { opacity: 0.5 }]}
              resizeMode={"cover"}
              source={require("../../assets/zelda.png")}
            />
            <LinearGradient colors={colorsBackground} style={styles.backgroundAvatar} />

            <View style={styles.headerTitleContainer}>
              {!isOtherUser ? (
                <>
                  <Image resizeMode={"contain"} style={{ width: 50, height: 50 }} source={require("../../assets/ice.png")} />
                  <Text style={styles.titleProfileScreen}>Profile</Text>
                  <TouchableOpacity
                    onPress={this._navigateToSettings}
                    style={{ position: "absolute", right: 0, alignSelf: "center", marginEnd: 15 }}>
                    <Icon name={"ios-cog"} size={22} color={"white"} />
                  </TouchableOpacity>
                </>
              ) : (
                <View style={{ flex: 1 }}>
                  <TouchableOpacity onPress={this._onBack} style={{ alignSelf: "flex-start", marginStart: 15, marginTop: 5 }}>
                    <Icon name={"arrow-back"} size={23} color={"white"} />
                  </TouchableOpacity>
                </View>
              )}
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={styles.coverProfileContainer}>
                <Image source={{ uri: "https://picsum.photos/200/300" }} style={styles.coverProfile} />
              </View>

              <View style={styles.profileNameContainer}>
                <View style={{ flexDirection: "row" }}>
                  <Text style={styles.profileNameText}>Cliew</Text>
                  <View style={styles.infoIconContainer}>
                    <Icon style={styles.infoIcon} name={"heart"} size={16.5} color={"#D75A4A"} />
                    <Text style={[styles.infoText, { marginRight: 15 }]}>30</Text>
                    <Icon style={styles.infoIcon} name={"chatbubbles"} size={16.5} color={"#72d8ff"} />
                    <Text style={styles.infoText}>30</Text>
                  </View>
                </View>

                <Text style={styles.interestedIn}>Interested in:</Text>
                <Text style={styles.interestsText}>Action Shooter Platform</Text>
              </View>
              {isOtherUser ? (
                <View style={{ flex: 1, justifyContent: "center" }}>
                  <TouchableOpacity
                    style={[styles.followTouchable, isFollow ? { borderColor: "#117ea9", width: 90 } : null]}
                    onPress={() => this.setState({ isFollow: !isFollow })}>
                    <Text style={styles.followText}>{isFollow ? "FOLLOWING" : "FOLLOW"}</Text>
                  </TouchableOpacity>
                </View>
              ) : null}
            </View>
          </View>

          {this._renderGamesSection("Favorites", favorites)}
          {this._renderGamesSection("Recent activity", recentActivity)}

          <View style={styles.separator} />
          <View style={{ paddingBottom: 15 }}>
            <Text
              style={{
                marginStart: 15,
                marginTop: 15,
                fontFamily: "Roboto-Bold",
                fontSize: 16,
                color: "white",
              }}>
              Ratings
            </Text>

            <ChartRating past={past} future={past} minTemperature={0} maxTemperature={25} />
          </View>
          <View style={styles.separator} />
          {this._renderMoreDataSection()}
        </ScrollView>
      </View>
    );
  }
}
export default ProfileScreen;
