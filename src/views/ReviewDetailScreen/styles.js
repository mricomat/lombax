import { StyleSheet, Platform, StatusBar } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

const STATUS_BAR_HEIGHT = StatusBar.currentHeight;

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
    backgroundColor: "#191919",
  },
  headerContainer: {
    flexDirection: "row",
    width: "100%",
    height: 60,
    backgroundColor: "#0F0F0F",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  headerTitle: {
    fontFamily: "Roboto-Bold",
    color: "white",
    marginTop: STATUS_BAR_HEIGHT - 5,
    fontSize: 16.5,
    marginStart: 15,
  },
  arrowIcon: {
    marginTop: STATUS_BAR_HEIGHT - 5,
    marginStart: 15,
  },
  detailContainer: {
    marginStart: 15,
    marginEnd: 15,
    marginTop: 15,
  },
  userContainer: {
    flexDirection: "row",
    alignContent: "center",
    width: wp(28),
  },
  userAvatar: {
    borderRadius: 30,
    width: wp(10.5),
    height: wp(10.5),
  },
  userAvatarComment: {
    borderRadius: 30,
    width: wp(9),
    height: wp(9),
  },
  userNameText: {
    fontFamily: "Roboto-Regular",
    alignSelf: "center",
    color: "#fff",
    marginStart: 10,
    fontSize: 15,
  },
  reviewDateText: {
    fontFamily: "Roboto-Regular",
    alignSelf: "center",
    color: "#fff",
    marginStart: 15,
    fontSize: 12,
  },
  gameCover: {
    position: "absolute",
    top: 0,
    right: 0,
    width: wp(26),
    height: hp(22),
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: "#817D7D",
  },
  titleContainer: {
    flexDirection: "row",
    marginTop: 13,
  },
  titleText: {
    fontFamily: "Roboto-Bold",
    alignSelf: "center",
    color: "#fff",
    fontSize: 19,
  },
  reviewText: {
    fontFamily: "Roboto-Regular",
    color: "#fff",
    fontSize: 13,
    flexWrap: "wrap",
    flexShrink: 1,
    marginTop: 6,
  },
  reviewTextComment: {
    fontFamily: "Roboto-Regular",
    color: "#fff",
    fontSize: 12.5,
    marginEnd: 4,
    flexWrap: "wrap",
    flexShrink: 1,
    marginStart: wp(9) + 10,
    marginTop: -6,
  },
  dateComment: {
    fontFamily: "Roboto-Regular",
    alignSelf: "center",
    color: "#fff",
    fontSize: 12,
    marginEnd: 2,
  },
  headerCommentContainer: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  likesContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginStart: 10,
  },
  separatorView: {
    backgroundColor: "white",
    width: "100%",
    height: 0.8,
    marginTop: 14,
  },
});
