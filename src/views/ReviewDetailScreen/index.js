import React from "react";
import { FlatList, ScrollView, Text, TouchableOpacity, View } from "react-native";
import FastImage from "react-native-fast-image";
import Icon from "react-native-ionicons";
import styles from "./styles";

class ReviewsScreen extends React.Component {
  
  _renderItem = ({ item, index }) => {
    return (
      <View>
        <View style={styles.detailContainer}>
          <View style={styles.headerCommentContainer}>
            <View style={styles.userContainer}>
              <FastImage
                style={styles.userAvatarComment}
                source={{ uri: "https://images.igdb.com/igdb/image/upload/t_cover_big/co1h9f.jpg" }}
              />
              <Text style={styles.userNameText}>Martin Rico</Text>
              <View style={styles.likesContainer}>
                <Icon style={{ justifyContent: "center" }} name={"heart"} size={14} color={"#D75A4A"} />
                <Text style={[styles.reviewDateText, { marginStart: 6 }]}>305</Text>
              </View>
            </View>
            <Text style={styles.dateComment}>7d</Text>
          </View>
          <Text style={styles.reviewTextComment}>
            Lorem ipsum dolor sit amet consectetur adipiscing elit auctor inceptos, varius sociosqu taciti commodo libero quam massa
          </Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Icon style={styles.arrowIcon} name={"arrow-back"} size={20} color={"white"} />
          </TouchableOpacity>

          <Text style={styles.headerTitle}>Martin's Review</Text>
        </View>
        <ScrollView>
          <View style={styles.detailContainer}>
            <View style={styles.userContainer}>
              <FastImage
                style={styles.userAvatar}
                source={{ uri: "https://images.igdb.com/igdb/image/upload/t_cover_big/co1h9f.jpg" }}
              />
              <Text style={styles.userNameText}>Martin Rico</Text>
              {/* <Text style={styles.reviewDateText}>November 10, 2019</Text> */}
            </View>
            <FastImage style={styles.gameCover} source={{ uri: "https://images.igdb.com/igdb/image/upload/t_cover_big/co1h9f.jpg" }} />
            <View style={styles.titleContainer}>
              <Text style={styles.titleText}>Hollow knight</Text>
              <Text style={styles.reviewDateText}>2019</Text>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 6 }}>
              <Icon name={"star"} size={16} color={"#A78864"} />
              <Icon name={"star"} size={16} color={"#A78864"} />
              <Icon name={"star"} size={16} color={"#A78864"} />
              <Icon name={"star"} size={16} color={"#A78864"} />
              <Icon name={"star"} size={16} color={"#A78864"} />
            </View>

            <Text style={styles.reviewText}>
              Lorem ipsum dolor sit amet consectetur {"\n"}adipiscing elit auctor inceptos, varius {"\n"}sociosqu taciti commodo libero
              quam massa
              {"\n"}fames vitae, id est ante lectus pellentesque dis potenti felis... Lorem ipsum dolor sit amet consectetur adipiscing
              elit auctor inceptos,varius sociosqu taciti commodo libero quam massa fames vitae, id est ante lectus pellentesque dis
            </Text>
            <View style={{ flexDirection: "row", marginTop: 6, justifyContent: "space-between", marginEnd: 5 }}>
              <View style={{ flexDirection: "row" }}>
                <Icon style={{ justifyContent: "center", marginTop: 2 }} name={"heart"} size={14} color={"#D75A4A"} />
                <Text style={[styles.reviewDateText, { marginStart: 8 }]}>305 Likes</Text>
              </View>
              <Text style={styles.reviewDateText}>November 10, 2019</Text>
            </View>

            <View style={styles.separatorView} />
          </View>
          <View style={{ flex: 1 }}>
            <FlatList
              scrollEnabled
              ref={ref => (this.list = ref)}
              keyExtractor={item => item}
              data={[1, 2, 3, 4, 5]}
              renderItem={this._renderItem}
              style={{ flex: 1 }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default ReviewsScreen;
