import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

//   index > 1 ? {flexDirection: 'column'} : {flexDirection: 'row'},

const tagWidth = tag => {
  const width = tag.length * wp('2.40%');
  if (tag.length <= 3) {
    return width + 13;
  }

  if (tag.length >= 9) {
    return width - 9;
  }
  return width;
};

const renderItem = (tag, alignItems) => {
  if (tag.includes('(')) {
    const start = tag.indexOf('(');
    const end = tag.indexOf(')');
    tag = tag.substring(start + 1, end);
  }
  return (
    <TouchableOpacity
      style={[
        styles.containerTag,
        {width: tagWidth(tag), alignSelf: alignItems},
      ]}>
      <Text style={styles.tagText}>{tag}</Text>
    </TouchableOpacity>
  );
};

const calculateNumberColumuns = (columns, length) => {
  if (columns) {
    return columns;
  }
  return length % 2 == 0 ? 2 : 3;
};

const TagsGroup = React.memo(({title, tags, columns, alignItems}) => {
  return (
    <View style={styles.container}>
      <Text style={[styles.titleText, {alignSelf: alignItems}]}>{title}</Text>
      <FlatList
        data={tags}
        renderItem={({item}) =>
          renderItem(
            item.abbreviation ? item.abbreviation : item.name,
            alignItems,
          )
        }
        style={styles.flatList}
        numColumns={calculateNumberColumuns(columns, tags.length)}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
});

const styles = StyleSheet.create({
  flatList: {
    flexGrow: 0,
  },
  container: {
    marginTop: 10,
  },
  containerTag: {
    marginBottom: 4,
    marginRight: 4,
    backgroundColor: 'grey',
    justifyContent: 'center',
    height: 25,
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: {height: 1, width: 1}, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 10, // Android
    borderRadius: 5,
  },
  titleText: {
    marginBottom: 5,
    textTransform: 'capitalize',
    fontFamily: 'Roboto-Regular',
    alignSelf: 'flex-start',
    fontSize: hp('2.1%'),
    color: '#DADADA',
  },
  tagText: {
    textTransform: 'capitalize',
    fontFamily: 'Roboto-Regular',
    alignSelf: 'center',
    fontSize: hp('1.9%'),
    color: '#DADADA',
  },
  dot: {
    alignSelf: 'center',
    width: 4.5,
    height: 4.5,
    borderRadius: 100 / 2,
    backgroundColor: '#C42727',
    marginStart: 5,
    marginEnd: 5,
  },
});

export default TagsGroup;
