import { LinearGradient, Path, Shape, Surface } from "@react-native-community/art";
import React, { Component } from "react";
import { Animated, Dimensions, StyleSheet, View, Text } from "react-native";
import Icon from "react-native-ionicons";
import { aggregate, AnimatedAggregation } from "./AnimatedAggregation";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

const CHART_WIDTH = Dimensions.get("window").width * 0.72;
const CHART_HEIGHT = 75;
const DIVISION = 12;
const UNIT_SIZE = CHART_WIDTH / DIVISION;

const AnimatedShape = Animated.createAnimatedComponent(Shape);

type Props = {
  past: Array[],
  future: Array[],
  minValue: number,
  maxValue: number,
  style: StyleSheet,
};

function calcHeight(minValue: number, maxValue: number, value: number): number {
  const ratio = (value - minValue) / (maxValue - minValue);
  return CHART_HEIGHT * (ratio * 0.6);
}

function areaChartPath(w: number, h: number, heights: number[]) {
  const points = heights.map((height, i) => ({
    x: (0.5 + i) * UNIT_SIZE,
    y: h - height,
  }));
  let i = 0;
  let path = Path()
    .moveTo(0, h)
    .lineTo(0, points[i].y)
    .lineTo(points[i].x, points[i].y);
  for (i = 1; i < DIVISION - 2; i++) {
    const p = points[i];
    const q = points[i + 1];
    const xc = (p.x + q.x) / 2;
    const yc = (p.y + q.y) / 2;
    path = path.curveTo(p.x, p.y, xc, yc);
  }
  path = path.curveTo(points[i].x, points[i].y, points[i + 1].x, points[i + 1].y).lineTo(CHART_WIDTH, points[i + 1].y);
  return path.lineTo(w, h).close();
}

type AnimatedPath<T> = {
  heights: Animated.Value[],
  path: AnimatedAggregation<T>,
};

function buildAnimatedPath(): AnimatedPath<*> {
  const animatedHeights = Array.from(Array(DIVISION)).map(() => new Animated.Value(0));
  const animatedPath = aggregate(animatedHeights, heights => areaChartPath(CHART_WIDTH, CHART_HEIGHT, heights));
  //console.log('aniamatedPath', animatedPath);
  return {
    heights: animatedHeights,
    path: animatedPath,
  };
}

function springPath(animatedPath: AnimatedPath<*>, minValue: number, maxValue: number, values: number): void {
  const heights = values.map(v => {
    return calcHeight(minValue, maxValue, v);
  });

  const anims = animatedPath.heights.map((ah, i) => {
    return Animated.spring(ah, {
      toValue: heights[i],
      friction: 3,
      tension: 50,
    });
  });

  Animated.parallel(anims).start();
}

export class ChartRating extends Component {
  state: {
    future: AnimatedPath<*>,
    past: AnimatedPath<*>,
  };

  constructor(props: Props) {
    super();
    this.state = {
      future: buildAnimatedPath(),
      past: buildAnimatedPath(),
    };
  }

  shouldComponentUpdate(nextState, nextProps) {
    const { future, past } = this.state;

    if (future != nextState.future) {
      return true;
    }

    if (past != nextState.past) {
      return true;
    }

    return false;
  }

  componentDidMount() {
    const { past, future } = this.state;
    const { minTemperature, maxTemperature } = this.props;

    let pastValues = this.props.past;
    let futureValues = this.props.future;

    springPath(past, minTemperature, maxTemperature, pastValues);
    springPath(future, minTemperature, maxTemperature, futureValues);
  }

  render() {
    const { style, past } = this.props;
    let colors = ["#67D7F5", "#3BB6EA", "#289FD1", "#2A64B5", "#144D93", "#0e3669", "#0E0E0E", "#0E0E0E"];

    let linearGradient = new LinearGradient(colors, 250, 0, 0, 180);
    const areaChart = (
      <View style={styles.areaChartContainer}>
        <Icon style={{ alignSelf: "flex-end", paddingStart: 10, paddingEnd: 10 }} name={"star"} size={12} color={"#144D93"} />

        <View style={styles.surfaceContainer}>
          <Surface width={CHART_WIDTH} height={CHART_HEIGHT} style={[{ backgroundColor: "tranparent" }]}>
            <AnimatedShape fill={linearGradient} d={this.state.past.path} />
            <AnimatedShape fill="#C1C1C166" d={this.state.future.path} />
          </Surface>
        </View>
        <View style={{ flexDirection: "row", alignSelf: "flex-end", marginEnd: 10 }}>
          {colors.map((color, index) => {
            if (index < 5) {
              return (
                <Icon
                  style={{
                    alignSelf: "flex-end",
                    paddingStart: index == 0 ? 5 : 1,
                  }}
                  name={"ios-star"}
                  size={11}
                  color={color}
                />
              );
            }
          })}
        </View>
      </View>
    );

    return <View style={[style, styles.container]}>{areaChart}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: CHART_HEIGHT,
    justifyContent: "flex-end",
    backgroundColor: "#0E0E0E",
  },
  surfaceContainer: {
    width: CHART_WIDTH,
    height: CHART_HEIGHT,
    alignItems: "center",
    alignSelf: "center",
  },
  areaChartContainer: {
    flexDirection: "row",
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    width: "100%",
    justifyContent: "space-between",
  },
});
