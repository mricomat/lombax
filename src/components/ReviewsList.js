import React from "react";
import { Text, StyleSheet, TouchableOpacity, View, FlatList } from "react-native";
import Icon from "react-native-ionicons";
import FastImage from "react-native-fast-image";

import RatingStars from "./RatingStars";

const ReviewsList = React.memo(({ reviews }) => {
  const renderReviewItem = item => {
    return (
      <TouchableOpacity style={styles.reviewContainer}>
        <TouchableOpacity>
          <FastImage style={styles.reviewImage} source={{ uri: "https://images.igdb.com/igdb/image/upload/t_cover_big/co1re6.jpg" }} />
        </TouchableOpacity>

        <View style={styles.infoReviewContainer}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
            }}>
            <Text style={styles.titleReview}>Matt</Text>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <RatingStars size={11} />
              <Icon name={"heart"} color={"#D75A4A"} size={17} style={{ marginStart: 8 }} />
            </View>
          </View>
          <Text numberOfLines={7} style={styles.detailReview}>
            Lorem ipsum dolor sit amet consectetura dipiscing elit auctor inceptos, varius sociosqu taciti commodo libero quam massa
            fames vitae, id est ante lectus pellentesque dis potenti felis... Lorem ipsum dolor sit amet consectetur adipiscing elit
            auctor inceptos,varius sociosqu taciti commodo libero quam massa fames vitae, id est ante lectus pellentesque dis
          </Text>
          <View style={styles.separatorReview} />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        data={[1, 2, 3]}
        renderItem={({ item, index }) => renderReviewItem(item)}
        style={{ flex: 1 }}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
});

const styles = StyleSheet.create({
  reviewContainer: {
    flexDirection: "row",
    marginBottom: 15,
  },
  reviewImage: {
    marginTop: 5,
    width: 47,
    height: 47,
    borderRadius: 50,
  },
  infoReviewContainer: {
    marginStart: 15,
    width: "80%",
  },
  titleReview: {
    fontFamily: "Roboto-Bold",
    color: "#dadada",
    fontSize: 16,
  },
  detailReview: {
    fontFamily: "Roboto-Regular",
    color: "#dadada",
    fontSize: 14,
    marginTop: 10,
    marginBottom: 15,
  },
  separatorReview: {
    height: 1,
    width: 500,
    opacity: 0.5,
    backgroundColor: "grey",
  },
});

export default ReviewsList;
