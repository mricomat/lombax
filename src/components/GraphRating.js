import React from "react";
import { View, Easing, Text, Dimensions, TouchableOpacity } from "react-native";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import * as Progress from "react-native-progress";
import Icon from "react-native-ionicons";

const dataStarsMock = [0.3, 0.5, 0.7, 0.85, 0.6];
const colorsMock = ["#4C9CBE", "#4E75AC", "#456295", "#37405E", "#5482BF"];

class GraphRating extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      progress: 0,
      indeterminate: true,
    };
  }

  componentDidMount() {
    this.circularProgresUsers.animate(this.props.rating ? this.props.rating : 0, 1000, Easing.quad);

    //this.circularProgresCritics.animate(75, 1000, Easing.quad);
    //this.circularProgresFriends.animate(91, 1000, Easing.quad);
  }
  render() {
    const rating = this.props.rating;

    return (
      <View
        style={{
          flexDirection: "column",
        }}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginEnd: 25,
            marginTop: 10,
            justifyContent: "space-between",
          }}>
          <View style={{ flexDirection: "column", alignItems: "center" }}>
            <AnimatedCircularProgress
              style={{ marginEnd: 5 }}
              ref={ref => (this.circularProgresUsers = ref)}
              lineCap="round"
              size={60}
              width={6}
              rotation={360}
              fill={50}
              tintColorSecondary="#1BAE4B"
              //arcSweepAngle={240}
              tintColor="white"
              backgroundColor="#3d5875">
              {fill => (
                <View style={{ alignItems: "center" }}>
                  <Text
                    style={{
                      fontSize: 15,
                      color: "#dadada",
                      fontFamily: "Roboto-Regular",
                    }}>
                    {this.props.rating ? ((this.props.rating / 100) * 5).toFixed(1) : "N/A"}
                  </Text>
                  <Text
                    style={{
                      color: "#dadada",
                      fontFamily: "Roboto-Regular",
                      fontSize: 11,
                    }}>
                    Great
                  </Text>
                </View>
              )}
            </AnimatedCircularProgress>
            {/* <Text style={{marginTop: 3, color: 'white'}}>Users</Text> */}
          </View>
          <View
            style={{
              flexDirection: "column",
              alignItems: "center",
              marginStart: 12,
            }}>
            {dataStarsMock.map((item, index) => {
              return (
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: -5,
                  }}>
                  <Icon ios="star" android="star" color={"#dadada"} size={12} style={{ opacity: 0.8 }} />
                  <Text
                    style={{
                      fontSize: 12,
                      marginLeft: 5,
                      color: "#dadada",
                      fontFamily: "Roboto-Regular",
                    }}>
                    {index + 1}
                  </Text>
                  <Progress.Bar
                    progress={item}
                    width={250}
                    height={8}
                    borderRadius={5}
                    borderColor={"#191919"}
                    color={colorsMock[index]}
                    style={{ margin: 3, opacity: 0.8 }}
                  />
                </View>
              );
            })}
          </View>
        </View>

        <View
          style={{
            flexDirection: "row",
            marginTop: 15,
            justifyContent: "center",
          }}>
          <TouchableOpacity
            style={{
              width: 50,
              marginBottom: 4,
              marginRight: 5,
              alignSelf: "center",
              justifyContent: "center",
              opacity: 0.8,
              backgroundColor: "#3B415E",
              height: 25,
              shadowColor: "rgba(0,0,0, .4)", // IOS
              shadowOffset: { height: 1, width: 1 }, // IOS
              shadowOpacity: 1, // IOS
              shadowRadius: 1, //IOS
              elevation: 10, // Android
              borderRadius: 5,
            }}>
            <Text
              style={{
                textTransform: "capitalize",
                fontFamily: "Roboto-Regular",
                alignSelf: "center",
                fontSize: hp("2%"),
                color: "#DADADA",
              }}>
              Users
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              width: 53,
              marginBottom: 4,
              marginRight: 5,
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: "grey",
              height: 25,
              shadowColor: "rgba(0,0,0, .4)", // IOS
              shadowOffset: { height: 1, width: 1 }, // IOS
              shadowOpacity: 1, // IOS
              shadowRadius: 1, //IOS
              elevation: 10, // Android
              borderRadius: 5,
            }}>
            <Text
              style={{
                textTransform: "capitalize",
                fontFamily: "Roboto-Regular",
                alignSelf: "center",
                fontSize: hp("2%"),
                color: "#DADADA",
              }}>
              Critics
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              width: 55,
              marginBottom: 4,
              marginRight: 5,
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: "grey",
              height: 25,
              shadowColor: "rgba(0,0,0, .4)", // IOS
              shadowOffset: { height: 1, width: 1 }, // IOS
              shadowOpacity: 1, // IOS
              shadowRadius: 1, //IOS
              elevation: 10, // Android
              borderRadius: 5,
            }}>
            <Text
              style={{
                textTransform: "capitalize",
                fontFamily: "Roboto-Regular",
                alignSelf: "center",
                fontSize: hp("2%"),
                color: "#DADADA",
              }}>
              Friends
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              width: 38,
              marginBottom: 4,
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: "grey",
              height: 25,
              shadowColor: "rgba(0,0,0, .4)", // IOS
              shadowOffset: { height: 1, width: 1 }, // IOS
              shadowOpacity: 1, // IOS
              shadowRadius: 1, //IOS
              elevation: 10, // Android
              borderRadius: 5,
            }}>
            <Text
              style={{
                textTransform: "capitalize",
                fontFamily: "Roboto-Regular",
                alignSelf: "center",
                fontSize: hp("2%"),
                color: "#DADADA",
              }}>
              All
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default GraphRating;
