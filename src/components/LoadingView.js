import React from "react";
import { Image, View } from "react-native";
import { PacmanIndicator } from "react-native-indicators";

const LoadingView = React.memo(({}) => {
  return (
    <View
      style={{
        backgroundColor: "black",
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
      }}>
      <Image resizeMode={"contain"} style={{ width: 70, height: 70 }} source={require("../assets/ice.png")} />
      <View
        style={{
          width: 40,
          height: 40,
          marginTop: 12,
        }}>
        <PacmanIndicator color="white" size={29} />
      </View>
    </View>
  );
});

export default LoadingView;
