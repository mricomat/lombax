import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const KeyWordsList = React.memo(({wordsList}) => {
  return (
    <View style={styles.container}>
      {wordsList ? wordsList.map((word, index) => {
        return (
          <View style={styles.container} key={index}>
            <Text style={styles.label}>{word.name}</Text>
            {index != wordsList.length - 1 ? <View style={styles.dot} /> : null}
          </View>
        );
      }) : null}
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  label: {
    textTransform: 'capitalize',
    fontFamily: 'Montserrat-Medium',
    alignSelf: 'center',
    fontSize: hp('1.9%'),
    color: 'white',
  },
  dot: {
    alignSelf: 'center',
    width: 4.5,
    height: 4.5,
    borderRadius: 100 / 2,
    backgroundColor: '#C42727',
    marginStart: 5,
    marginEnd: 5,
  },
});

KeyWordsList.propTypes = {
  wordsList: PropTypes.array,
};

export default KeyWordsList;
