import React from "react";
import { SafeAreaView, View, StatusBar, Dimensions } from "react-native";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

const { height } = Dimensions.get("window");
const HEIGTH_BOTTOM_BAR = 56;
const IMAGE_HEIGTH = height - HEIGTH_BOTTOM_BAR;

const SkeletonLoader = () => (
  <SafeAreaView style={{ flex: 1 }}>
    <SkeletonPlaceholder backgroundColor={"#eee"} minOpacity={0.2} maxOpacity={0.3}>
      <View
        style={{
          alignSelf: "center",
          position: "relative",
          borderRadius: 5,
          width: "80%",
          height: IMAGE_HEIGTH * 0.5,
          marginTop: 30,
        }}
      />
      <View
        style={{
          alignSelf: "center",
          position: "relative",
          width: "40%",
          borderRadius: 5,
          height: 20,
          marginTop: 10,
        }}
      />
      <View
        style={{
          alignSelf: "center",
          position: "relative",
          width: "75%",
          height: 20,
          borderRadius: 5,
          marginTop: 8,
        }}
      />
      <View
        style={{
          alignSelf: "center",
          position: "relative",
          width: "75%",
          height: 20,
          borderRadius: 5,
          marginTop: 5,
        }}
      />
      <View
        style={{
          alignSelf: "center",
          position: "relative",
          width: "75f%",
          height: 20,
          borderRadius: 5,
          marginTop: 5,
        }}
      />
      <View
        style={{
          marginTop: 10,
          flexDirection: "row",
          alignContent: "flex-start",
          borderRadius: 5,
        }}>
        {[0, 1, 2, 3, 4].map((_, index) => (
          <View
            style={{
              width: wp("29%"),
              height: hp("24%"),
              borderRadius: 5,
              marginStart: 5,
            }}></View>
        ))}
      </View>
    </SkeletonPlaceholder>
  </SafeAreaView>
);

export default SkeletonLoader;
