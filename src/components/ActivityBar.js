import React from "react";
import { Image, StyleSheet, View } from "react-native";
import Icon from "react-native-ionicons";

const colors = ["#144D93", "#2A64B5", "#289FD1", "#3BB6EA", "#67D7F5"];

const ActivityBar = React.memo(({ rating, isReview, isRePlayed, isLiked }) => {
  const renderStars = () => {
    const decimal = Math.floor(rating);
    const fraction = (rating % 1).toFixed(1);

    return colors.map((color, index) => {
      if (decimal > index) {
        return <Icon name={"ios-star"} size={12} color={color} />;
      } else if (fraction >= 0.5 && index == colors.length - 1) {
        return <Icon name={"ios-star-half"} size={12} color={color} />;
      }
    });
  };
  
  return (
    <View style={styles.container}>
      <View style={{ flexDirection: "row", alignItems: "center", marginStart: 2 }}>
        {rating ? renderStars() : null}
        {isLiked ? <Icon style={{ marginTop: 2, marginStart: 5 }} name={"heart"} size={14} color={"#D75A4A"} /> : null}
        {isRePlayed ? <Icon style={{ marginTop: 2, marginStart: 5 }} name={"sync"} size={14} color={"white"} /> : null}
        {isReview ? (
          <Image source={require("../assets/baseline.png")} style={{ height: 11, width: 11, marginEnd: 3, marginStart: 5 }} />
        ) : null}
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
  label: {
    fontFamily: "Roboto-Bold",
    marginStart: 9,
    alignSelf: "center",
    fontSize: 17,
    color: "black",
  },
});

export default ActivityBar;
