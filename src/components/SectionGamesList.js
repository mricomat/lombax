import React from "react";
import { View, FlatList, Text, Image, StyleSheet, TouchableWithoutFeedback, TouchableOpacity } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import FastImage from "react-native-fast-image";
import SplashScreen from "react-native-splash-screen";
import Icon from "react-native-ionicons";
import { connect } from "react-redux";

import YoutubeVideoView from "./YoutubeVideoView";
import Navigation from "../routes/Navigation";
import { getImageUrl } from "../utils/imageUris";
import LoadingView from "./LoadingView";
import { getPopularSection, getTopRatedSection, getGenreGamesSection, getCollectionsSection } from "../api/games";

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => ({
  games: state.sections.games,
  isFetching: state.sections.isFetching,
  sort: state.sections.sort,
  failure: state.sections.failure,
  error: state.sections.error,
});

class SectionGamesList extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { games, isFetching } = this.props;
    if (nextProps.games != games) {
      return true;
    }
    if (nextProps.isFetching != isFetching) {
      return true;
    }
    return false;
  }

  async componentDidMount() {}

  navigateToDetail = game => {
    Navigation.navigate("DetailGameScreen", {
      game,
    });
  };

  navigateToMosaicScreen = item => {
    Navigation.navigate("MosaicScreen", this.handleDataMosaicScreen(item));
  };

  handleDataMosaicScreen = item => {
    let wService;
    let genre = null;

    switch (item.type) {
      case "popular":
        wService = getPopularSection;
        break;
      case "collection":
        wService = getCollectionsSection;
        break;
      case "top":
        wService = getTopRatedSection;
        break;
      default:
        wService = getGenreGamesSection;
        genre = item.type;
        break;
    }
    return { offset: item.games.length - 1, wService: wService, section: item, genre: genre };
  };

  renderItem = ({ item, index }) => {
    if (item == "") {
      return <View style={[styles.imageItem, { backgroundColor: "#191919" }]} />;
    }
    let image_id;
    if (item.cover) {
      const cover = item.cover;
      image_id = cover.image_id;
    } else {
      image_id = item.image_id;
    }

    return (
      <TouchableWithoutFeedback onPress={() => this.navigateToDetail(item)}>
        <FastImage
          style={styles.imageItem}
          source={{
            uri: getImageUrl(image_id, "t_cover_big"),
            priority: FastImage.priority.normal,
          }}
          resizeMode={FastImage.resizeMode.cover}
        />
      </TouchableWithoutFeedback>
    );
  };

  renderCollectionItem = ({ item, index }) => {
    if (item == "") {
      return <View style={[styles.imageItem, { backgroundColor: "#191919" }]} />;
    }
    const cover = item.data.cover;
    if (cover) {
      return (
        <TouchableWithoutFeedback onPress={() => this.navigateToDetail(item)}>
          <FastImage
            style={styles.imageItemCollection}
            source={{
              uri: getImageUrl(cover.image_id, "t_cover_big"),
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </TouchableWithoutFeedback>
      );
    }
  };

  renderSection = ({ item, index }) => {
    if (item && item.type == "next_trailer" && item.games.video) {
      return (
        <View>
          <Text style={styles.headerSectionLabelVideo}>{item.title}</Text>
          <YoutubeVideoView idVideo={item.games.video.video_id} />
        </View>
      );
    }
    if (item.games && item.games.length > 0) {
      return (
        <View>
          <View style={{ flexDirection: "row", alignContent: "center", marginTop: 15 }}>
            <Text style={item.title == "COLLECTION GAMES" ? styles.headerSectionLabelCollection : styles.headerSectionLabel}>
              {item.title}
            </Text>
            {item.title ? (
              <TouchableOpacity onPress={() => this.navigateToMosaicScreen(item)}>
                <Icon
                  style={{ alignSelf: "center", marginStart: 8, marginBottom: 2 }}
                  name={"add-circle-outline"}
                  size={22}
                  color={"#FFFFFF"}
                />
              </TouchableOpacity>
            ) : null}
          </View>

          <FlatList
            extraData={this.state}
            horizontal={true}
            ref={ref => (this.list = ref)}
            keyExtractor={item => item.id}
            data={item.games}
            windowSize={10}
            removeClippedSubviews={true}
            initialNumToRender={6}
            renderItem={item.title == "COLLECTION GAMES" ? this.renderCollectionItem : this.renderItem}
            style={styles.flatList}
          />
        </View>
      );
    }
  };

  render() {
    const { games, sort } = this.props;
    const placeHolderSection = [{ title: "Popular right now", type: "popular", games: ["", "", "", ""] }];
    if (games && games.length != 0) {
      SplashScreen.hide();
    }
    const data =
      (sort && sort.type) == "all genres"
        ? games[games.findIndex(item => item.type == "all genres")]
        : games[games.findIndex(item => item.type == sort.name)];

    return (
      <FlatList
        extraData={this.state}
        ref={ref => (this.list = ref)}
        keyExtractor={item => item.title}
        data={data && data.sections && data.sections.length != 0 ? data.sections : placeHolderSection}
        renderItem={this.renderSection}
        style={styles.flatList}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionGamesList);

const styles = StyleSheet.create({
  flatList: {
    flex: 1,
    marginStart: 3,
    marginTop: 10,
  },
  headerSectionLabelVideo: {
    marginTop: 15,
    marginStart: 8,
    fontFamily: "Montserrat-Bold",
    fontSize: 14,
    color: "white",
  },
  headerSectionLabel: {
    marginStart: 8,
    fontFamily: "Montserrat-Bold",
    fontSize: 14,
    color: "white",
  },
  headerSectionLabelCollection: {
    marginStart: 8,
    fontFamily: "Montserrat-Bold",
    fontSize: 14,
    color: "white",
  },
  imageItem: {
    resizeMode: "stretch",
    width: wp("29%"),
    height: hp("24%"),
    marginStart: 5,
    borderRadius: 4,
  },
  imageItemCollection: {
    resizeMode: "stretch",
    width: wp("37%"),
    height: hp("36%"),
    marginStart: 5,
    borderRadius: 4,
  },
});
