import React from "react";
import { Text, View, Image, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import LinearGradient from "react-native-linear-gradient";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import Icon from "react-native-ionicons";

const { height } = Dimensions.get("window");
const HEIGTH_BOTTOM_BAR = 56;
const IMAGE_HEIGTH = height - HEIGTH_BOTTOM_BAR;

const COLORS = ["rgba(0,0,0,0.00)", "rgba(26, 26, 26, 0.85)", "rgba(26, 26, 26, 0.95)", "rgba(26, 26, 26, 1)"];

const LOCATIONS = [0.5, 0.8, 0.9, 1];

const MainImageView = React.memo(({ imageUri }) => {
  const createRGBWithOpacity = (hex, a) => {
    if (!hex) return;

    let emptyHex = hex.substring(hex.indexOf("#") + 1);
    var r = parseInt(emptyHex.substring(0, 2), 16);
    var g = parseInt(emptyHex.substring(2, 4), 16);
    var b = parseInt(emptyHex.substring(4, 6), 16);

    r = isNaN(r) ? 0 : r;
    g = isNaN(g) ? 0 : g;
    b = isNaN(b) ? 0 : b;

    let rgba = `rgba(${r}, ${g}, ${b}, ${a})`;

    return rgba;
  };

  const getColorUp = () => {
    let arr = [0.9, 0.9, 0.7, 0.5, 0.5, 0.3, 0];
    var arrRGBA = [];
    let color = "#1A1A1A";

    for (var i = 0; i < arr.length; i++) {
      arrRGBA.push(createRGBWithOpacity(color, arr[i]));
    }
    return arrRGBA;
  };

  return (
    <View>
      <Image source={imageUri} style={styles.image}></Image>
      <LinearGradient colors={getColorUp()} style={styles.linearGradientTop}></LinearGradient>
      <LinearGradient locations={LOCATIONS} colors={COLORS} style={styles.linearGradientAll}></LinearGradient>
    </View>
  );
});

const styles = StyleSheet.create({
  image: {
    position: "absolute",
    width: "100%",
    height: IMAGE_HEIGTH,
    backgroundColor: "#191919",
    resizeMode: "cover",
  },
  linearGradientAll: {
    position: "absolute",
    width: "100%",
    height: IMAGE_HEIGTH,
  },
  linearGradientTop: {
    position: "absolute",
    width: "100%",
    height: 100,
  },
});

MainImageView.propTypes = {
  imageUri: PropTypes.any,
};

export default MainImageView;
