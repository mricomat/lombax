import React from "react";
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import Icon from "react-native-ionicons";

const ButtonPlay = React.memo(({ onPress }) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Icon name={"play"} size={24} color={"#000000"} />
      <Text style={styles.label}>Play Trailer</Text>
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: 145,
    height: 38,
    borderRadius: 5,
    backgroundColor: "white",
  },
  label: {
    fontFamily: "Roboto-Bold",
    marginStart: 9,
    alignSelf: "center",
    fontSize: 17,
    color: "black",
  },
});

ButtonPlay.propTypes = {
  wordsList: PropTypes.array,
};

export default ButtonPlay;
