import React from "react";
import { StyleSheet, Text, TextInput, View } from "react-native";
import { Tooltip } from "react-native-elements";
import Icon from "react-native-ionicons";

export default (LTextInput = props => {
  const tooltip = () => {
    return (
      <Tooltip
        popover={<Text style={{ fontFamily: "Roboto-Regular" }}>{props.valid}</Text>}
        overlayColor={"rgba(250, 250, 250, 0.70"}
        backgroundColor={"white"}>
        <Icon style={{ marginStart: 11 }} name="alert" color={"#e50000"} size={20} />
      </Tooltip>
    );
  };

  return (
    <View
      style={{
        width: "93%",
        alignSelf: "center",
      }}>
      {props.title && (
        <View style={{ flexDirection: "row", alignItems: "center", marginTop: 16 }}>
          <Text style={styles.textInputTitle}>{props.title}</Text>
          {props.valid != "valid" && props.valid != "" && tooltip()}
          {props.valid == "valid" && <Icon style={{ marginStart: 11 }} name="checkmark" color={"#117ea9"} size={20} />}
        </View>
      )}
      <TextInput
        autoFocus={false}
        autoCorrect={false}
        style={styles.textinput}
        placeholderTextColor={"grey"}
        placeholder={props.placeholder}
        keyboardAppearance="dark"
        {...props}
      />
    </View>
  );
});

const styles = StyleSheet.create({
  textinput: {
    width: "100%",
    height: 40,
    borderRadius: 5,
    marginTop: 12,
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: "#191919",
    color: "white",
  },
  textInputTitle: {
    color: "white",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
  },
});
