import React from "react";
import { View, StyleSheet } from "react-native";
import YouTube from "react-native-youtube";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { BackHandler } from "react-native";

import LoadingView from "./LoadingView";

class YoutubeVideoView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      idVideo: props.navigation.getParam("idVideo"),
      fullScreen: props.navigation.getParam("fullScreen"),
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState != this.state) {
      return true;
    }
    return false;
  }

  render() {
    const { idVideo, fullScreen } = this.state;
    if (!fullScreen) {
      this.props.navigation.pop();
    }

    return (
      <View style={styles.container}>
        <LoadingView style={{ position: "absolute", top: 0, flex: 1, width: "100%", height: "100%" }} />
        <YouTube
          apiKey="AIzaSyAUSNAYreW6N5hrdyjEltRxUEWHdl-oBCM"
          videoId={idVideo} // The YouTube video ID
          fullscreen={fullScreen}
          play={false} // control playback of video with true/false
          loop // control whether the video should loop when ended
          onChangeFullscreen={e => {
            this.setState({ fullScreen: e.isFullscreen });
          }}
          onError={e => console.log("ERROR", e)}
          style={styles.youtube}
        />
      </View>
    );
  }
}

export default YoutubeVideoView;

const styles = StyleSheet.create({
  youtube: {
    alignSelf: "stretch",
  },
  container: {
    flex: 1,
  },
});
