import React from "react";
import Icon from "react-native-ionicons";

let colorsX = ["#67D7F5", "#3BB6EA", "#289FD1", "#2A64B5", "#144D93", "#0e3669", "#0E0E0E", "#0E0E0E"];

const RatingStars = React.memo(({ size }) => {
  return (
    <>
      {colorsX.map((color, index) => {
        if (index < 5) {
          return <Icon style={{}} name={"ios-star"} size={size} color={colorsX[index]} />;
        }
      })}
    </>
  );
});

export default RatingStars;
