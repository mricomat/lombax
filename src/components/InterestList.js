import React from "react";
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import FastImage from "react-native-fast-image";
import Icon from "react-native-ionicons";
import LinearGradient from "react-native-linear-gradient";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import { getImageUrl } from "../utils/imageUris";

const InterestList = React.memo(({ data, title, onPress, style }) => {
  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => onPress(item)} style={styles.touchableItem}>
        <LinearGradient
          start={{ x: 0.0, y: 0.25 }}
          end={{ x: 1, y: 1 }}
          style={styles.linearGradient}
          colors={[item.colorLeft ? item.colorLeft : "#F59B23", item.colorRight ? item.colorRight : "#7B4E12"]}>
          <View
            style={{
              backgroundColor: "white",
              position: "absolute",
              right: -25,
              top: 15,
              borderRadius: 3,
              transform: [{ rotate: "22deg" }],
              width: wp("25%"),
              height: hp("13%"),
              overflow: "hidden",
            }}>
            <FastImage
              style={{
                alignSelf: "flex-start",
                width: wp("22%"),
                height: hp("18%"),
                marginStart: -9,
                marginTop: -7,
                transform: [{ rotate: "-22deg" }],
              }}
              source={{
                uri: getImageUrl(item.image_id, "t_cover_big"),
                priority: FastImage.priority.cover,
              }}
            />
          </View>

          <View
            style={{
              flexDirection: "row",
              marginTop: 12,
            }}>
            <Text style={styles.textTitleItem}>{item.name}</Text>
            <Icon style={styles.iconInfo} name={"information-circle-outline"} size={21} color={"white"} />
          </View>
          <Text style={styles.textDetailItem}>{"Borderlands 3"}</Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  };
  return (
    <View style={[styles.container, style]}>
      {title ? <Text style={styles.titleGenre}>{title}</Text> : null}

      <View style={{ width: "100%", height: "100%" }}>
        <FlatList
          columnWrapperStyle={styles.columnWrapper}
          contentContainerStyle={styles.contentContainer}
          style={styles.flatList}
          data={data}
          numColumns={2}
          maxToRenderPerBatch={12}
          keyExtractor={item => Math.random() * 1000}
          renderItem={({ item }) => renderItem({ item })}
        />
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },

  iconInfo: {
    marginTop: 1,
    marginStart: 5,
    alignSelf: "center",
  },
  flatList: {
    flex: 1,
    width: "100%",
    paddingBottom: 50,
  },
  columnWrapper: {
    margin: 8,
  },
  contentContainer: {
    alignItems: "center",
    paddingBottom: 80,
  },
  titleGenre: {
    fontFamily: "Roboto-Bold",
    alignSelf: "flex-start",
    marginStart: 17,
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    marginBottom: 10,
    fontSize: 18.5,
    color: "white",
  },
  touchableItem: {
    elevation: 10,
    height: hp("14.6%"),
    width: wp("43.2%"),
    backgroundColor: "#dadada",
    borderRadius: 4,
    marginRight: 8,
    marginLeft: 8,
    overflow: "hidden",
  },
  linearGradient: {
    elevation: 10,
    height: hp("14.6%"),
    width: wp("43.2%"),
    backgroundColor: "#dadada",
    overflow: "hidden",
    borderRadius: 4,
  },
  textTitleItem: {
    fontFamily: "Roboto-Bold",
    marginStart: 10,
    fontSize: 16,
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    color: "white",
  },
  textDetailItem: {
    fontFamily: "Roboto-Regular",
    marginStart: 10,
    fontSize: 11,
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    color: "white",
  },
});

export default InterestList;
