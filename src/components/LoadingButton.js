import React from "react";
import { ActivityIndicator, Animated, StyleSheet, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-ionicons";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create({
  button: {
    alignSelf: "center",
    width: wp(85),
    paddingVertical: 10,
    borderRadius: 40,
    elevation: 4,
    height: 45,
    justifyContent: "center",
    marginTop: 20,
    backgroundColor: "#1169c6",
  },
  text: {
    textAlign: "center",
    fontSize: 14,
  },
});

class LoadingButton extends React.Component {
  constructor() {
    super();

    this.state = {
      loading: false,
      success: false,
      error: false,
    };
    this.onPress = this.onPress.bind(this);
    this.animateRotation = new Animated.Value(0);
  }

  onPress() {
    this.setState({ loading: true }, () => this.props.onPress());
  }

  onSuccess = () => {
    this.setState({ success: true, loading: false, error: false });
  };

  onError = () => {
    this.setState({ loading: false, error: true }, () => this.shake());
  };

  cleanError = () => {
    this.setState({ error: false, loading: false });
  };

  renderText() {
    const textColor = { color: "white" };
    if (this.state.loading) {
      return <ActivityIndicator color={textColor.color} />;
    } else if (this.state.success) {
      return <Icon style={{ alignSelf: "center" }} name={"checkmark"} size={18} color={"white"} />;
    } else if (this.state.error) {
      return <Icon style={{ alignSelf: "center" }} name={"ios-alert"} size={18} color={"white"} />;
    } else {
      return (
        <Text
          style={{
            ...styles.text,
            ...textColor,
            ...this.props.textStyle,
          }}>
          {this.props.title}
        </Text>
      );
    }
  }

  shake = () => {
    Animated.sequence([
      Animated.timing(this.animateRotation, {
        toValue: 1,
        duration: 60,
        useNativeDriver: true,
      }),
      Animated.timing(this.animateRotation, {
        toValue: -1,
        duration: 60,
        useNativeDriver: true,
      }),
      Animated.timing(this.animateRotation, {
        toValue: 1,
        duration: 60,
        useNativeDriver: true,
      }),
      Animated.timing(this.animateRotation, {
        toValue: -1,
        duration: 60,
        useNativeDriver: true,
      }),
      Animated.spring(this.animateRotation, {
        toValue: 0,
        bounciness: 1,
        useNativeDriver: true,
      }),
    ]).start();
  };

  render() {
    const rotation = this.animateRotation.interpolate({
      inputRange: [0, 360],
      outputRange: ["0deg", "360deg"],
    });

    return (
      <TouchableOpacity {...this.props} onPress={this.onPress}>
        <Animated.View
          style={{
            ...styles.button,
            backgroundColor: this.state.error ? "#bc0000" : "#1169c6",
            transform: [
              {
                rotate: rotation,
              },
            ],
          }}>
          {this.renderText()}
        </Animated.View>
      </TouchableOpacity>
    );
  }
}

export default LoadingButton;
