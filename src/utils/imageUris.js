const BASE_URL_IMAGE = "https://images.igdb.com/igdb/image/upload/";
const BASE_URL_IMAGE_2 = "https://lombax.herokuapp.com/avatar";

export const getImageUrl = (imageId, size) => {
  return BASE_URL_IMAGE + `${size}/${imageId}.jpg`;
};

export const getImageUrl_2 = imageId => {
  return BASE_URL_IMAGE_2 + `/${imageId}`;
};
