export default class Utils {
  static createRGBWithOpacity(hex: string, a: number): string {
    if (!hex) return;

    let emptyHex: string = hex.substring(hex.indexOf("#") + 1);
    var r: number = parseInt(emptyHex.substring(0, 2), 16);
    var g: number = parseInt(emptyHex.substring(2, 4), 16);
    var b: number = parseInt(emptyHex.substring(4, 6), 16);

    r = isNaN(r) ? 0 : r;
    g = isNaN(g) ? 0 : g;
    b = isNaN(b) ? 0 : b;

    let rgba = `rgba(${r}, ${g}, ${b}, ${a})`;

    return rgba;
  }
}
