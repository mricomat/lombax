import { applyMiddleware, createStore, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import Reactotron from "../ReactotronConfig";

import reduxFreeze from "redux-freeze";
import environment from "../config/environment.config";
import rootReducer from "./rootReducer";
import rootSagas from "./rootSagas";

let sagaMiddleware;
let enhancers;

if (environment.isDevelopment) {
  sagaMiddleware = createSagaMiddleware({
    sagaMonitor: Reactotron.createSagaMonitor(),
  });
  enhancers = compose(applyMiddleware(sagaMiddleware), Reactotron.createEnhancer(), applyMiddleware(reduxFreeze));
} else {
  sagaMiddleware = createSagaMiddleware();
  enhancers = compose(applyMiddleware(sagaMiddleware));
}

const store = createStore(rootReducer, enhancers);
sagaMiddleware.run(rootSagas);

export default store;
