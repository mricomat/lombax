import { combineReducers } from "redux";
import sections from "./sections/SectionsReducer";
import login from "./login/LoginReducer";

export default combineReducers({
  sections,
  login,
});
