import { all } from "redux-saga/effects";

import Sections from "./sections/SectionsSaga";
import Login from "./login/LoginSaga";

const root = function* root() {
  yield all([Sections(), Login()]);
};

export default root;
