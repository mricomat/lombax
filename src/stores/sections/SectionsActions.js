import * as types from "../actionTypes";

export const sectionsRequest = sort => {
  return {
    type: types.SECTIONS.REQUEST,
    sort,
  };
};

export const sectionsSuccess = sections => {
  return {
    type: types.SECTIONS.SUCCESS,
    sections,
  };
};

export const sectionsFailure = err => {
  return {
    type: types.SECTIONS.FAILURE,
    error: err,
  };
};
