import {
  getPopularSection,
  getMyListSection,
  getTopRatedSection,
  getGenreGamesSection,
  getCollectionsSection,
  getMainGameData,
} from "../../api/games";
import rootStore from "../rootStore";

let sections = [];

const sectionCallBack = (result, position, title) => {
  sections[position] = { title: title, games: result };
};

export default class SectionsEffects {
  static async requestSections() {
    sections = [];
    const indie = { id: 32, type: "genres", name: "Indie" };
    const strategy = { id: 15, type: "genres", name: "Strategy" };
    const shooter = { id: 5, type: "genres", name: "Shooter", image_id: "co1re6" };

    await Promise.all([
      getMainGameData().then(result => sectionCallBack(result, 8, "highLight")),
      getPopularSection().then(result => sectionCallBack(result, 0, "Trending right now")),
      getTopRatedSection().then(result => sectionCallBack(result, 1, "Top rating games of this year")),
      getMyListSection().then(result => sectionCallBack(result, 2, "Popular from friends")),
      getCollectionsSection().then(result => sectionCallBack(result, 3, "COLLECTION GAMES")),
      getMyListSection().then(result => sectionCallBack(result, 4, "My list")),
      getGenreGamesSection(0, strategy).then(result => sectionCallBack(result, 5, "Strategy games")),
      getGenreGamesSection(0, indie).then(result => sectionCallBack(result, 6, "Indie")),
      getGenreGamesSection(0, shooter).then(result => sectionCallBack(result, 7, "Shooters")),
    ]);

    const section = {
      games: {
        type: "all genres",
        sections: sections,
      },
      highLight: {
        type: "all genres",
        game: sections[8].games,
      },
    };

    return section;
  }

  static async requestSortBy(genre) {
    sections = [];
    const gamesStore = rootStore.getState().sections.games;
    const data = await gamesStore[gamesStore.findIndex(item => item.type == genre.name)];

    if (!data) {
      let indie = { id: 32, type: "genres", name: "Indie" };
      let strategy = { id: 15, type: "genres", name: "Strategy" };
      let shooter = { id: 5, type: "genres", name: "Shooter", image_id: "co1re6" };
      let theme = null;

      if (genre.type == "genres") {
        indie.id = [32, genre.id];
        strategy.id = [15, genre.id];
        shooter.id = [5, genre.id];
      } else {
        theme = genre;
      }

      await Promise.all([
        getMainGameData(genre).then(result => sectionCallBack(result, 7, "highLight")),
        getPopularSection(0, genre).then(result => sectionCallBack(result, 0, "Trending right now")),
        getMyListSection().then(result => sectionCallBack(result, 1, "Recent friends activity")),
        getTopRatedSection(0, genre).then(result => sectionCallBack(result, 2, "Top rating games of this year")),
        getCollectionsSection().then(result => sectionCallBack(result, 3, "COLLECTION GAMES")),
        getMyListSection().then(result => sectionCallBack(result, 4, "My list")),
        getGenreGamesSection(0, indie, theme).then(result => sectionCallBack(result, 5, indie.name)),
        getGenreGamesSection(0, shooter, theme).then(result => sectionCallBack(result, 6, shooter.name)),
      ]);

      const section = {
        games: {
          type: genre.name,
          sections: sections,
        },
        highLight: {
          type: genre.name,
          game: sections[7].games,
        },
      };

      return section;
    } else {
      const highLightStore = await highLightStore[highLightStore.findIndex(item => item.type == genre.name)];
      const sec = {
        games: gamesStore,
        highLight: {
          type: genre.name,
          game: highLightStore,
        },
      };
      return sec;
    }
  }
}
