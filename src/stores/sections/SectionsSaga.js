import { all, put, call, takeLatest, take, fork, cancel, race } from "redux-saga/effects";
import * as types from "../actionTypes";
import { sectionsSuccess, sectionsFailure } from "./SectionsActions";
import SectionsEffects from "./SectionsEffects";

const handleSectionsRequest = function* handleSectionsRequest(actionData) {
  try {
    const sort = actionData.sort;

    let result = null;
    
    if (sort.type === "all genres") {
      result = yield call(SectionsEffects.requestSections);
    } else {
      result = yield call(SectionsEffects.requestSortBy, sort);
    }
    if (result) {
      yield put(sectionsSuccess(result));
    } else {
      yield put(sectionsFailure(error));
    }
  } catch (error) {
    yield put(sectionsFailure(error));
  }
};

const root = function* root() {
  while (true) {
    const params = yield take(types.SECTIONS.REQUEST);
    const handleSectionsTask = yield fork(handleSectionsRequest, params);
    yield race({
      sectionsSuccess: take(types.SECTIONS.SUCCESS),
      sectionsFailure: take(types.SECTIONS.FAILURE),
    });
    yield cancel(handleSectionsTask);
  }
};
export default root;
