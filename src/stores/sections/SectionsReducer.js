import * as types from "../actionTypes";

const initialState = {
  isFetching: false,
  games: [],
  highLight: {},
  sort: {},
  error: {},
};

export default function sectionsReducer(state = initialState, action) {
  switch (action.type) {
    case types.SECTIONS.REQUEST:
      return {
        ...state,
        isFetching: true,
        failure: false,
        sort: action.sort,
        error: {},
      };
    case types.SECTIONS.SUCCESS:
      return {
        ...state,
        isFetching: false,
        games: state.games.concat(action.sections.games),
        highLight: action.sections.highLight,
        failure: false,
        error: {},
      };
    case types.SECTIONS.FAILURE:
      return {
        ...state,
        isFetching: false,
        failure: true,
        error: action.error,
      };
    default:
      return state;
  }
}
