import * as types from "../actionTypes";

export const loginRequest = credentials => {
  return {
    type: types.LOGIN.REQUEST,
    credentials,
  };
};

export function loginSuccess(user, auto = false) {
  return {
    type: types.LOGIN.SUCCESS,
    user,
    auto,
  };
}

export function loginFailure(err) {
  return {
    type: types.LOGIN.FAILURE,
    err,
  };
}

export function logout(forcedByServer = false) {
  return {
    type: types.LOGOUT,
    forcedByServer,
  };
}

export function setUser(user) {
  return {
    type: types.USER.SET,
    user,
  };
}
