import rootStore from "../rootStore";
import { loginWithPass } from "../../api2/auth";
export default class LoginEffects {
  static async requestLogin(credentials) {
    return "";
  }

  static async requestLoginWithPass(credentials) {
    return result = loginWithPass(credentials);
  }
}
