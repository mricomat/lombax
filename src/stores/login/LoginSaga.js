import { put, call, takeLatest, select, take, fork, cancel, race, delay } from "redux-saga/effects";
import RNUserDefaults from "rn-user-defaults";

import * as types from "../actionTypes";
import { loginFailure, loginSuccess, setUser, logout } from "./LoginActions";
import LoginEffects from "./LoginEffects";
import { getGamesByIds } from "../../api2/games";

const handleLoginRequest = function* handleLoginRequest({ credentials }) {
  try {
    let result;
    if (credentials.resume) {
      result = yield call(LoginEffects.requestLogin, credentials);
    } else {
      result = yield call(LoginEffects.requestLoginWithPass, credentials);
    }
    if (result.username) {
      yield put(loginSuccess(result, credentials.resume));
    } else {
      yield put(loginFailure(result));
    }
  } catch (e) {
    yield put(loginFailure(e));
  }
};

const handleLoginSuccess = function* handleLoginSuccess(user, auto) {
  yield RNUserDefaults.set(`"lombax_user_token"-${user.userId}`, user.accessToken);
  // fetch all data
  // FETCH SECTIONS with

  yield put(setUser(user));

  if (auto) {
    //start at home
  } else {
    const favorites = yield call(getGamesByIds, user.favorites);
    const reviews = yield call(getGamesByIds, user.favorites);

    
    yield put(setUser(user));
    //go to profile
  }
};

const handleLogout = function* handleLogout() {};

const root = function* root() {
  console.log("login");
  yield takeLatest(types.LOGIN.REQUEST, handleLoginRequest);
  yield takeLatest(types.LOGOUT, handleLogout);

  while (true) {
    const params = yield take(types.LOGIN.SUCCESS);
    const loginSuccessTask = yield fork(handleLoginSuccess, params);
    yield cancel(loginSuccessTask);
  }
};
export default root;
