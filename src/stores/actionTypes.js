const REQUEST = "REQUEST";
const SUCCESS = "SUCCESS";
const FAILURE = "FAILURE";
const defaultTypes = [REQUEST, SUCCESS, FAILURE];
createRequestTypes = (base, types = defaultTypes) => {
  const res = {};
  types.forEach(type => (res[type] = `${base}_${type}`));
  return res;
};

export const APP = createRequestTypes("APP", ["START", "READY", "INIT", "INIT_LOCAL_SETTINGS"]);

// Sections
export const SECTIONS = createRequestTypes("SECTIONS", [...defaultTypes]);

// Login events
export const LOGIN = createRequestTypes("LOGIN", [...defaultTypes]);
export const LOGOUT = "LOGOUT"; // logout is always success
export const USER = createRequestTypes("USER", ["SET"]);
