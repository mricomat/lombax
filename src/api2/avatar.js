import { getDataRequest } from "./fetch";

export const saveAvatar = async ({ image, userId }) => {
  const formData = new FormData();
  formData.append("image", image);
  return await getDataRequest("/avatar/save/default", "POST", { image: formData });
};

export const saveBackground = async ({ image, userId }) => {
  return await getDataRequest("/avatar/save/background", "POST", { image, userId });
};
