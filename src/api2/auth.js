import { getDataRequest } from "./fetch";
import { saveAvatar, saveBackground } from "./avatar";
const BASE_URL = "http://192.168.0.20:8080/avatar/save/default";
import axios from "axios";

export const isValidEmail = async email => {
  return await getDataRequest("/users/isEmailValid", "POST", email);
};

export const isValidUsername = async username => {
  return await getDataRequest("/users/isUsernameValid", "POST", username);
};

export const loginWithPass = async credentials => {
  return await getDataRequest("/auth/login", "POST", credentials);
};

export const register = async ({ username, email, password, interests, avatar, background }) => {
  // return await getDataRequest("/users/register", "POST", { username, email, password, interests });

  const headers = {
    "content-type": "multipart/form-data",
  };
  GLOBAL.FormData = GLOBAL.originalFormData || GLOBAL.FormData;

  // TODO Query GET
  const formData = new FormData();
  // formData.append("image", {
  //   uri: avatar.uri,
  //   type: "image/jpeg",
  //   name: "profile-picture",
  // });

  console.log(avatar);
  formData.append("file", {
    uri: "file://storage/emulated/0/DCIM/Camera/IMG_20200420_124248.jpg",
    type: "image/jpeg",
    name: "IMG_20200420_124248.jpg",
  });

  //   console.log("entra", formData);
  //   return await axios({
  //     url: BASE_URL,
  //     method: "POST",
  //     headers,
  //     data: formData,
  //   })
  //     .then(response => {
  //       if (response.status == 200 || response.status == 201) {
  //         return response.data;
  //       } else {
  //         return null;
  //       }
  //     })
  //     .catch(err => {
  //       console.warn("Request Error", err);
  //       return error;
  //     });

  // };

  var config = { headers: { Accept: "application/json", "Content-Type": "multipart/form-data" } };

  axios
    .post(BASE_URL, { file: formData }, config)
    .then(response => {
      //code
    })
    .catch(function(error) {
      //code
    });
};
