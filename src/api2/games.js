import { getDataRequest } from "./fetch";

export const getGamesByIds = async ids => {
  return await getDataRequest("/games/ids", "GET", ids, true);
};
