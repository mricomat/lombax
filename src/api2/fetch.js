import axios from "axios";
import store from "../stores/rootStore";

const BASE_URL = "https://lombax.herokuapp.com";

export const getDataRequest = async (endPoint, method, params, auth) => {
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };

  const { login } = reduxStore.getState();
  auth && (headers["Authorization"] = `Bearer ${login.user.userToken}`);

  console.log(endPoint, method, params);

  // TODO Query GET

  return await axios({
    url: `${BASE_URL}${endPoint}`,
    method: method,
    headers,
    data: params,
  })
    .then(response => {
      if (response.status == 200 || response.status == 201) {
        return response.data;
      } else {
        return response;
      }
    })
    .catch(error => {
      if (error.response.data.apierror) {
        return error.response.data.apierror;
      }
      return error.response.data;
    });
};
