import React from "react";
import { View, Image, StatusBar } from "react-native";
import Root from "./routes/Routes";
import store from "./stores/rootStore";
import { sectionsRequest } from "./stores/sections/SectionsActions";
import { Provider } from "react-redux";

import rootStore from "./stores/rootStore";

export default class App extends React.Component {
  componentDidMount() {
    const sort = { type: "all genres", name: "action", id: 1 };
    store.dispatch(sectionsRequest(sort));
  }

  render() {
    return (
      <Provider store={rootStore}>
        <View style={{ flex: 1, backgroundColor: "#191919" }}>
          <StatusBar translucent={true} />
          <Root
            style={{
              flex: 1,
              justifyContent: "flex-start",
              backgroundColor: "#191919",
            }}
          />
        </View>
      </Provider>
    );
  }
}
