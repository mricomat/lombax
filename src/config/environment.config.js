import development from "./environments/development.env";
import production from "./environments/production.env";

export default __DEV__ ? development : production;
