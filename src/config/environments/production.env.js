import environment from "./base.env";

const baseApi = `https://wsc01.ionplayer.es/IONPws/ws/v118`;
const env = environment(baseApi);

export default {
  ...env,
  route: {
    ...env.route,
  },
};
