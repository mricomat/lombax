export default function(baseApi) {
  return {
    api: {
      login: `${baseApi}/login_v118.php`,
    },
    isProduction: true,
    isDevelopment: false,
  };
}
