export const platformsWithImage = [
  { id: 48, type: "platform", name: "PS4", image_id: "co1lga", colorLeft: "#303EFF", colorRight: "#1A1A1A", detail: "The PlayStation 4 system" },
  { id: 49, type: "platform", name: "Xbox One", image_id: "co1re6", colorLeft: "#107C10", colorRight: "#1A1A1A", detail: "" },
  {
    id: 130,
    type: "platform",
    name: "Nintendo Switch",
    image_id: "co1mdo",
    colorLeft: "#E7000A",
    colorRight: "#1A1A1A",
    detail: "Nitendo Switch is a hybrid console/tablet",
  },
  {
    id: 6,
    type: "platform",
    name: "PC",
    image_id: "ar4rw",
    colorLeft: "#30ACFF",
    colorRight: "#1A1A1A",
    detail: "Microsoft Windows operating system",
  },
  {
    id: 34,
    type: "platform",
    name: "Android",
    image_id: "co1ofs",
    colorLeft: "#95CF00",
    colorRight: "#1A1A1A",
    detail: "Android mobile operating system",
  },
  {
    id: 14,
    type: "platform",
    name: "MAC",
    image_id: "htsbgb75k1zmn5gxjuqc",
    colorLeft: "#D1C8BD",
    colorRight: "#1A1A1A",
    detail: "MAC computer from Apple",
  },
  {
    id: 39,
    type: "platform",
    name: "IOS",
    image_id: "kdiivvsrspavceeqadld",
    colorLeft: "#D1C8BD",
    colorRight: "#1A1A1A",
    detail: "IOS is Apples mobile operating system",
  },
  {
    id: 9,
    type: "platform",
    name: "PS3",
    image_id: "rchvtoqmrqxldczm19gy",
    colorLeft: "#003989",
    colorRight: "#1A1A1A",
    detail: "The PlayStation 3 system",
  },
  { id: 12, type: "platform", name: "Xbox 360", image_id: "ar5sr", colorLeft: "#95CB47", colorRight: "#1A1A1A", detail: "" },
  { id: 3, type: "platform", name: "Linux", image_id: "co1tab", colorLeft: "#F4AA12", colorRight: "#1A1A1A", detail: "Linux operating system" },
];

export default Platforms = [
  {
    id: 156,
    name: "Thomson MO5",
  },
  {
    id: 160,
    name: "Nintendo eShop",
  },
  {
    id: 74,
    abbreviation: "Win Phone",
    alternative_name: "WP",
    name: "Windows Phone",
  },
  {
    id: 123,
    name: "WonderSwan Color",
  },
  {
    id: 163,
    abbreviation: "Steam VR",
    name: "SteamVR",
  },
  {
    id: 44,
    abbreviation: "zod",
    name: "Tapwave Zodiac",
  },
  {
    id: 169,
    alternative_name: "Project Scarlett",
    name: "Xbox Project Scarlett",
  },
  {
    id: 68,
    abbreviation: "colecovision",
    name: "ColecoVision",
  },
  {
    id: 236,
    name: "Exidy Sorcerer",
  },
  {
    id: 3,
    abbreviation: "Linux",
    alternative_name: "GNU/Linux",
    name: "Linux",
  },
  {
    id: 6,
    abbreviation: "PC",
    alternative_name: "mswin",
    name: "PC (Microsoft Windows)",
  },
  {
    id: 7,
    abbreviation: "PS1",
    name: "PlayStation",
  },
  {
    id: 8,
    abbreviation: "PS2",
    alternative_name: "PS2",
    name: "PlayStation 2",
  },
  {
    id: 9,
    abbreviation: "PS3",
    name: "PlayStation 3",
  },
  {
    id: 19,
    abbreviation: "SNES",
    alternative_name: "SNES, Super Nintendo",
    name: "Super Nintendo Entertainment System (SNES)",
  },
  {
    id: 25,
    abbreviation: "ACPC",
    alternative_name: "Colour Personal Computer",
    name: "Amstrad CPC",
  },
  {
    id: 27,
    abbreviation: "MSX",
    name: "MSX",
  },
  {
    id: 39,
    abbreviation: "iOS",
    name: "iOS",
  },
  {
    id: 75,
    abbreviation: "Apple][",
    alternative_name: "apple ][",
    name: "Apple II",
  },
  {
    id: 80,
    abbreviation: "neogeoaes",
    alternative_name: "AES",
    name: "Neo Geo AES",
  },
  {
    id: 94,
    abbreviation: "C+4",
    name: "Commodore Plus/4",
  },
  {
    id: 96,
    abbreviation: "pdp10",
    name: "PDP-10",
  },
  {
    id: 237,
    name: "Sol-20",
  },
  {
    id: 129,
    abbreviation: "ti-99",
    name: "Texas Instruments TI-99",
  },
  {
    id: 133,
    alternative_name: "Magnavox Odyssey²",
    name: "Philips Videopac G7000",
  },
  {
    id: 134,
    name: "Acorn Electron",
  },
  {
    id: 170,
    abbreviation: "Stadia",
    alternative_name: "Stadia",
    name: "Google Stadia",
  },
  {
    id: 203,
    alternative_name: "Google Stadia",
    name: "Stadia",
  },
  {
    id: 51,
    abbreviation: "fds",
    alternative_name: "Famicom Disk System",
    name: "Family Computer Disk System",
  },
  {
    id: 120,
    name: "Neo Geo Pocket Color",
  },
  {
    id: 165,
    abbreviation: "PlayStation VR",
    name: "PlayStation VR",
  },
  {
    id: 67,
    abbreviation: "intellivision",
    name: "Intellivision",
  },
  {
    id: 136,
    name: "Neo Geo CD",
  },
  {
    id: 71,
    abbreviation: "vic-20",
    name: "Commodore VIC-20",
  },
  {
    id: 142,
    name: "PC-50X Family",
  },
  {
    id: 144,
    name: "AY-3-8710",
  },
  {
    id: 146,
    name: "AY-3-8605",
  },
  {
    id: 88,
    abbreviation: "odyssey",
    alternative_name: "Magnavox Odyssey; Odysee; Odisea; Odissea",
    name: "Odyssey",
  },
  {
    id: 90,
    abbreviation: "cpet",
    name: "Commodore PET",
  },
  {
    id: 147,
    name: "AY-3-8606",
  },
  {
    id: 91,
    abbreviation: "astrocade",
    name: "Bally Astrocade",
  },
  {
    id: 11,
    abbreviation: "XBOX",
    name: "Xbox",
  },
  {
    id: 12,
    abbreviation: "X360",
    alternative_name: "xbx360",
    name: "Xbox 360",
  },
  {
    id: 164,
    name: "Daydream",
  },
  {
    id: 48,
    abbreviation: "PS4",
    alternative_name: "PS4",
    name: "PlayStation 4",
  },
  {
    id: 97,
    abbreviation: "pdp-8",
    name: "PDP-8",
  },
  {
    id: 115,
    name: "Apple IIGS",
  },
  {
    id: 118,
    name: "FM Towns",
  },
  {
    id: 126,
    name: "TRS-80",
  },
  {
    id: 128,
    abbreviation: "supergrafx",
    name: "PC Engine SuperGrafx",
  },
  {
    id: 135,
    name: "Hyper Neo Geo 64",
  },
  {
    id: 98,
    abbreviation: "gt40",
    name: "DEC GT40",
  },
  {
    id: 105,
    abbreviation: "hp3000",
    name: "HP 3000",
  },
  {
    id: 4,
    abbreviation: "N64",
    alternative_name: "N64",
    name: "Nintendo 64",
  },
  {
    id: 36,
    abbreviation: "xla",
    name: "Xbox Live Arcade",
  },
  {
    id: 45,
    abbreviation: "psn",
    alternative_name: "PSN",
    name: "PlayStation Network",
  },
  {
    id: 46,
    abbreviation: "Vita",
    alternative_name: "PS Vita",
    name: "PlayStation Vita",
  },
  {
    id: 47,
    abbreviation: "VC",
    name: "Virtual Console (Nintendo)",
  },
  {
    id: 53,
    abbreviation: "MSX2",
    name: "MSX2",
  },
  {
    id: 56,
    abbreviation: "WiiWare",
    name: "WiiWare",
  },
  {
    id: 57,
    abbreviation: "WonderSwan",
    name: "WonderSwan",
  },
  {
    id: 60,
    abbreviation: "Atari7800",
    alternative_name: "Atari 7800 ProSystem",
    name: "Atari 7800",
  },
  {
    id: 62,
    abbreviation: "Jaguar",
    name: "Atari Jaguar",
  },
  {
    id: 65,
    abbreviation: "Atari8bit",
    name: "Atari 8-bit",
  },
  {
    id: 66,
    abbreviation: "Atari5200",
    alternative_name: "Atari 5200 SuperSystem",
    name: "Atari 5200",
  },
  {
    id: 124,
    name: "SwanCrystal",
  },
  {
    id: 127,
    name: "Fairchild Channel F",
  },
  {
    id: 131,
    alternative_name: "Nintendo Super Disc",
    name: "Nintendo PlayStation",
  },
  {
    id: 148,
    name: "AY-3-8607",
  },
  {
    id: 149,
    name: "PC-98",
  },
  {
    id: 14,
    abbreviation: "Mac",
    alternative_name: "Mac OS",
    name: "Mac",
  },
  {
    id: 158,
    alternative_name: "Commodore Dynamic Total Vision",
    name: "Commodore CDTV",
  },
  {
    id: 15,
    abbreviation: "C64",
    name: "Commodore C64/128",
  },
  {
    id: 34,
    abbreviation: "Android",
    alternative_name: "Infocusa3",
    name: "Android",
  },
  {
    id: 35,
    abbreviation: "Game Gear",
    name: "Sega Game Gear",
  },
  {
    id: 87,
    abbreviation: "virtualboy",
    name: "Virtual Boy",
  },
  {
    id: 92,
    abbreviation: "steam",
    name: "SteamOS",
  },
  {
    id: 159,
    name: "Nintendo DSi",
  },
  {
    id: 50,
    abbreviation: "3DO",
    name: "3DO Interactive Multiplayer",
  },
  {
    id: 119,
    name: "Neo Geo Pocket",
  },
  {
    id: 52,
    abbreviation: "Arcade",
    name: "Arcade",
  },
  {
    id: 55,
    abbreviation: "Mobile",
    name: "Mobile",
  },
  {
    id: 58,
    abbreviation: "SFAM",
    alternative_name: "SFC",
    name: "Super Famicom",
  },
  {
    id: 5,
    abbreviation: "Wii",
    alternative_name: "Revolution",
    name: "Wii",
  },
  {
    id: 69,
    abbreviation: "bbcmicro",
    name: "BBC Microcomputer System",
  },
  {
    id: 70,
    abbreviation: "vectrex",
    name: "Vectrex",
  },
  {
    id: 89,
    abbreviation: "microvision",
    name: "Microvision",
  },
  {
    id: 95,
    abbreviation: "pdp1",
    alternative_name: "Programmed Data Processor-1",
    name: "PDP-1",
  },
  {
    id: 108,
    abbreviation: "pdp11",
    name: "PDP-11",
  },
  {
    id: 112,
    abbreviation: "microcomputer",
    name: "Microcomputer",
  },
  {
    id: 22,
    abbreviation: "GBC",
    name: "Game Boy Color",
  },
  {
    id: 23,
    abbreviation: "DC",
    name: "Dreamcast",
  },
  {
    id: 26,
    abbreviation: "ZXS",
    name: "ZX Spectrum",
  },
  {
    id: 33,
    abbreviation: "Game Boy",
    name: "Game Boy",
  },
  {
    id: 13,
    abbreviation: "DOS",
    name: "PC DOS",
  },
  {
    id: 38,
    abbreviation: "PSP",
    alternative_name: "PSP",
    name: "PlayStation Portable",
  },
  {
    id: 24,
    abbreviation: "GBA",
    alternative_name: "GBA",
    name: "Game Boy Advance",
  },
  {
    id: 41,
    abbreviation: "WiiU",
    name: "Wii U",
  },
  {
    id: 42,
    abbreviation: "NGage",
    name: "N-Gage",
  },
  {
    id: 125,
    name: "PC-8801",
  },
  {
    id: 138,
    name: "VC 4000",
  },
  {
    id: 150,
    name: "Turbografx-16/PC Engine CD",
  },
  {
    id: 155,
    name: "Tatung Einstein",
  },
  {
    id: 166,
    name: "Pokémon mini",
  },
  {
    id: 30,
    abbreviation: "Sega32",
    name: "Sega 32X",
  },
  {
    id: 137,
    name: "New Nintendo 3DS",
  },
  {
    id: 37,
    abbreviation: "3DS",
    alternative_name: "3DS",
    name: "Nintendo 3DS",
  },
  {
    id: 84,
    abbreviation: "sg1000",
    alternative_name: "Sega Game 1000",
    name: "SG-1000",
  },
  {
    id: 130,
    abbreviation: "Switch",
    alternative_name: "NX",
    name: "Nintendo Switch",
  },
  {
    id: 85,
    abbreviation: "donner30",
    name: "Donner Model 30",
  },
  {
    id: 132,
    name: "Amazon Fire TV",
  },
  {
    id: 139,
    name: "1292 Advanced Programmable Video System",
  },
  {
    id: 140,
    name: "AY-3-8500",
  },
  {
    id: 152,
    name: "FM-7",
  },
  {
    id: 153,
    name: "Dragon 32/64",
  },
  {
    id: 154,
    name: "Amstrad PCW",
  },
  {
    id: 101,
    abbreviation: "nimrod",
    name: "Ferranti Nimrod Computer",
  },
  {
    id: 161,
    alternative_name: "WMR",
    name: "Windows Mixed Reality",
  },
  {
    id: 106,
    abbreviation: "sdssigma7",
    name: "SDS Sigma 7",
  },
  {
    id: 109,
    abbreviation: "cdccyber70",
    name: "CDC Cyber 70",
  },
  {
    id: 143,
    name: "AY-3-8760",
  },
  {
    id: 49,
    abbreviation: "XONE",
    alternative_name: "Xbone",
    name: "Xbox One",
  },
  {
    id: 78,
    abbreviation: "segacd",
    alternative_name: "Mega CD",
    name: "Sega CD",
  },
  {
    id: 16,
    abbreviation: "Amiga",
    name: "Amiga",
  },
  {
    id: 82,
    abbreviation: "browser",
    alternative_name: "Internet",
    name: "Web browser",
  },
  {
    id: 114,
    name: "Amiga CD32",
  },
  {
    id: 116,
    name: "Acorn Archimedes",
  },
  {
    id: 29,
    abbreviation: "Genesis",
    alternative_name: "Sega Genesis",
    name: "Sega Mega Drive/Genesis",
  },
  {
    id: 145,
    name: "AY-3-8603",
  },
  {
    id: 151,
    name: "TRS-80 Color Computer",
  },
  {
    id: 167,
    name: "PlayStation 5",
  },
  {
    id: 122,
    name: "Nuon",
  },
  {
    id: 77,
    abbreviation: "x1",
    name: "Sharp X1",
  },
  {
    id: 79,
    abbreviation: "neogeomvs",
    alternative_name: "Neo Geo Multi Video System",
    name: "Neo Geo MVS",
  },
  {
    id: 93,
    abbreviation: "C16",
    name: "Commodore 16",
  },
  {
    id: 99,
    abbreviation: "famicom",
    alternative_name: "Famicom",
    name: "Family Computer (FAMICOM)",
  },
  {
    id: 100,
    abbreviation: "analogueelectronics",
    name: "Analogue electronics",
  },
  {
    id: 102,
    abbreviation: "edsac",
    alternative_name: "Electronic Delay Storage Automatic Calculator",
    name: "EDSAC",
  },
  {
    id: 103,
    abbreviation: "pdp-7",
    name: "PDP-7",
  },
  {
    id: 104,
    abbreviation: "hp2100",
    name: "HP 2100",
  },
  {
    id: 107,
    abbreviation: "call-a-computer",
    name: "Call-A-Computer time-shared mainframe computer system",
  },
  {
    id: 110,
    abbreviation: "plato",
    alternative_name: "Programmed Logic for Automatic Teaching Operations",
    name: "PLATO",
  },
  {
    id: 18,
    abbreviation: "NES",
    alternative_name: "NES",
    name: "Nintendo Entertainment System (NES)",
  },
  {
    id: 32,
    abbreviation: "Saturn",
    alternative_name: "JVC Saturn, Hi-Saturn, Samsung Saturn, V-Saturn",
    name: "Sega Saturn",
  },
  {
    id: 59,
    abbreviation: "Atari2600",
    name: "Atari 2600",
  },
  {
    id: 64,
    abbreviation: "SMS",
    alternative_name: "SMS",
    name: "Sega Master System",
  },
  {
    id: 72,
    abbreviation: "Ouya",
    name: "Ouya",
  },
  {
    id: 113,
    name: "OnLive Game System",
  },
  {
    id: 117,
    name: "Philips CD-i",
  },
  {
    id: 121,
    name: "Sharp X68000",
  },
  {
    id: 141,
    name: "AY-3-8610",
  },
  {
    id: 157,
    name: "NEC PC-6000 Series",
  },
  {
    id: 162,
    abbreviation: "Oculus VR",
    name: "Oculus VR",
  },
  {
    id: 21,
    abbreviation: "NGC",
    alternative_name: "Dolphin",
    name: "Nintendo GameCube",
  },
  {
    id: 20,
    abbreviation: "NDS",
    alternative_name: "DS",
    name: "Nintendo DS",
  },
  {
    id: 61,
    abbreviation: "Lynx",
    name: "Atari Lynx",
  },
  {
    id: 63,
    abbreviation: "Atari-ST",
    name: "Atari ST/STE",
  },
  {
    id: 73,
    abbreviation: "blackberry",
    name: "BlackBerry OS",
  },
  {
    id: 86,
    abbreviation: "turbografx16",
    name: "TurboGrafx-16/PC Engine",
  },
  {
    id: 111,
    abbreviation: "imlac-pds1",
    name: "Imlac PDS-1",
  },
];
