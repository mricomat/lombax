import React from "react";
import { View, Image } from "react-native";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import Icon from "react-native-ionicons";

import Navigation from "./Navigation";

const RouteNames = ["Soon", "Search", " ", "Reviews", "More"];

const KeyIconTabs = {
  Soon: "rocket",
  Search: "search",
  Home: "home",
  Reviews: "stats",
  More: "menu",
};

const fade = props => {
  const { position, scene } = props;

  const index = scene.index;

  const translateX = 0;
  const translateY = 0;

  const opacity = position.interpolate({
    inputRange: [index - 0.7, index, index + 0.7],
    outputRange: [0.3, 1, 0.3],
  });

  return {
    opacity,
    transform: [{ translateX }, { translateY }],
  };
};

const HomeNavigator = createStackNavigator(
  {
    HomeView: {
      getScreen: () => require("../views/HomeView").default,
    },
    MosaicScreen: {
      getScreen: () => require("../views/MosaicScreen").default,
    },
    YoutubeVideoScreen: {
      getScreen: () => require("../components/YoutubeVideoView").default,
    },
    DetailGameView: {
      getScreen: () => require("../views/DetailGameView").default,
    },
    DetailGameScreen: {
      getScreen: () => require("../views/DetailGameScreen").default,
    },
    AddReviewScreen: {
      getScreen: () => require("../views/AddReviewScreen").default,
    },
    ReviewsGameScreen: {
      getScreen: () => require("../views/ReviewsGameScreen").default,
    },
  },
  {
    headerMode: "none",
    transitionConfig: () => ({
      screenInterpolator: props => {
        return fade(props);
      },
    }),
  },
);

const SearchNavigator = createStackNavigator(
  {
    SearchView: {
      getScreen: () => require("../views/SearchView").default,
    },
    DetailGameView: {
      getScreen: () => require("../views/DetailGameView").default,
    },
    MosaicScreen: {
      getScreen: () => require("../views/MosaicScreen").default,
    },
  },
  {
    headerMode: "none",
    transitionConfig: () => ({
      screenInterpolator: props => {
        return fade(props);
      },
    }),
  },
);
const IncomingNavigator = createStackNavigator(
  {
    SoonScreen: {
      getScreen: () => require("../views/SoonScreen").default,
    },
  },
  {
    headerMode: "none",
    transitionConfig: () => ({
      screenInterpolator: props => {
        return fade(props);
      },
    }),
  },
);
const ReviewsNavigator = createStackNavigator(
  {
    ReviewsScreen: {
      getScreen: () => require("../views/ReviewsScreen").default,
    },
    ReviewDetailScreen: {
      getScreen: () => require("../views/ReviewDetailScreen").default,
    },
    SectionsProfileScreen: {
      getScreen: () => require("../views/SectionsProfileScreen").default,
    }
  },
  {
    headerMode: "none",
    transitionConfig: () => ({
      screenInterpolator: props => {
        return fade(props);
      },
    }),
  },
);
const MoreNavigator = createStackNavigator(
  {
    LoginScreen: {
      getScreen: () => require("../views/LoginScreen").default,
    },
    ProfileScreen: {
      getScreen: () => require("../views/ProfileScreen").default,
    },
    RegisterScreen: {
      getScreen: () => require("../views/RegisterScreen").default,
    },
    SettingsScreen: {
      getScreen: () => require("../views/SettingsScreen").default,
    },
  },
  {
    headerMode: "none",
    transitionConfig: () => ({
      screenInterpolator: props => {
        return fade(props);
      },
    }),
  },
);

const Screens = [IncomingNavigator, SearchNavigator, HomeNavigator, ReviewsNavigator, MoreNavigator];

const createAppBottomTabs = () => {
  let tabs = {};

  RouteNames.forEach((item, index) => {
    tabs[item] = {
      screen: Screens[index],
      navigationOptions: ({ navigation }) => {
        title: item;
      },
    };
  });

  return tabs;
};

const TabNavigator = createBottomTabNavigator(createAppBottomTabs(), {
  initialRouteName: " ",
  defaultNavigationOptions: ({ navigation }) => {
    return {
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName = KeyIconTabs[routeName];

        return routeName == " " ? (
          <View
            style={{
              width: 70,
              height: 70,
              padding: 10,
              borderRadius: 35,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "black",
            }}>
            <Image
              resizeMode={"contain"}
              style={{
                width: 50,
                height: 50,
                marginTop: 6,
                backgroundColor: "black",
                tintColor: focused ? null : "white",
              }}
              source={require("../assets/ice.png")}
            />
          </View>
        ) : (
          <Icon name={iconName} size={25} color={focused ? "#117ea9" : "#707070"} />
        );
      },
      tabBarOptions: {
        activeTintColor: "#117ea9",
        //inactiveTintColor: global.loginData.lookAndFeel.color,
        style: {
          backgroundColor: "black",
        },
        transitionConfig: () => ({
          screenInterpolator: props => {
            return fade(props);
          },
        }),
      },
    };
  },
});

const RootStack = createAppContainer(
  createSwitchNavigator(
    {
      TabStack: TabNavigator,
    },

    {
      initialRouteName: "TabStack",
    },
  ),
);

export default class Root extends React.Component {
  render() {
    return (
      <RootStack
        ref={navigatorRef => {
          Navigation.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
