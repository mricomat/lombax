import { NavigationActions } from "react-navigation";

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
      key: routeName + Math.random(10000),
    }),
  );
}

export default {
  navigate,
  setTopLevelNavigator,
};
