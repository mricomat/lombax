/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import {YellowBox} from 'react-native';

console.disableYellowBox = true; // TODO DELETE
YellowBox.ignoreWarnings(['Warning: ...']);
if (__DEV__) {
  require('./src/ReactotronConfig');
} else {
  console.log = () => {};
  console.time = () => {};
  console.timeLog = () => {};
  console.timeEnd = () => {};
  console.warn = () => {};
  console.count = () => {};
  console.countReset = () => {};
  console.error = () => {};
  console.info = () => {};
}

AppRegistry.registerComponent(appName, () => App);
