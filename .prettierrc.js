module.exports = {
  printWidth: 135,
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: false,
  trailingComma: 'all',
};
